package com.becam.android.app.fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkResponse;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HttpHeaderParser;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.becam.android.app.R;
import com.becam.android.app.activity.LoginActivity;
import com.becam.android.app.others.SharedPrefsUtils;
import com.becam.android.app.others.appConstants;
import com.becam.android.app.others.urlClass;

import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

/**
 * Created by HP on 5/3/2017.
 */

public class RegisterDetailsFragment extends Fragment
{
    View view;

    EditText fnameTXT,lnameTXT,emailTXT,passTXT,confirmPassTXT,mobileTXT;
    Button submitBTN;
    //Animation animShke;
    SharedPrefsUtils sharedPref;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view=inflater.inflate(R.layout.register_details_fragment_layout, container, false);
        initialScreen();
        submitBTN.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {


                if(checkRule())
                {
                    appConstants.startSpinwheel(getActivity(), false, true);

                    new Thread(new Runnable() {
                        @Override
                        public void run()
                        {

                            try
                            {
                                String url="";// urlClass.registerURL;
                                JSONObject registerObjct = new JSONObject();
                                registerObjct.putOpt("FNAME", fnameTXT.getText().toString().trim());
                                registerObjct.putOpt("LNAME", lnameTXT.getText().toString().trim());
                                registerObjct.putOpt("EMAIL", emailTXT.getText().toString().trim());
                                registerObjct.putOpt("PASSWORD", confirmPassTXT.getText().toString().trim());
                                registerObjct.putOpt("MOBILE", mobileTXT.getText().toString().trim());
                                registerObjct.putOpt("DEVICE_ID","1");
                                registerObjct.putOpt("STATUS","1");

                                registrationRequest(url,registerObjct);
                            }
                            catch (Exception xx)
                            {
                                xx.toString();
                                appConstants.stopSpinWheel();
                            }
                        }
                    }).start();
                }
            }
        });

        return view;

    }



    private void initialScreen()
    {
        fnameTXT= (EditText) view.findViewById(R.id.reg_fname_txt);
        lnameTXT= (EditText) view.findViewById(R.id.reg_lname_txt);
        mobileTXT= (EditText) view.findViewById(R.id.reg_mobile_txt);
        emailTXT= (EditText) view.findViewById(R.id.reg_email_address_txt);
        passTXT= (EditText) view.findViewById(R.id.reg_password_txt);
        confirmPassTXT= (EditText) view.findViewById(R.id.reg_confirm_password_txt);
        submitBTN= (Button) view.findViewById(R.id.submitRgsterBTN);
       // animShke= AnimationUtils.loadAnimation(getActivity(),R.anim.shake);
    }


    private  boolean checkRule()
    {
        if (fnameTXT.getText().toString().trim().equals("")) {
            Toast.makeText(getActivity(), "Please check the First name", Toast.LENGTH_SHORT).show();
            return false;
        }
        if (lnameTXT.getText().toString().trim().equals("")) {
            Toast.makeText(getActivity(), "Please check the last name", Toast.LENGTH_SHORT).show();
            return false;
        }

        if (mobileTXT.getText().toString().trim().equals("")) {
            Toast.makeText(getActivity(), "Please check mobile number", Toast.LENGTH_SHORT).show();
            return false;
        }
        if (mobileTXT.length()<5) {
            Toast.makeText(getActivity(), "Invalid mobile", Toast.LENGTH_SHORT).show();
            return false;
        }

        if (emailTXT.getText().toString().trim().equals("")) {
            Toast.makeText(getActivity(), "Please check the email", Toast.LENGTH_SHORT).show();
            return false;
        }
        if (!emailTXT.getText().toString().trim().matches(appConstants.email_pattern)) {
            Toast.makeText(getActivity(), "Invalid email", Toast.LENGTH_SHORT).show();
            return false;
        }
        if (passTXT.getText().toString().trim().equals("")) {
            Toast.makeText(getActivity(), "Please check the password", Toast.LENGTH_SHORT).show();
            return false;
        }
        if (!confirmPassTXT.getText().toString().trim().equals(passTXT.getText().toString().trim()))
        {
            Toast.makeText(getActivity(), "Please confirm password", Toast.LENGTH_SHORT).show();
            return false;
        }

        return  true;
    }


    private void registrationRequest2(String urlPost, final JSONObject jsonObject) {

        final RequestQueue queue;
        StringRequest stringRequest = null;

        try {


            queue = Volley.newRequestQueue(getActivity());

            stringRequest = new StringRequest(Request.Method.POST, urlPost,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            try
                            {
                                appConstants.stopSpinWheel();
                                if(response !=null)
                                {
                                    JSONObject jsonObjResp = new JSONObject(response);
                                    final String status=jsonObjResp.getString("status");

                                    if(status.equalsIgnoreCase("true"))
                                    {
                                        Toast.makeText(getActivity(), "Registration Success", Toast.LENGTH_LONG).show();

                                        String userID=jsonObjResp.get("message").toString();
                                        sharedPref.setStringPreference(getActivity(), appConstants.userID_KEY,userID);
                                        sharedPref.setStringPreference(getActivity(), appConstants.userEmail_KEY,emailTXT.getText().toString().trim());
                                        sharedPref.setStringPreference(getActivity(), appConstants.userPassword_KEY,passTXT.getText().toString().trim());
                                        sharedPref.setBooleanPreference(getActivity(), appConstants.isLoggedIn,true);
                                        getActivity().finish();
                                    }
                                    else
                                    {
                                        Toast.makeText(getActivity(), status, Toast.LENGTH_LONG).show();
                                    }
                                }
                                else
                                {
                                  //  Toast.makeText(getActivity(), "server error", Toast.LENGTH_LONG).show();
                                }



                            }
                            catch (Exception xx)
                            {
                                xx.toString();
                                appConstants.stopSpinWheel();
                            }


                        }
                    }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(final VolleyError error) {
                    String xx = error.toString();
                    Toast.makeText(getActivity(), xx, Toast.LENGTH_SHORT).show();
                    appConstants.stopSpinWheel();


                }


            })


            {

                @Override
                protected Map<String, String> getParams() {

                    Map<String, String> params = new HashMap<String, String>();
                    try {

                        Iterator<?> keys = jsonObject.keys();

                        while (keys.hasNext()) {
                            String key = (String) keys.next();
                            String value = jsonObject.getString(key);
                            params.put(key, value);

                        }


                    } catch (Exception xx) {
                        xx.toString();
                        appConstants.stopSpinWheel();
                    }
                    return params;
                }


                @Override
                protected Response<String> parseNetworkResponse(NetworkResponse response) {
                    try {

                        String jsonString = new String(response.data,
                                HttpHeaderParser.parseCharset(response.headers));

                        return Response.success(jsonString, HttpHeaderParser.parseCacheHeaders(response));


                    } catch (UnsupportedEncodingException e) {
                        return Response.error(new ParseError(e));
                    }
                }


            };

            stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                    appConstants.appTimeOut,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

            queue.add(stringRequest);


        } catch (Exception e) {
            appConstants.stopSpinWheel();
            e.toString();
        }



    }

    private void registrationRequest(String urlPost, final JSONObject jsonObjectRequest) {

        JsonObjectRequest jsonObjReq;

        jsonObjReq = new JsonObjectRequest(Request.Method.POST, urlPost, jsonObjectRequest, new Response.Listener<JSONObject>()
        {

            @Override
            public void onResponse(JSONObject response) {

                try
                {
                    if(response !=null)
                    {
                        appConstants.stopSpinWheel();

                            JSONObject jsonObjResp = new JSONObject(response.toString());
                            final String status=jsonObjResp.getString("status");

                            if(status.equalsIgnoreCase("true"))
                            {
                                Toast.makeText(getActivity(), "Registration Success", Toast.LENGTH_LONG).show();
                                LoginActivity.viewPager.setCurrentItem(0);
                            }
                            else
                            {
                                Toast.makeText(getActivity(), jsonObjResp.getString("message"), Toast.LENGTH_LONG).show();
                            }

                    }
                    else
                    {
                        appConstants.stopSpinWheel();
                      //  Toast.makeText(getActivity(), "server error", Toast.LENGTH_LONG).show();
                    }


                }
                catch (Exception e)
                {
                    Toast.makeText(getActivity(),e.getMessage().toString(), Toast.LENGTH_SHORT).show();
                    appConstants.stopSpinWheel();
                }

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error)
            {
                Toast.makeText(getActivity(),error.getMessage(), Toast.LENGTH_SHORT).show();
                appConstants.stopSpinWheel();

            }
        });

        RequestQueue requestQueue = Volley.newRequestQueue(getActivity());
        jsonObjReq.setRetryPolicy(new DefaultRetryPolicy(
                appConstants.appTimeOut,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(jsonObjReq);
    }



}
