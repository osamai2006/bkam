package com.becam.android.app.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.Switch;
import android.widget.TextView;

import com.becam.android.app.R;
import com.becam.android.app.fragment.SettingFragment;
import com.becam.android.app.models.SettingListModel;

import java.util.ArrayList;

/**
 * Created by Ayadi on 2/6/2018.
 */

public class SettingAdapter extends RecyclerView.Adapter<SettingAdapter.MenuItemViewHolder> {

    Context context;
    ArrayList<SettingListModel> resourcelList;


    public SettingAdapter(Context context, ArrayList<SettingListModel> ResourcelList) {
        this.context = context;
        resourcelList = ResourcelList;
    }

    @Override
    public MenuItemViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.setting_list_item_row, parent, false);
        MenuItemViewHolder viewholder = new MenuItemViewHolder(view);
        return viewholder;
    }

    @Override
    public void onBindViewHolder(MenuItemViewHolder holder, final int position)
    {

        holder.nameTXT.setText(resourcelList.get(position).getName());


        if(resourcelList.get(position).getStatus().equalsIgnoreCase("1"))
        {
            holder.pushSwitch.setChecked(true);
        }
        else
        {
            holder.pushSwitch.setChecked(false);
        }

        holder.pushSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked)
                {
                    SettingFragment.postPushStatusReq("on",resourcelList.get(position).getId());
                }
                else
                {
                    SettingFragment.postPushStatusReq("off",resourcelList.get(position).getId());
                }
            }
        });


    }

    @Override
    public int getItemCount() {
        return resourcelList.size();
    }

    public static class MenuItemViewHolder extends RecyclerView.ViewHolder
    {

        LinearLayout mainLO;
        TextView nameTXT ;
        Switch pushSwitch;




        public MenuItemViewHolder(View itemView)
        {
            super(itemView);
            mainLO = itemView.findViewById(R.id.mainLO);
            pushSwitch= itemView.findViewById(R.id.pushSwitch);
            nameTXT = itemView.findViewById(R.id.nameTXT);


        }


    }




}
