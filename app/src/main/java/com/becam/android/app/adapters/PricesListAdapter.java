package com.becam.android.app.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.becam.android.app.R;
import com.becam.android.app.fragment.HomeFragment;
import com.becam.android.app.models.CategoryModel;
import com.becam.android.app.models.PricesListModel;
import com.becam.android.app.models.SharesModel;

import java.util.ArrayList;

/**
 * Created by Ayadi on 2/6/2018.
 */

public class PricesListAdapter extends RecyclerView.Adapter<PricesListAdapter.MenuItemViewHolder> {

    Context context;
    ArrayList<PricesListModel> resourcelList;


    public PricesListAdapter(Context context, ArrayList<PricesListModel> ResourcelList) {
        this.context = context;
        resourcelList = ResourcelList;
    }

    @Override
    public MenuItemViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.prices_list_item_row, parent, false);
        MenuItemViewHolder viewholder = new MenuItemViewHolder(view);
        return viewholder;
    }

    @Override
    public void onBindViewHolder(MenuItemViewHolder holder, final int position) {

        holder.nameTXT.setText(resourcelList.get(position).getName());


        holder.mainLO.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {


              //  HomeFragment.goToCategoryDetails(resourcelList.get(position).getFlag(),resourcelList.get(position).getDate());
                HomeFragment.goToCategoryDetails(resourcelList.get(position).getFlag(),"",resourcelList.get(position).getName());
            }
        });



    }

    @Override
    public int getItemCount() {
        return resourcelList.size();
    }

    public static class MenuItemViewHolder extends RecyclerView.ViewHolder
    {

        LinearLayout mainLO;
        TextView nameTXT ;




        public MenuItemViewHolder(View itemView)
        {
            super(itemView);
            mainLO = itemView.findViewById(R.id.mainLO);
            nameTXT = itemView.findViewById(R.id.nameTXT);


        }


    }




}
