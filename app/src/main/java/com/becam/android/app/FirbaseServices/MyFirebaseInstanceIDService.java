package com.becam.android.app.FirbaseServices;

import android.content.Context;
import android.content.Intent;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.becam.android.app.activity.LoginSubscriptionActivity;
import com.becam.android.app.activity.MainScreenActivity;
import com.becam.android.app.others.SharedPrefsUtils;
import com.becam.android.app.others.appConstants;
import com.becam.android.app.others.urlClass;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;

import org.json.JSONArray;
import org.json.JSONObject;


/**
 * Created by Ayadi on 3/20/2017.
 */



public class MyFirebaseInstanceIDService extends FirebaseInstanceIdService {
    private static final String TAG = MyFirebaseInstanceIDService.class.getSimpleName();
    SharedPrefsUtils sharedPref;

    @Override
    public void onTokenRefresh()
    {
        super.onTokenRefresh();
        final String refreshedToken = FirebaseInstanceId.getInstance().getToken();
        sharedPref.setStringPreference(getApplicationContext(), appConstants.deviceToken_KEY,refreshedToken);

        final String mobile = sharedPref.getStringPreference(getApplicationContext(), appConstants.userMobile_KEY);

        if(!mobile.equalsIgnoreCase(""))
        {
            new Thread(new Runnable() {
                @Override
                public void run()
                {
                    try {


                        String url= urlClass.updateTokenURL+mobile+"&token="+refreshedToken;
                        postTokenRequest(url, null);
                    }
                    catch (Exception xx){}
                }
            }).start();
        }





        Log.d("tokenDevice", refreshedToken);
        // Saving reg id to shared preferences
        //storeRegIdInPref(refreshedToken);

        // sending reg id to your server
        //sendRegistrationToServer(refreshedToken);

        // Notify UI that registration has completed, so the progress indicator can be hidden.
        Intent registrationComplete = new Intent(ConfigFCM.REGISTRATION_COMPLETE);
        registrationComplete.putExtra("token", refreshedToken);
        LocalBroadcastManager.getInstance(this).sendBroadcast(registrationComplete);
    }

    private void postTokenRequest(String urlPost, final JSONObject jsonObject) {

        final RequestQueue queue;
        StringRequest stringRequest = null;

        try {

            queue = Volley.newRequestQueue(getApplicationContext());

            stringRequest = new StringRequest(Request.Method.GET, urlPost,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response)
                        {
                            try
                            {
                                if(response!=null)
                                {
                                    appConstants.stopSpinWheel();
                                }


                            }
                            catch (Exception e)
                            {
                                e.printStackTrace();
                                appConstants.stopSpinWheel();
                            }


                        }
                    }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(final VolleyError error) {

                    appConstants.stopSpinWheel();


                }


            });

            stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                    appConstants.appTimeOut,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

            queue.add(stringRequest);


        } catch (Exception e) {
            appConstants.stopSpinWheel();
            e.toString();
        }



    }
}