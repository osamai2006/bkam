package com.becam.android.app.fragment;

import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.becam.android.app.R;
import com.becam.android.app.activity.MainScreenActivity;
import com.becam.android.app.adapters.FoodDetailsAdapter;
import com.becam.android.app.models.FoodDetailsChartModel;
import com.becam.android.app.models.FoodDetailsModel;
import com.becam.android.app.others.appConstants;
import com.becam.android.app.others.urlClass;
import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.components.AxisBase;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.formatter.IAxisValueFormatter;
import com.github.mikephil.charting.formatter.IFillFormatter;
import com.github.mikephil.charting.interfaces.dataprovider.LineDataProvider;
import com.github.mikephil.charting.interfaces.datasets.IDataSet;
import com.github.mikephil.charting.interfaces.datasets.ILineDataSet;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by HP on 5/3/2018.
 */

public class FoodDetailsFragment extends Fragment  {
    public static MainScreenActivity myContext;

    public  static String itemID="";
    public  static String itemName="";
    public  static String lastUpdateDate="";
    public  static String country="";


    View view;


    FoodDetailsModel model;
    ArrayList<FoodDetailsModel> detailsArrayList;
    FoodDetailsAdapter detailsAdapter;

    ArrayList<FoodDetailsChartModel> chartArrayList;
    FoodDetailsChartModel chartModel;


    TextView nameTXT,lastUpdateTXT;
    LinearLayout yesterdayLO,weekLO,monthLO;
    RecyclerView foodDetailsLV;
    LineChart mChart;


    @Override
    public void onAttach(Context context) {
        myContext=(MainScreenActivity) getActivity();
        super.onAttach(context);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.food_details_fragment, container, false);

        MainScreenActivity.screenTitleTXT.setText(itemName);
        final SwipeRefreshLayout  swipeToRefreshLayout = (SwipeRefreshLayout) view.findViewById(R.id.swipeTorefresh);
        foodDetailsLV=view.findViewById(R.id.foodDetailsLV);
        lastUpdateTXT=view.findViewById(R.id.lastUpdate_TXT);
        nameTXT=view.findViewById(R.id.nameTXT);

        yesterdayLO=view.findViewById(R.id.yesterdayLO);
        weekLO=view.findViewById(R.id.weekLO);
        monthLO=view.findViewById(R.id.monthLO);

        lastUpdateTXT.setText(String.valueOf(lastUpdateDate));
        nameTXT.setText(itemName);

        mChart = view.findViewById(R.id.chart);

        swipeToRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                swipeToRefreshLayout.setRefreshing(true);

                appConstants.startSpinwheel(getActivity(), false, true);
                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        try {

                            String  urlData= urlClass.getFoodDetailsURL+itemID+"&country"+country;
                            getFoodDetailsReq(urlData, null);

                        } catch (Exception xx) {
                        }
                    }
                }).start();


                appConstants.startSpinwheel(getActivity(), false, true);
                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        try {

                            String  urlData= urlClass.getFoodChartURL+itemID+"&period=1";
                            getFoodChartReq(urlData, null);


                        } catch (Exception xx) {
                        }
                    }
                }).start();

                swipeToRefreshLayout.setRefreshing(false);

            }
        });

        appConstants.startSpinwheel(getActivity(), false, true);
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {

                    String  urlData= urlClass.getFoodDetailsURL+itemID+"&country"+country;
                        getFoodDetailsReq(urlData, null);

                } catch (Exception xx) {
                }
            }
        }).start();


        appConstants.startSpinwheel(getActivity(), false, true);
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {

                    String  urlData= urlClass.getFoodChartURL+itemID+"&period=1";
                    getFoodChartReq(urlData, null);


                } catch (Exception xx) {
                }
            }
        }).start();


        yesterdayLO.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                appConstants.startSpinwheel(getActivity(), false, true);
                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        try {

                            String  urlData= urlClass.getMaterialChartURL+itemID+"&period=1";
                            getFoodChartReq(urlData, null);


                        } catch (Exception xx) {
                        }
                    }
                }).start();

            }
        });

        weekLO.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                appConstants.startSpinwheel(getActivity(), false, true);
                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        try {

                            String  urlData= urlClass.getMaterialChartURL+itemID+"&period=7";
                            getFoodChartReq(urlData, null);


                        } catch (Exception xx) {
                        }
                    }
                }).start();

            }
        });

        monthLO.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                appConstants.startSpinwheel(getActivity(), false, true);
                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        try {

                            String  urlData= urlClass.getMaterialChartURL+itemID+"&period=30";
                            getFoodChartReq(urlData, null);


                        } catch (Exception xx) {
                        }
                    }
                }).start();

            }
        });



        return view;
    }




    private void getFoodDetailsReq(final String urlPost, final JSONObject jsonObject) {

        final RequestQueue queue;
        StringRequest stringRequest = null;

        try {

            queue = Volley.newRequestQueue(getActivity());

            stringRequest = new StringRequest(Request.Method.GET, urlPost,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response)
                        {
                            try
                            {
                                if(response!=null)
                                {
                                    appConstants.stopSpinWheel();
                                    detailsArrayList=new ArrayList<>();

                                    JSONObject mainObj = new JSONObject(response.toString());
                                    JSONArray itemsArr =mainObj.getJSONArray("items");
                                    for(int i=0;i<itemsArr.length();i++)
                                    {
                                        JSONObject obj=itemsArr.getJSONObject(i);

                                        String name=obj.getString("name");
                                        String sale=obj.getString("price");
                                        String status=obj.getString("status");
                                        String date=obj.getString("date");

                                        model=new FoodDetailsModel(name,sale,status,date);
                                        detailsArrayList.add(model);
                                    }


                                    if(detailsArrayList.size()>0) {
                                        detailsAdapter = new FoodDetailsAdapter(getActivity(), detailsArrayList);
                                        foodDetailsLV.setAdapter(detailsAdapter);
                                        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
                                        foodDetailsLV.setLayoutManager(layoutManager);
                                    }

                                }


                            }
                            catch (Exception e)
                            {
                                e.printStackTrace();
                                appConstants.stopSpinWheel();
                            }


                        }
                    }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(final VolleyError error) {

                   // Toast.makeText(getActivity(), "server error", Toast.LENGTH_SHORT).show();
                    appConstants.stopSpinWheel();


                }


            });

            stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                    appConstants.appTimeOut,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

            queue.add(stringRequest);


        } catch (Exception e) {
            appConstants.stopSpinWheel();
            e.toString();
        }



    }

    private void getFoodChartReq(final String urlPost, final JSONObject jsonObject) {

        final RequestQueue queue;
        StringRequest stringRequest = null;

        try {

            queue = Volley.newRequestQueue(getActivity());

            stringRequest = new StringRequest(Request.Method.GET, urlPost,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response)
                        {
                            try
                            {
                                if(response!=null) {
                                    appConstants.stopSpinWheel();
                                    chartArrayList = new ArrayList<>();

                                    JSONObject mainObj = new JSONObject(response.toString());
                                    JSONArray itemsArr = mainObj.getJSONArray("items");

                                    for (int i = 0; i < itemsArr.length(); i++) {
                                        JSONObject obj = itemsArr.getJSONObject(i);

                                        String name = obj.getString("name");
                                        String sale = obj.getString("price");
                                        String status = obj.getString("status");
                                        String date = obj.getString("date");

                                        chartModel = new FoodDetailsChartModel(name, sale, status, date);
                                        chartArrayList.add(chartModel);

                                    }
                                    if(chartArrayList.size()>1)
                                    {

                                        initilizeChart();
                                    }
                                    else
                                    {
                                       // Toast.makeText(getActivity(), getResources().getString(R.string.no_data), Toast.LENGTH_SHORT).show();
                                    }

                                }


                            }
                            catch (Exception e)
                            {
                                e.printStackTrace();
                                appConstants.stopSpinWheel();
                            }


                        }
                    }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(final VolleyError error) {

                  //  Toast.makeText(getActivity(), "server error", Toast.LENGTH_SHORT).show();
                    appConstants.stopSpinWheel();


                }


            });

            stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                    appConstants.appTimeOut,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

            queue.add(stringRequest);


        } catch (Exception e) {
            appConstants.stopSpinWheel();
            e.toString();
        }



    }


    private void initilizeChart()
    {

        mChart.setViewPortOffsets(0, 0, 0, 0);
        mChart.setBackgroundColor(Color.TRANSPARENT);

        // no description text
        mChart.getDescription().setEnabled(false);

        // enable touch gestures
        mChart.setTouchEnabled(true);

        // enable scaling and dragging
        mChart.setDragEnabled(true);
        mChart.setScaleEnabled(true);

        // if disabled, scaling can be done on x- and y-axis separately
        mChart.setPinchZoom(false);

        mChart.setDrawGridBackground(false);
        mChart.setMaxHighlightDistance(300);

        final HashMap<Integer, String> dateMap = new HashMap<>();
        for(int x=0;x<chartArrayList.size();x++)
        {
            String [] dateArr=chartArrayList.get(x).getDate().split(" ");
            dateMap.put(x,dateArr[0]);
        }

        XAxis x = mChart.getXAxis();
        x.setLabelCount(chartArrayList.size(), false);
        x.setTextColor(Color.WHITE);
        x.setTextSize(9f);
        x.setPosition(XAxis.XAxisPosition.BOTTOM_INSIDE);
        x.setDrawGridLines(true);
        x.setAxisLineColor(Color.WHITE);
        x.setValueFormatter(new IAxisValueFormatter() {
            @Override
            public String getFormattedValue(float value, AxisBase axis)
            {
                // return chartArryList.get((int)value).getDate();
                return dateMap.get((int)value);
            }
        });


        YAxis y = mChart.getAxisLeft();
        y.setLabelCount(chartArrayList.size(), false);
        y.setTextColor(Color.WHITE);
        x.setTextSize(9f);
        y.setPosition(YAxis.YAxisLabelPosition.INSIDE_CHART);
        y.setDrawGridLines(true);
        y.setAxisLineColor(Color.WHITE);

        mChart.getAxisRight().setEnabled(false);

        // add data
        setData();

        mChart.getLegend().setEnabled(false);

        mChart.animateXY(1000, 1000);

        // dont forget to refresh the drawing
        mChart.invalidate();

    }
    private void setData() {

        ArrayList<Entry> yVals = new ArrayList<Entry>();

        for (int i = 0; i < chartArrayList.size(); i++) {
            float val =Float.valueOf(chartArrayList.get(i).getPrice());
            yVals.add(new Entry(i, val));
        }

        LineDataSet set1;

        if (mChart.getData() != null &&
                mChart.getData().getDataSetCount() > 0) {
            set1 = (LineDataSet)mChart.getData().getDataSetByIndex(0);
            set1.setValues(yVals);
            mChart.getData().notifyDataChanged();
            mChart.notifyDataSetChanged();
        } else {
            // create a dataset and give it a type
            set1 = new LineDataSet(yVals, " ");

            set1.setMode(LineDataSet.Mode.CUBIC_BEZIER);
            set1.setCubicIntensity(0.2f);
            //set1.setDrawFilled(true);
            set1.setDrawCircles(false);
            set1.setLineWidth(1.0f);
            set1.setCircleRadius(5f);
            set1.setCircleColor(Color.WHITE);
            set1.setHighLightColor(Color.rgb(244, 117, 117));
            set1.setColor(Color.WHITE);
            set1.setFillColor(Color.WHITE);
            set1.setFillAlpha(100);
            set1.setDrawHorizontalHighlightIndicator(false);
            set1.setFillFormatter(new IFillFormatter() {
                @Override
                public float getFillLinePosition(ILineDataSet dataSet, LineDataProvider dataProvider) {
                    return -10;
                }
            });

            // create a data object with the datasets
            LineData data = new LineData(set1);
            data.setValueTextSize(9f);
            data.setDrawValues(false);

            // set data
            mChart.setData(data);

            for (IDataSet set : mChart.getData().getDataSets())
            { set.setDrawValues(!set.isDrawValuesEnabled());
                set.setValueTextColor(Color.WHITE);
            }


            List<ILineDataSet> sets = mChart.getData().getDataSets();
            for (ILineDataSet iSet : sets) {

                LineDataSet set = (LineDataSet) iSet;

                if (set.isDrawFilledEnabled())
                    set.setDrawFilled(false);
                else
                    set.setDrawFilled(true);
            }


            for (ILineDataSet iSet : sets) {

                LineDataSet set = (LineDataSet) iSet;
                if (set.isDrawCirclesEnabled())
                    set.setDrawCircles(false);
                else
                    set.setDrawCircles(true);
            }

            mChart.invalidate();
        }
    }
}
