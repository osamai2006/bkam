package com.becam.android.app.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.becam.android.app.R;
import com.becam.android.app.fragment.HomeFragment;
import com.becam.android.app.models.SharesModel;

import java.util.ArrayList;

/**
 * Created by Ayadi on 2/6/2018.
 */

public class SharesAdapter extends RecyclerView.Adapter<SharesAdapter.MenuItemViewHolder> {

    Context context;
    ArrayList<SharesModel> resourcelList;


    public SharesAdapter(Context context, ArrayList<SharesModel> ResourcelList) {
        this.context = context;
        resourcelList = ResourcelList;
    }

    @Override
    public MenuItemViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.shares_item_row, parent, false);
        MenuItemViewHolder viewholder = new MenuItemViewHolder(view);
        return viewholder;
    }

    @Override
    public void onBindViewHolder(MenuItemViewHolder holder, final int position) {

        holder.share_nameTXT.setText(resourcelList.get(position).getName());

        holder.otherPriceTXT.setVisibility(View.GONE);

        if(resourcelList.get(position).getPrice().equalsIgnoreCase(""))
        {
            holder.priceTXT.setVisibility(View.GONE);
        }
        else
        {
            holder.priceTXT.setText( "LYD      "+resourcelList.get(position).getPrice());
        }

        if(resourcelList.get(position).getSale().equalsIgnoreCase(""))
        {
            holder.saleTXT.setVisibility(View.GONE);
        }
        else
        {
            holder.saleTXT.setText("LYD      "+resourcelList.get(position).getSale() );
        }

        if(resourcelList.get(position).getBuy().equalsIgnoreCase(""))
        {
            holder.buyTXT.setVisibility(View.GONE);
        }
        else
        {
            holder.buyTXT.setText("LYD      "+resourcelList.get(position).getBuy() );
        }

        if(!resourcelList.get(position).getOpenPrice().equalsIgnoreCase(""))
        {
            holder.priceTXT.setVisibility(View.VISIBLE);
            holder.saleTXT.setVisibility(View.VISIBLE);
            holder.buyTXT.setVisibility(View.VISIBLE);
            holder.otherPriceTXT.setVisibility(View.VISIBLE);
            holder.shareStatusIMG.setVisibility(View.GONE);

            holder.priceTXT.setGravity(Gravity.LEFT);
            holder.saleTXT.setGravity(Gravity.LEFT);
            holder.buyTXT.setGravity(Gravity.LEFT);
            holder.otherPriceTXT.setGravity(Gravity.LEFT);

            holder.share_nameTXT.setText("Bitcoin");
            holder.priceTXT.setText("Open    "+resourcelList.get(position).getOpenPrice());
            holder.saleTXT.setText ("High      "+resourcelList.get(position).getHighPrice());
            holder.buyTXT.setText  ("Low      "+resourcelList.get(position).getLowPrice());
            holder.otherPriceTXT.setText("Close    "+resourcelList.get(position).getClosePrice());
        }


        if (resourcelList.get(position).getStatus().equalsIgnoreCase("fixed"))
        {

            holder.shareMainLO.setBackgroundResource(R.drawable.border_layout_kohly);
            holder.shareStatusIMG.setImageResource(R.drawable.fixed);

        }else  if (resourcelList.get(position).getStatus().equalsIgnoreCase("up"))
        {
            holder.shareMainLO.setBackgroundResource(R.drawable.border_layout_green);
            holder.shareStatusIMG.setImageResource(R.drawable.up_white);

        }else  if (resourcelList.get(position).getStatus().equalsIgnoreCase("down"))
        {
            holder.shareMainLO.setBackgroundResource(R.drawable.border_layout_red);
            holder.shareStatusIMG.setImageResource(R.drawable.down_white);
        }
        else
        {
            holder.shareMainLO.setBackgroundResource(R.drawable.border_layout_kohly);
            holder.shareStatusIMG.setImageResource(R.drawable.fixed);
        }



        holder.shareMainLO.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {

//                String id=resourcelList.get(position).getId();
//                String name=resourcelList.get(position).getName();
//                String flag=resourcelList.get(position).getFlag();
//
//                HomeFragment.goToShareDetails(id,name,flag);

                HomeFragment.goToCategoryDetails(resourcelList.get(position).getFlag(),resourcelList.get(position).getDate(),resourcelList.get(position).getName());
            }
        });



    }

    @Override
    public int getItemCount() {
        return resourcelList.size();
    }

    public static class MenuItemViewHolder extends RecyclerView.ViewHolder
    {

        LinearLayout shareMainLO;
        TextView share_nameTXT , priceTXT,saleTXT,buyTXT,otherPriceTXT ;
        ImageView shareStatusIMG;



        public MenuItemViewHolder(View itemView)
        {
            super(itemView);
            shareMainLO = itemView.findViewById(R.id.shareMainLO);
            share_nameTXT = itemView.findViewById(R.id.share_nameTXT);
            priceTXT = itemView.findViewById(R.id.priceTXT);
            saleTXT = itemView.findViewById(R.id.saleTXT);
            buyTXT = itemView.findViewById(R.id.buyTXT);
            otherPriceTXT = itemView.findViewById(R.id.otherPriceTXT);
            shareStatusIMG= itemView.findViewById(R.id.shareStatusIMG);
//            shareStatusIMG2= itemView.findViewById(R.id.shareStatusIMG2);

        }


    }




}
