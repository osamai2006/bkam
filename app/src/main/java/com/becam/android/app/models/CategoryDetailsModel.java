package com.becam.android.app.models;

/**
 * Created by HP on 3/25/2018.
 */

public class CategoryDetailsModel {

    private String id;
    private String item_id;
    private String name;
    private String symbol;
    private String price;
    private String sell;
    private String buy;
    private String status;
    private String date;
    private String flag;
    private String flagImg;
    private String measurment;
    private String country;
    private String weight;

    public CategoryDetailsModel(String id, String item_id, String name, String symbol, String price, String sell, String buy, String status, String date, String flag, String flagImg, String measurment, String country, String weight) {
        this.id = id;
        this.item_id = item_id;
        this.name = name;
        this.symbol = symbol;
        this.price = price;
        this.sell = sell;
        this.buy = buy;
        this.status = status;
        this.date = date;
        this.flag = flag;
        this.flagImg = flagImg;
        this.measurment = measurment;
        this.country = country;
        this.weight = weight;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getItem_id() {
        return item_id;
    }

    public void setItem_id(String item_id) {
        this.item_id = item_id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSymbol() {
        return symbol;
    }

    public void setSymbol(String symbol) {
        this.symbol = symbol;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getSell() {
        return sell;
    }

    public void setSell(String sell) {
        this.sell = sell;
    }

    public String getBuy() {
        return buy;
    }

    public void setBuy(String buy) {
        this.buy = buy;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getFlag() {
        return flag;
    }

    public void setFlag(String flag) {
        this.flag = flag;
    }

    public String getFlagImg() {
        return flagImg;
    }

    public void setFlagImg(String flagImg) {
        this.flagImg = flagImg;
    }

    public String getMeasurment() {
        return measurment;
    }

    public void setMeasurment(String measurment) {
        this.measurment = measurment;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getWeight() {
        return weight;
    }

    public void setWeight(String weight) {
        this.weight = weight;
    }
}
