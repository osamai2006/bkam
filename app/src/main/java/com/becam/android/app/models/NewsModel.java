package com.becam.android.app.models;

public class NewsModel {

    private String id;
    private String title;
    private String shortDesc;
    private String image;
    private String date;

    public NewsModel(String id, String title, String shortDesc, String image, String date) {
        this.id = id;
        this.title = title;
        this.shortDesc = shortDesc;
        this.image = image;
        this.date = date;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getShortDesc() {
        return shortDesc;
    }

    public void setShortDesc(String shortDesc) {
        this.shortDesc = shortDesc;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }
}
