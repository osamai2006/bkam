package com.becam.android.app.models;

import org.json.JSONArray;

/**
 * Created by HP on 3/25/2018.
 */

public class SettingListModel {

    private String id;
    private String name;
    private String status;

    public SettingListModel(String id, String name, String status) {
        this.id = id;
        this.name = name;
        this.status = status;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }


    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
