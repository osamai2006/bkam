package com.becam.android.app.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.becam.android.app.R;
import com.becam.android.app.fragment.HomeFragment;
import com.becam.android.app.models.CategoryDetailsModel;
import com.becam.android.app.models.SharesModel;
import com.becam.android.app.others.appConstants;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;

import java.util.ArrayList;

/**
 * Created by Ayadi on 2/6/2018.
 */

public class CatDetailsAdapter extends RecyclerView.Adapter<CatDetailsAdapter.MenuItemViewHolder> {

    Context context;
    ArrayList<CategoryDetailsModel> resourcelList;

    ArrayList<SharesModel> sharesModelArrayList;
    SharesModel sharesModel;
    SharesAdapter sharesAdapter;



    public CatDetailsAdapter(Context context, ArrayList<CategoryDetailsModel> ResourcelList) {
        this.context = context;
        resourcelList = ResourcelList;
    }

    @Override
    public MenuItemViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.category_details_item_row, parent, false);
        MenuItemViewHolder viewholder = new MenuItemViewHolder(view);
        return viewholder;
    }

    @Override
    public void onBindViewHolder(MenuItemViewHolder holder, final int position) {

        try {





            if(resourcelList.get(position).getCountry().equalsIgnoreCase(""))
            {

                holder.nameTXT.setText(resourcelList.get(position).getName());
            }
            else
            {
                holder.nameTXT.setText(resourcelList.get(position).getName()+
                        " : "+resourcelList.get(position).getCountry()+
                        " "+resourcelList.get(position).getMeasurment());
            }


            if(!resourcelList.get(position).getWeight().equalsIgnoreCase(""))
            {

                holder.nameTXT.setText(resourcelList.get(position).getName()+" "+resourcelList.get(position).getWeight());
            }


            holder.priceTXT.setText(resourcelList.get(position).getPrice());
            holder.sellTXT.setText(resourcelList.get(position).getSell());
            holder.buyTXT.setText(resourcelList.get(position).getBuy());
            holder.symbolTXT.setText(resourcelList.get(position).getSymbol());
            holder.measurmnetTXT.setText(resourcelList.get(position).getMeasurment());
            holder.countryTXT.setText(resourcelList.get(position).getCountry());
            //holder.lastUpdateTXT.setText(resourcelList.get(position).getDate());
            Glide.with(context).load(resourcelList.get(position).getFlagImg()).apply(RequestOptions.circleCropTransform()).into(holder.flagImg);


            //Picasso.with(context).load(resourcelList.get(position).getStatus()).into(holder.statusImg);

            if (resourcelList.get(position).getPrice().equalsIgnoreCase(""))
            {
                holder.priceTXT.setVisibility(View.GONE);
            }
            if (resourcelList.get(position).getSell().equalsIgnoreCase(""))
            {
                holder.sellTXT.setVisibility(View.GONE);
            }
            if (resourcelList.get(position).getBuy().equalsIgnoreCase(""))
            {
                holder.buyTXT.setVisibility(View.INVISIBLE);
            }


            if (resourcelList.get(position).getCountry().equalsIgnoreCase(""))
            {
                holder.countryTXT.setVisibility(View.GONE);
            }

            if (resourcelList.get(position).getMeasurment().equalsIgnoreCase(""))
            {
                holder.measurmnetTXT.setVisibility(View.GONE);
            }

            if (resourcelList.get(position).getMeasurment().equalsIgnoreCase("")&&
                    resourcelList.get(position).getCountry().equalsIgnoreCase(""))
            {
                holder.addtionalInfoLO.setVisibility(View.GONE);
            }


            if (resourcelList.get(position).getStatus().equalsIgnoreCase("fixed")) {
                // holder.shareStatusIMG.setImageResource(R.drawable.up_arrow);

                holder.statusImg.setImageResource(R.drawable.fixed);

            } else if (resourcelList.get(position).getStatus().equalsIgnoreCase("up")) {

                holder.statusImg.setImageResource(R.drawable.up);


            } else if (resourcelList.get(position).getStatus().equalsIgnoreCase("down")) {

                holder.statusImg.setImageResource(R.drawable.down);

            } else {
                holder.statusImg.setImageResource(R.drawable.fixed);

            }


            holder.mainLO.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    String id = resourcelList.get(position).getId();
                    String name = resourcelList.get(position).getName();
                    String flag = resourcelList.get(position).getFlag();
                    String date = resourcelList.get(position).getDate();
                    String country = resourcelList.get(position).getCountry();
                    String measurment = resourcelList.get(position).getMeasurment();

                    if(appConstants.categoryTypeFlag.equalsIgnoreCase("material")
                            ||appConstants.categoryTypeFlag.equalsIgnoreCase("food")
                            ||appConstants.categoryTypeFlag.equalsIgnoreCase("veg")
                            ||appConstants.categoryTypeFlag.equalsIgnoreCase("fruit")
                            ||appConstants.categoryTypeFlag.equalsIgnoreCase("meat")
                            ||appConstants.categoryTypeFlag.equalsIgnoreCase("wmeat"))
                    {
                        id= resourcelList.get(position).getItem_id();
                    }


                    HomeFragment.goToShareDetails(id, name+" "+country, flag, date,country,measurment);


                }
            });

        }
        catch (Exception xx)
        {

        }


    }

    @Override
    public int getItemCount() {
        return resourcelList.size();
    }

    public static class MenuItemViewHolder extends RecyclerView.ViewHolder
    {

        TextView nameTXT ,priceTXT, symbolTXT,sellTXT,buyTXT,countryTXT,measurmnetTXT ;
        ImageView statusImg,flagImg;
        LinearLayout mainLO,addtionalInfoLO;


        public MenuItemViewHolder(View itemView)
        {
            super(itemView);
            nameTXT = itemView.findViewById(R.id.nameTXT);
            symbolTXT = itemView.findViewById(R.id.symbolTXT);
            sellTXT = itemView.findViewById(R.id.sellTXT);
            buyTXT= itemView.findViewById(R.id.buyTXT);
            priceTXT= itemView.findViewById(R.id.priceTXT);
            statusImg = itemView.findViewById(R.id.statusImg);
            flagImg= itemView.findViewById(R.id.flagImg);
            mainLO = itemView.findViewById(R.id.mainLO);
            measurmnetTXT= itemView.findViewById(R.id.measurmnetTXT);
            countryTXT= itemView.findViewById(R.id.countryTXT);
            addtionalInfoLO= itemView.findViewById(R.id.addtionalInfoLO);
            //lastUpdateTXT= itemView.findViewById(R.id.lastUpdateTXT);


        }


    }




}
