package com.becam.android.app.activity;

import android.Manifest;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.telephony.TelephonyManager;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.becam.android.app.R;
import com.becam.android.app.others.SharedPrefsUtils;
import com.becam.android.app.others.appConstants;
import com.becam.android.app.others.urlClass;
import com.daimajia.slider.library.SliderTypes.BaseSliderView;
import com.daimajia.slider.library.SliderTypes.DefaultSliderView;
import com.google.android.gms.common.ConnectionResult;

import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.Scope;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.tasks.OnSuccessListener;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.List;
import java.util.Locale;


import static android.provider.UserDictionary.Words.APP_ID;

public class LoginSubscriptionActivity extends AppCompatActivity {
    EditText mobileTXT, emailTXT, zipCodeTXT;
    Button loginBTN;
    SharedPrefsUtils sharedPref;
    boolean isLogedIn = false;

//    public static String currntlatti = "31.9570";
//    public static String currntlongi = "35.8690";
    //LocationManager locationManager;
  //  private GoogleApiClient googleApiClient;



//    @Override
//    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
//        if(requestCode==11)
//        {
//            try {
//
//                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED)
//                {
//
//                    googleApiClient = new GoogleApiClient.Builder(this, this, this).addApi(LocationServices.API).build();
//                    googleApiClient.registerConnectionCallbacks(this);
//
//                }
//                else
//                {
//
//                    finish();
//                }
//
//            }
//            catch (Exception xx){}
//        }
//
//
//    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login_subsription_layout);
       // locationManager = (LocationManager) LoginSubscriptionActivity.this.getSystemService(Context.LOCATION_SERVICE);


        isLogedIn = sharedPref.getBooleanPreference(getApplicationContext(), appConstants.isLoggedIn, false);

        if (isLogedIn) {


            Intent i = new Intent(getApplicationContext(), MainScreenActivity.class);
            startActivity(i);
            finish();
        }


        mobileTXT = findViewById(R.id.mobileNumTXT);
        emailTXT = findViewById(R.id.emailTXT);
        loginBTN = findViewById(R.id.loginBTN);
        zipCodeTXT = findViewById(R.id.zipCodeTXT);

        TelephonyManager tm = (TelephonyManager) this.getSystemService(Context.TELEPHONY_SERVICE);
        String countryCodeValue = tm.getSimCountryIso();
        appConstants.zipCode = countryCodeValue;
        zipCodeTXT.setText(appConstants.zipCode);


        loginBTN.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (checkRule()) {

                    try {

                        new Thread(new Runnable() {
                            @Override
                            public void run() {

                                    String token = sharedPref.getStringPreference(getApplicationContext(), appConstants.deviceToken_KEY);

                                    String urlData = urlClass.postUserInfoURL + "&phoneNumber=" + mobileTXT.getText().toString() + "&email=" + emailTXT.getText().toString() + "&token=" + token +
                                            "&countryCode=" + zipCodeTXT.getText().toString().trim() +
                                            //"&latlong=" + appConstants.longLatValue +
                                            "&serialNumber=" + appConstants.serialNum;
                                    loginReq(urlData, null);

                            }
                        }).start();

                    } catch (Exception xx) {
                    }

                }

            }
        });



    }



    private  boolean checkRule()
    {
       if(mobileTXT.getText().toString().equalsIgnoreCase(""))
        {
            Toast.makeText(getApplicationContext(),"من فضلك تأكد من رقم الهاتف",Toast.LENGTH_LONG).show();
            return  false;
        }
        if(mobileTXT.getText().toString().length()<10 )
        {
            Toast.makeText(getApplicationContext(),"يجب ان يتكون رقم الهاتف من 10 خانات",Toast.LENGTH_LONG).show();
            return  false;
        }
        if(!mobileTXT.getText().toString().startsWith("0") )
        {
            Toast.makeText(getApplicationContext(),"يجب ان يبدأ رقم الهاتف ب 0",Toast.LENGTH_LONG).show();
            return  false;
        }
        if(emailTXT.getText().toString().equalsIgnoreCase("") )
        {
            Toast.makeText(getApplicationContext(),"من فضلك تأكد من البريد الاليكتروني",Toast.LENGTH_LONG).show();
            return  false;
        }
        if (!emailTXT.getText().toString().trim().matches(appConstants.email_pattern)) {
            Toast.makeText(getApplicationContext(),"من فضلك تأكد من صيغة البريد الاليكتروني",Toast.LENGTH_LONG).show();
            return  false;
        }

        return  true;
    }

    private void loginReq(String urlPost, final JSONObject jsonObject) {

        final RequestQueue queue;
        StringRequest stringRequest = null;

        try {

            queue = Volley.newRequestQueue(LoginSubscriptionActivity.this);

            stringRequest = new StringRequest(Request.Method.GET, urlPost,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response)
                        {
                            try
                            {
                                if(response!=null)
                                {
                                    response=new String(response.getBytes("ISO-8859-1"), "UTF-8");
                                    appConstants.stopSpinWheel();

                                    JSONArray obj=new JSONArray(response.toString());
                                    JSONObject result=obj.getJSONObject(0);
                                    if(result.getString("status").equalsIgnoreCase("success"))
                                    {
                                        sharedPref.setStringPreference(getApplicationContext(), appConstants.userEmail_KEY,emailTXT.getText().toString().trim());
                                        sharedPref.setStringPreference(getApplicationContext(), appConstants.userID_KEY,"");
                                        sharedPref.setStringPreference(getApplicationContext(), appConstants.userMobile_KEY,mobileTXT.getText().toString().trim());
                                        sharedPref.setBooleanPreference(getApplicationContext(), appConstants.isLoggedIn,true);

                                        Intent i = new Intent(getApplicationContext(), MainScreenActivity.class);
                                        startActivity(i);
                                        finish();

                                    }
                                    else
                                    {

                                        Toast.makeText(LoginSubscriptionActivity.this, result.getString("status"), Toast.LENGTH_SHORT).show();

                                    }



                                }


                            }
                            catch (Exception e)
                            {
                                e.printStackTrace();
                                appConstants.stopSpinWheel();
                                Toast.makeText(LoginSubscriptionActivity.this, "Theres An Error, Try Later", Toast.LENGTH_SHORT).show();
                            }


                        }
                    }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(final VolleyError error) {

                    //Toast.makeText(LoginSubscriptionActivity.this, "server error", Toast.LENGTH_SHORT).show();
                    appConstants.stopSpinWheel();


                }


            });

            stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                    appConstants.appTimeOut,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

            queue.add(stringRequest);


        } catch (Exception e) {
            appConstants.stopSpinWheel();
            e.toString();
        }




    }

}
