package com.becam.android.app.fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.becam.android.app.R;
import com.becam.android.app.activity.MainScreenActivity;
import com.becam.android.app.others.appConstants;
import com.becam.android.app.others.urlClass;
import com.daimajia.slider.library.SliderTypes.BaseSliderView;
import com.daimajia.slider.library.SliderTypes.DefaultSliderView;

import org.json.JSONArray;
import org.json.JSONObject;

/**
 * Created by HP on 5/9/2017.
 */

public class TermsConditionFragment extends Fragment {

    View view;
    TextView termsTXT,titleTXT;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        view = inflater.inflate(R.layout.terms_layout, container, false);
        termsTXT=view.findViewById(R.id.termsTXT);
        titleTXT=view.findViewById(R.id.titleTXT);
        MainScreenActivity.screenTitleTXT.setText(getResources().getString(R.string.terms_dr));

        appConstants.startSpinwheel(getActivity(), false, true);
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    getTermsReq(urlClass.getTermsPagesURL, null);
                } catch (Exception xx) {
                }
            }
        }).start();


        return view;
    }

    private void getTermsReq(String urlPost, final JSONObject jsonObject) {

        final RequestQueue queue;
        StringRequest stringRequest = null;

        try {

            queue = Volley.newRequestQueue(getActivity());

            stringRequest = new StringRequest(Request.Method.GET, urlPost,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response)
                        {
                            try
                            {
                                if(response!=null)
                                {
                                    appConstants.stopSpinWheel();
                                    JSONArray arr = new JSONArray(response.toString());

                                        JSONObject obj=arr.getJSONObject(1);

                                        String title =obj.getString("title");
                                        String content =obj.getString("content");
                                        termsTXT.setText(content);
                                        titleTXT.setText(title);

                                    }




                            }
                            catch (Exception e)
                            {
                                e.printStackTrace();
                                appConstants.stopSpinWheel();
                            }


                        }
                    }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(final VolleyError error) {

                 //   Toast.makeText(getActivity(), "server error", Toast.LENGTH_SHORT).show();
                    appConstants.stopSpinWheel();


                }


            });

            stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                    appConstants.appTimeOut,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

            queue.add(stringRequest);


        } catch (Exception e) {
            appConstants.stopSpinWheel();
            e.toString();
        }



    }
}
