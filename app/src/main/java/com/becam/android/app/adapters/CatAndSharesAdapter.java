package com.becam.android.app.adapters;

import android.content.Context;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import com.becam.android.app.R;
import com.becam.android.app.fragment.HomeFragment;
import com.becam.android.app.models.CategoryModel;
import com.becam.android.app.models.SharesModel;


import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Ayadi on 2/6/2018.
 */

public class CatAndSharesAdapter extends RecyclerView.Adapter<CatAndSharesAdapter.MenuItemViewHolder> {

    Context context;
    ArrayList<CategoryModel> resourcelList;

    ArrayList<SharesModel> sharesModelArrayList;
    SharesModel sharesModel;
    SharesAdapter sharesAdapter;



    public CatAndSharesAdapter(Context context, ArrayList<CategoryModel> ResourcelList) {
        this.context = context;
        resourcelList = ResourcelList;
    }

    @Override
    public MenuItemViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.category_and_shares_item_row, parent, false);
        MenuItemViewHolder viewholder = new MenuItemViewHolder(view);
        return viewholder;
    }

    @Override
    public void onBindViewHolder(MenuItemViewHolder holder, final int position) {

        holder.category_nameTXT.setText(resourcelList.get(position).getName());

        holder.category_nameTXT.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                HomeFragment.goToCategoryDetails(resourcelList.get(position).getFlag(),"",resourcelList.get(position).getName());
            }
        });

        if(resourcelList.get(position).getSharesArr().length()>0)
        {
            sharesModelArrayList=new ArrayList<>();
            JSONArray array=resourcelList.get(position).getSharesArr();
            String flag=resourcelList.get(position).getFlag();
            for(int x=0;x<array.length();x++)
            {
                try
                {
                    JSONObject obj = array.getJSONObject(x);

                    String id="";
                    String name="";
                    String price="";
                    String sale="";
                    String buy="";
                    String date="";
                    String openPrice="";
                    String closePrice="";
                    String highPrice="";
                    String lowPrice="";
                    String volumeBTC="";
                    String volumeCurrency="";
                    String weighted_Price="";
                    String date_price="";
                    String status="";

                    try{   id=obj.getString("id");}  catch (Exception xx){}
                    try{   name=obj.getString("name");}  catch (Exception xx){}
                    try{   price=obj.getString("price");}  catch (Exception xx){}
                    try{   sale=obj.getString("sale");}  catch (Exception xx){}
                    try{   buy=obj.getString("buy");}  catch (Exception xx){}
                    try{   date=obj.getString("date");}  catch (Exception xx){}
                    try{   openPrice=obj.getString("open_price");}  catch (Exception xx){}
                    try{   closePrice=obj.getString("close_price");}  catch (Exception xx){}
                    try{   highPrice=obj.getString("high_price");}  catch (Exception xx){}
                    try{   lowPrice=obj.getString("low_price");}  catch (Exception xx){}
                    try{   volumeBTC=obj.getString("volume_BTC");}  catch (Exception xx){}
                    try{   volumeCurrency=obj.getString("volume_Currency");}  catch (Exception xx){}
                    try{   weighted_Price=obj.getString("weighted_Price");}  catch (Exception xx){}
                    try{   date_price=obj.getString("date_Price");}  catch (Exception xx){}
                    try{   status=obj.getString("status");}  catch (Exception xx){}



                    sharesModel=new SharesModel(id,name,price,sale,buy,date,openPrice,closePrice,highPrice,lowPrice,volumeBTC,volumeCurrency,weighted_Price,date_price,status,flag);
                    sharesModelArrayList.add(sharesModel);

                }

                catch (Exception xx)
                {
                    xx.toString();
                }



            }
            sharesAdapter = new SharesAdapter(context, sharesModelArrayList);
            holder.sharesLV.setAdapter(sharesAdapter);
            LinearLayoutManager layoutManager = new LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false);
            holder.sharesLV.setLayoutManager(layoutManager);

        }


    }

    @Override
    public int getItemCount() {
        return resourcelList.size();
    }

    public static class MenuItemViewHolder extends RecyclerView.ViewHolder
    {

        TextView category_nameTXT  ;
        RecyclerView sharesLV;


        public MenuItemViewHolder(View itemView)
        {
            super(itemView);
            category_nameTXT = itemView.findViewById(R.id.category_nameTXT);
            sharesLV = itemView.findViewById(R.id.sharesRecycleView);

        }


    }




}
