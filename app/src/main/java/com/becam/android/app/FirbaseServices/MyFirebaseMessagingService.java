package com.becam.android.app.FirbaseServices;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.media.RingtoneManager;
import android.net.Uri;
import android.support.v4.app.NotificationCompat;
import android.util.Log;
import android.widget.Toast;

import com.becam.android.app.R;
import com.becam.android.app.activity.MainScreenActivity;
import com.becam.android.app.others.SharedPrefsUtils;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import org.json.JSONArray;
import org.json.JSONObject;


public class MyFirebaseMessagingService extends FirebaseMessagingService {
    private static final String TAG = MyFirebaseMessagingService.class.getSimpleName();
    SharedPrefsUtils sharedPref;
    String  body, title,id="",textToShow="";
    PendingIntent pendingIntent;

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {

        try {
            //Map<String, String> dataDic = remoteMessage.getData();

            body="";title="";id="";textToShow="";

            JSONObject obj=new JSONObject(remoteMessage.getData());
            body = obj.getString("body");
            title = obj.getString("title");
            id = obj.getString("id");

            JSONArray mainArr=new JSONArray(body);
            for (int i=0;i<mainArr.length();i++)
            {
                JSONObject bodyObj=mainArr.getJSONObject(i);

                String name=bodyObj.getString("name");
                String sale=bodyObj.getString("sale");
                textToShow=textToShow+"\r\n"+name+" : "+sale;
            }

        }
        catch (Exception xx)
        {
        }



        showNotification(textToShow,id);
    }

    private void showNotification(String messageBody,String catID) {


        try {


            if (!catID.equalsIgnoreCase(""))
            {

                Intent intent = new Intent(this, MainScreenActivity.class);
                intent.putExtra("catID", catID);
                pendingIntent = PendingIntent.getActivity(this, 0, intent, PendingIntent.FLAG_ONE_SHOT);

            }
            else
            {
                Intent intent = new Intent(this, MainScreenActivity.class);
                intent.putExtra("catID", "");
                pendingIntent = PendingIntent.getActivity(this, 0, intent, PendingIntent.FLAG_ONE_SHOT);
            }


            Bitmap notifyImage = BitmapFactory.decodeResource(getResources(), R.mipmap.icon);
            Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);

            NotificationCompat.Builder builder = new NotificationCompat.Builder(this);
            Notification notification = builder.setContentIntent(pendingIntent)
                    .setSmallIcon(R.mipmap.icon)
                    .setTicker("Bekam").setWhen(0)
                    .setAutoCancel(true)
                    .setContentTitle(title)
                    .setAutoCancel(true)
                    .setSound(defaultSoundUri)
                    .setStyle(new NotificationCompat.BigTextStyle().bigText(messageBody))
                    .setContentText(messageBody).build();

            NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {

                NotificationChannel channel = new NotificationChannel("fcm_default_channel",getString(R.string.app_name), NotificationManager.IMPORTANCE_DEFAULT);
                channel.setDescription(messageBody);
                notificationManager.createNotificationChannel(channel);

                notificationManager.createNotificationChannel(channel);
                builder.setChannelId("fcm_default_channel");

                notification = builder.setContentIntent(pendingIntent)
                        .setSmallIcon(R.mipmap.icon)
                        .setTicker("Bekam").setWhen(0)
                        .setAutoCancel(true)
                        .setContentTitle(title)
                        .setAutoCancel(true)
                        .setSound(defaultSoundUri)
                        .setStyle(new NotificationCompat.BigTextStyle().bigText(messageBody))
                        .setContentText(messageBody).build();

            }

            notificationManager.notify(0, notification);



        }
        catch (Exception xx)
        {

        }
    }

}