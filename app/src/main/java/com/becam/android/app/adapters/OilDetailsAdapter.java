package com.becam.android.app.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.becam.android.app.R;
import com.becam.android.app.models.OilDetailsChartModel;
import com.becam.android.app.models.OilDetailsModel;
import com.becam.android.app.models.SharesModel;

import java.util.ArrayList;

/**
 * Created by Ayadi on 2/6/2018.
 */

public class OilDetailsAdapter extends RecyclerView.Adapter<OilDetailsAdapter.MenuItemViewHolder> {

    Context context;
    ArrayList<OilDetailsModel> resourcelList;

    SharesModel sharesModel;
    SharesAdapter sharesAdapter;



    public OilDetailsAdapter(Context context, ArrayList<OilDetailsModel> ResourcelList) {
        this.context = context;
        resourcelList = ResourcelList;
    }

    @Override
    public MenuItemViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.oil_details_item_row, parent, false);
        MenuItemViewHolder viewholder = new MenuItemViewHolder(view);
        return viewholder;
    }

    @Override
    public void onBindViewHolder(MenuItemViewHolder holder, final int position) {

        holder.dateOilTXT.setText(resourcelList.get(position).getDate());
        holder.priceOilTXT.setText(resourcelList.get(position).getPrice());



        //Picasso.with(context).load(resourcelList.get(position).getStatus()).into(holder.statusImg);

        if (resourcelList.get(position).getStatus().equalsIgnoreCase("fixed"))
        {
            // holder.shareStatusIMG.setImageResource(R.drawable.up_arrow);

            holder.statusOilImg.setImageResource(R.drawable.fixed);

        }else  if (resourcelList.get(position).getStatus().equalsIgnoreCase("up"))
        {
            holder.statusOilImg.setImageResource(R.drawable.up);


        }else  if (resourcelList.get(position).getStatus().equalsIgnoreCase("down"))
        {
            holder.statusOilImg.setImageResource(R.drawable.down);

        }
        else
        {
            holder.statusOilImg.setImageResource(R.drawable.fixed);

        }






    }

    @Override
    public int getItemCount() {
        return resourcelList.size();
    }

    public static class MenuItemViewHolder extends RecyclerView.ViewHolder
    {

        TextView dateOilTXT ,priceOilTXT ;
        ImageView statusOilImg;



        public MenuItemViewHolder(View itemView)
        {
            super(itemView);
            dateOilTXT = itemView.findViewById(R.id.dateOilTXT);
            priceOilTXT = itemView.findViewById(R.id.priceOilTXT);
            statusOilImg = itemView.findViewById(R.id.statusOilImg);

        }


    }




}
