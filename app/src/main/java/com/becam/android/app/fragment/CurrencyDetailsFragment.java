package com.becam.android.app.fragment;

import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.becam.android.app.R;
import com.becam.android.app.activity.MainScreenActivity;
import com.becam.android.app.adapters.CurrencyDetailsAdapter;
import com.becam.android.app.models.BitCoinDetailsModel;
import com.becam.android.app.models.CurrencyDetailsChartModel;
import com.becam.android.app.models.CurrencyDetailsModel;
import com.becam.android.app.models.GoldDetailsModel;
import com.becam.android.app.models.OilDetailsModel;
import com.becam.android.app.others.appConstants;
import com.becam.android.app.others.urlClass;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.components.AxisBase;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.formatter.DefaultAxisValueFormatter;
import com.github.mikephil.charting.formatter.IAxisValueFormatter;
import com.github.mikephil.charting.formatter.IFillFormatter;
import com.github.mikephil.charting.interfaces.dataprovider.LineDataProvider;
import com.github.mikephil.charting.interfaces.datasets.IDataSet;
import com.github.mikephil.charting.interfaces.datasets.ILineDataSet;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

/**
 * Created by HP on 5/3/2018.
 */

public class CurrencyDetailsFragment extends Fragment  {
    public static MainScreenActivity myContext;

    public  static String itemID="";
    public  static String itemName="";
    public  static String lastUpdateDate="";


    View view;


    CurrencyDetailsModel currencyDetailsModel;
    RecyclerView currencyDetailsLV;
    ArrayList<CurrencyDetailsModel> arryList;

    CurrencyDetailsChartModel chartModel;
    ArrayList<CurrencyDetailsChartModel> chartArryList;
    CurrencyDetailsAdapter adapter;

    TextView nameTXT,lastUpdateTXT,openSellTXT,openBuyTXT,closeSellTXT,closeBuyTXT;
    LinearLayout yesterdayLO,weekLO,monthLO;
    ImageView flagIMG;

    LineChart mChart;

    SwipeRefreshLayout swipeToRefreshLayout;

    @Override
    public void onAttach(Context context) {
        myContext=(MainScreenActivity) getActivity();
        super.onAttach(context);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.currency_details_fragment, container, false);
        //Date currentTime = Calendar.getInstance().getTime();

        MainScreenActivity.screenTitleTXT.setText(itemName);

        swipeToRefreshLayout = (SwipeRefreshLayout) view.findViewById(R.id.swipeTorefresh);
        lastUpdateTXT=view.findViewById(R.id.lastUpdate_TXT);
        lastUpdateTXT.setText(String.valueOf(lastUpdateDate));
        nameTXT=view.findViewById(R.id.nameTXT);

        yesterdayLO=view.findViewById(R.id.yesterdayLO);
        weekLO=view.findViewById(R.id.weekLO);
        monthLO=view.findViewById(R.id.monthLO);
        openSellTXT=view.findViewById(R.id.openSellTXT);
        openBuyTXT=view.findViewById(R.id.openBuyTXT);
        closeSellTXT=view.findViewById(R.id.closeSellTXT);
        closeBuyTXT=view.findViewById(R.id.closeBuyTXT);
        mChart = view.findViewById(R.id.currencyChart);
        flagIMG = view.findViewById(R.id.flagIMG);
        currencyDetailsLV= view.findViewById(R.id.currencyDetailsLV);
        nameTXT.setText(itemName);


        swipeToRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                swipeToRefreshLayout.setRefreshing(true);

                appConstants.startSpinwheel(getActivity(), false, true);
                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        try {

                            String  urlData= urlClass.getCurencyDetailsURL+itemID;
                            getCurrencyDetailsReq(urlData, null);


                        } catch (Exception xx) {
                        }
                    }
                }).start();

                appConstants.startSpinwheel(getActivity(), false, true);
                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        try {

                            String  urlData= urlClass.getCurencyChartURL+itemID+"&period=1";
                            getCurrencyChartReq(urlData, null);


                        } catch (Exception xx) {
                        }
                    }
                }).start();
                swipeToRefreshLayout.setRefreshing(false);

            }
        });

        appConstants.startSpinwheel(getActivity(), false, true);
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {

                  String  urlData= urlClass.getCurencyDetailsURL+itemID;
                        getCurrencyDetailsReq(urlData, null);


                } catch (Exception xx) {
                }
            }
        }).start();

        appConstants.startSpinwheel(getActivity(), false, true);
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {

                    String  urlData= urlClass.getCurencyChartURL+itemID+"&period=1";
                    getCurrencyChartReq(urlData, null);


                } catch (Exception xx) {
                }
            }
        }).start();


        yesterdayLO.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                appConstants.startSpinwheel(getActivity(), false, true);
                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        try {

                            String  urlData= urlClass.getCurencyChartURL+itemID+"&period=1";
                            getCurrencyChartReq(urlData, null);


                        } catch (Exception xx) {
                        }
                    }
                }).start();

            }
        });

        weekLO.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                appConstants.startSpinwheel(getActivity(), false, true);
                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        try {

                            String  urlData= urlClass.getCurencyChartURL+itemID+"&period=7";
                            getCurrencyChartReq(urlData, null);


                        } catch (Exception xx) {
                        }
                    }
                }).start();
            }
        });

        monthLO.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                appConstants.startSpinwheel(getActivity(), false, true);
                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        try {

                            String  urlData= urlClass.getCurencyChartURL+itemID+"&period=30";
                            getCurrencyChartReq(urlData, null);


                        } catch (Exception xx) {
                        }
                    }
                }).start();

            }
        });


        return view;
    }


    private void getCurrencyDetailsReq(final String urlPost, final JSONObject jsonObject) {

        final RequestQueue queue;
        StringRequest stringRequest = null;

        try {

            queue = Volley.newRequestQueue(getActivity());

            stringRequest = new StringRequest(Request.Method.GET, urlPost,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response)
                        {
                            try
                            {
                                if(response!=null)
                                {
                                    appConstants.stopSpinWheel();
                                    arryList=new ArrayList<>();

                                    JSONObject mainObj = new JSONObject(response.toString());
                                    JSONObject itemsObj =mainObj.getJSONObject("items");

                                    JSONArray openArr =itemsObj.getJSONArray("open");
                                    JSONArray closeArr =itemsObj.getJSONArray("close");
                                    JSONArray allDayArr =itemsObj.getJSONArray("allTheDay");

                                    JSONObject flagImg =allDayArr.getJSONObject(0);
                                    String flagURL=flagImg.getString("flag");
                                    Glide.with(getActivity()).load(flagURL).apply(RequestOptions.circleCropTransform()).into(flagIMG);


                                    try
                                    {
                                        JSONObject openObj=openArr.getJSONObject(0);
                                        openSellTXT.setText(openObj.getString("sale"));
                                        openBuyTXT.setText(openObj.getString("buy"));

                                    }
                                    catch (Exception xx){}

                                    try
                                    {
                                        JSONObject closeObj=closeArr.getJSONObject(0);
                                        closeSellTXT.setText(closeObj.getString("sale"));
                                        closeBuyTXT.setText(closeObj.getString("buy"));
                                    }
                                    catch (Exception xx){}

                                    for(int i=0;i<allDayArr.length();i++)
                                    {
                                        JSONObject obj=allDayArr.getJSONObject(i);

                                        String id=obj.getString("id");
                                        String name=obj.getString("name");
                                        String sell=obj.getString("sale");
                                        String buy=obj.getString("buy");
                                        String status=obj.getString("status");
                                        String date=obj.getString("date");

                                        currencyDetailsModel=new CurrencyDetailsModel(id,name,sell,buy,status,date);
                                        arryList.add(currencyDetailsModel);

                                    }

                                    if(arryList.size()>0)
                                    {
                                        adapter = new CurrencyDetailsAdapter(getActivity(), arryList);
                                        currencyDetailsLV.setAdapter(adapter);
                                        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
                                        currencyDetailsLV.setLayoutManager(layoutManager);

                                    }






                                }


                            }
                            catch (Exception e)
                            {
                                Toast.makeText(getActivity(), "لا يوجد نتائج", Toast.LENGTH_SHORT).show();
                                e.printStackTrace();
                                appConstants.stopSpinWheel();
                            }


                        }
                    }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(final VolleyError error) {

                  //  Toast.makeText(getActivity(), "server error", Toast.LENGTH_SHORT).show();
                    appConstants.stopSpinWheel();


                }


            });

            stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                    appConstants.appTimeOut,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

            queue.add(stringRequest);


        } catch (Exception e) {
            appConstants.stopSpinWheel();
            e.toString();
        }



    }

    private void getCurrencyChartReq(final String urlPost, final JSONObject jsonObject) {

        final RequestQueue queue;
        StringRequest stringRequest = null;

        try {

            queue = Volley.newRequestQueue(getActivity());

            stringRequest = new StringRequest(Request.Method.GET, urlPost,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response)
                        {
                            try
                            {
                                if(response!=null)
                                {
                                    appConstants.stopSpinWheel();
                                    chartArryList=new ArrayList<>();
                                    chartArryList.clear();

                                    JSONObject mainObj = new JSONObject(response.toString());
                                    JSONArray itemsArr =mainObj.getJSONArray("items");

                                     for(int i=0;i<itemsArr.length();i++)
                                     {
                                         JSONObject obj=itemsArr.getJSONObject(i);

                                         String id=obj.getString("id");
                                         String name=obj.getString("name");
                                         String sell=obj.getString("sale");
                                         String buy=obj.getString("buy");
                                         String status=obj.getString("status");
                                         String date=obj.getString("date");

                                         chartModel=new CurrencyDetailsChartModel(id,name,sell,buy,status,date);
                                         chartArryList.add(chartModel);

                                     }
                                    if(chartArryList.size()>1) {

                                        initilizeChart();
                                    }
                                    else
                                    {
                                        //Toast.makeText(getActivity(), getResources().getString(R.string.no_data), Toast.LENGTH_SHORT).show();
                                    }

                                }


                            }
                            catch (Exception e)
                            {
                                e.printStackTrace();
                                appConstants.stopSpinWheel();
                            }


                        }
                    }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(final VolleyError error) {

                   // Toast.makeText(getActivity(), "server error", Toast.LENGTH_SHORT).show();
                    appConstants.stopSpinWheel();


                }


            });

            stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                    appConstants.appTimeOut,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

            queue.add(stringRequest);


        } catch (Exception e) {
            appConstants.stopSpinWheel();
            e.toString();
        }



    }

    private void initilizeChart()
    {

        mChart.setViewPortOffsets(0, 0, 0, 0);
        mChart.setBackgroundColor(Color.TRANSPARENT);

        // no description text
        mChart.getDescription().setEnabled(false);

        // enable touch gestures
        mChart.setTouchEnabled(true);

        // enable scaling and dragging
        mChart.setDragEnabled(true);
        mChart.setScaleEnabled(true);

        // if disabled, scaling can be done on x- and y-axis separately
        mChart.setPinchZoom(false);

        mChart.setDrawGridBackground(false);
        mChart.setMaxHighlightDistance(200);

        final HashMap<Integer, String> dateMap = new HashMap<>();
        for(int x=0;x<chartArryList.size();x++)
        {
            String [] dateArr=chartArryList.get(x).getDate().split(" ");
            dateMap.put(x,dateArr[0]);
        }

        XAxis x = mChart.getXAxis();
        x.setLabelCount(chartArryList.size()/3, false);
        x.setTextColor(Color.WHITE);
        x.setTextSize(9f);
        x.setPosition(XAxis.XAxisPosition.BOTTOM_INSIDE);
        x.setDrawGridLines(true);
        x.setAxisLineColor(Color.WHITE);
        x.setValueFormatter(new IAxisValueFormatter() {
            @Override
            public String getFormattedValue(float value, AxisBase axis)
            {
               // return chartArryList.get((int)value).getDate();
               return dateMap.get((int)value);
            }
        });


        YAxis y = mChart.getAxisLeft();
        y.setLabelCount(chartArryList.size()/chartArryList.size()-1, false);
        y.setTextColor(Color.WHITE);
        x.setTextSize(9f);
        y.setPosition(YAxis.YAxisLabelPosition.INSIDE_CHART);
        y.setDrawGridLines(true);
        y.setAxisLineColor(Color.WHITE);

        mChart.getAxisRight().setEnabled(false);

        // add data
        setData();

        mChart.getLegend().setEnabled(false);

        mChart.animateXY(1000, 1000);

        // dont forget to refresh the drawing
        mChart.invalidate();

    }


    private void setData() {

        ArrayList<Entry> sellVals = new ArrayList<Entry>();

        for (int i = 0; i < chartArryList.size(); i++)
        {
            // float mult = (range + 1);
            float val =Float.valueOf(chartArryList.get(i).getSale());
            sellVals.add(new Entry(i, val));
        }


        LineDataSet sellSet;


        if (mChart.getData() != null &&
                mChart.getData().getDataSetCount() > 0) {
            sellSet = (LineDataSet)mChart.getData().getDataSetByIndex(0);
            sellSet.setValues(sellVals);


            mChart.getData().notifyDataChanged();
            mChart.notifyDataSetChanged();
        } else {
            // create a dataset and give it a type
            sellSet = new LineDataSet(sellVals, "");

            sellSet.setMode(LineDataSet.Mode.CUBIC_BEZIER);
            sellSet.setCubicIntensity(0.2f);

            //set1.setDrawFilled(true);
            sellSet.setDrawCircles(false);
            sellSet.setLineWidth(1.0f);
            sellSet.setCircleRadius(5f);

            sellSet.setCircleColor(Color.WHITE);
            sellSet.setHighLightColor(Color.rgb(244, 117, 117));
            sellSet.setColor(Color.WHITE);

            sellSet.setFillColor(Color.WHITE);
            sellSet.setFillAlpha(100);
            sellSet.setDrawHorizontalHighlightIndicator(false);


            sellSet.setFillFormatter(new IFillFormatter() {
                @Override
                public float getFillLinePosition(ILineDataSet dataSet, LineDataProvider dataProvider) {
                    return -10;
                }
            });



            // create a data object with the datasets
            LineData sellData = new LineData(sellSet);
            sellData.setValueTextSize(9f);
            sellData.setDrawValues(false);

            LineData buyData = new LineData(sellSet);
            buyData.setValueTextSize(9f);
            buyData.setDrawValues(false);

            // set data
            mChart.setData(sellData);


            for (IDataSet set : mChart.getData().getDataSets())
            { set.setDrawValues(!set.isDrawValuesEnabled());
                set.setValueTextColor(Color.WHITE);
            }


            List<ILineDataSet> sets = mChart.getData().getDataSets();
            for (ILineDataSet iSet : sets) {

                LineDataSet set = (LineDataSet) iSet;

                if (set.isDrawFilledEnabled())
                    set.setDrawFilled(false);
                else
                    set.setDrawFilled(true);
            }


            for (ILineDataSet iSet : sets) {

                LineDataSet set = (LineDataSet) iSet;
                if (set.isDrawCirclesEnabled())
                    set.setDrawCircles(false);
                else
                    set.setDrawCircles(true);
            }

            mChart.invalidate();
        }
    }

}
