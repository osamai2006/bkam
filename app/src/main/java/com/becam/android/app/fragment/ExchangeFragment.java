package com.becam.android.app.fragment;

import android.app.Activity;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.becam.android.app.R;
import com.becam.android.app.activity.MainScreenActivity;
import com.becam.android.app.activity.CurenciesActivity;
import com.becam.android.app.others.appConstants;
import com.becam.android.app.others.urlClass;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;

import org.json.JSONArray;
import org.json.JSONObject;

public class ExchangeFragment extends Fragment {

   static EditText amountTXT,resultTXT,symbolFromTXT,symbolToTXT;
   static ImageView flagImg1,flagImg2;
   ImageView calcBTN,changeBTN;
   TextView lastUpdateTXT;

   static   MainScreenActivity context;

    public void onAttach(Activity activity) {
        context = (MainScreenActivity) activity;
        super.onAttach(activity);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, final ViewGroup container, Bundle savedInstanceState) {


        View v=inflater.inflate(R.layout.exchange_fragment_layout, container, false);

        MainScreenActivity.screenTitleTXT.setText(getResources().getString(R.string.exchange_tab));

        amountTXT = v.findViewById(R.id.amountTXT);
        symbolFromTXT= v.findViewById(R.id.symbolFromTXT);
        symbolToTXT= v.findViewById(R.id.symbolToTXT);
        resultTXT = v.findViewById(R.id.resultTXT);
        resultTXT = v.findViewById(R.id.resultTXT);
        flagImg1 = v.findViewById(R.id.flagImg1);
        flagImg2 = v.findViewById(R.id.flagImg2);
        Glide.with(context).load("https://cdn.countryflags.com/thumbs/libya/flag-round-250.png").apply(RequestOptions.circleCropTransform()).into(flagImg1);
        Glide.with(context).load("https://cdn1.iconfinder.com/data/icons/flags-of-the-world-2/128/united-states-circle-512.png").apply(RequestOptions.circleCropTransform()).into(flagImg2);
        calcBTN= v.findViewById(R.id.calcBTN);

        lastUpdateTXT= v.findViewById(R.id.lastUpdateTXT);
        lastUpdateTXT.setText("loading...");

        changeBTN= v.findViewById(R.id.changeBTN);

        flagImg1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                appConstants.fromToFlag="from";
                startActivity(new Intent(context,CurenciesActivity.class));

            }
        });

        flagImg2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                appConstants.fromToFlag="to";
                startActivity(new Intent(context,CurenciesActivity.class));

            }
        });


        calcBTN.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(amountTXT.getText().toString().trim().equalsIgnoreCase(""))
                {
//                    Snackbar snackbar = Snackbar.make(getView(), "من فضلك ادخل الكمية المراد تحويلها", Snackbar.LENGTH_LONG);
//                    snackbar.show();

                     Toast.makeText(getActivity(), "من فضلك ادخل الكمية المراد تحويلها", Toast.LENGTH_SHORT).show();
                    return;
                }
                else   if(!symbolFromTXT.getText().toString().trim().equalsIgnoreCase("LYD") &&
                        !symbolToTXT.getText().toString().trim().equalsIgnoreCase("LYD"))
                {

                    Toast.makeText(getActivity(), "من فضلك قم باختيار LYD", Toast.LENGTH_SHORT).show();
                    return;
                }
                else
                    {
                    appConstants.startSpinwheel(getActivity(), false, true);
                    new Thread(new Runnable() {
                        @Override
                        public void run() {
                            try {
                                String amount=amountTXT.getText().toString().trim();

                                String from=symbolFromTXT.getText().toString().trim();
                                String to=symbolToTXT.getText().toString().trim();

                                String urlData=urlClass.getConvertResultURL+"amount="+amount+"&from="+from+"&to="+to;
                                getExchangeResultReq(urlData, null);
                            } catch (Exception xx) {
                            }
                        }
                    }).start();
                }
            }
        });

        changeBTN.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String resTemp="";
                String amountTemp,symbolTempTo="",symbolTempFrom="";
                ImageView currencyImageFrom,currencyImageTo;
                Drawable imgTempFrom,imgTempTo;

                resTemp=resultTXT.getText().toString();
                amountTemp=amountTXT.getText().toString();
                symbolTempFrom=symbolFromTXT.getText().toString();
                symbolTempTo=symbolToTXT.getText().toString();

                imgTempFrom=flagImg2.getDrawable();
                imgTempTo=flagImg1.getDrawable();

                resultTXT.setText(amountTemp);
                amountTXT.setText(resTemp);
                symbolFromTXT.setText(symbolTempTo);
                symbolToTXT.setText(symbolTempFrom);

               // Glide.with(context).load(imgTempTo).apply(RequestOptions.circleCropTransform()).into(flagImg1);
               // Glide.with(context).load(imgTempFrom).apply(RequestOptions.circleCropTransform()).into(flagImg2);
                flagImg1.setImageDrawable(imgTempFrom);
                flagImg2.setImageDrawable(imgTempTo);



            }
        });


        calcBTN.performClick();
        return v;


    }

    public static void setCurencyTXTVIEW(String symbol, String name, String img, String fromToFlag)
    {
        if(fromToFlag.equalsIgnoreCase("from"))
        {
            symbolFromTXT.setText(symbol);
            Glide.with(context).load(img).apply(RequestOptions.circleCropTransform()).into(flagImg1);


        }
        else if(fromToFlag.equalsIgnoreCase("to"))
        {
            Glide.with(context).load(img).apply(RequestOptions.circleCropTransform()).into(flagImg2);
            symbolToTXT.setText(symbol);
        }

    }


    private void getExchangeResultReq(String urlPost, final JSONObject jsonObject) {

        final RequestQueue queue;
        StringRequest stringRequest = null;

        try {

            queue = Volley.newRequestQueue(getActivity());

            stringRequest = new StringRequest(Request.Method.GET, urlPost,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response)
                        {
                            try
                            {
                                if(response!=null)
                                {
                                    appConstants.stopSpinWheel();

                                    JSONObject mainObj = new JSONObject(response.toString());

                                        JSONObject obj=mainObj.getJSONObject("0");

                                        String result =obj.getString("result");
                                        resultTXT.setText(result);
                                        lastUpdateTXT.setText(mainObj.getString("last_update_date"));


                                }


                            }
                            catch (Exception e)
                            {
//                                Snackbar snackbar = Snackbar.make(getView(), "لا يوجد نتائج", Snackbar.LENGTH_LONG);
//                                snackbar.show();
                                Toast.makeText(getActivity(), "لا يوجد نتائج", Toast.LENGTH_SHORT).show();
                                e.printStackTrace();
                                appConstants.stopSpinWheel();
                            }


                        }
                    }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(final VolleyError error) {

                   // Toast.makeText(getActivity(), "server error", Toast.LENGTH_SHORT).show();
                    appConstants.stopSpinWheel();


                }


            });

            stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                    appConstants.appTimeOut,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

            queue.add(stringRequest);


        } catch (Exception e) {
            appConstants.stopSpinWheel();
            e.toString();
        }



    }


}
