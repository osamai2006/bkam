package com.becam.android.app.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.becam.android.app.R;
import com.becam.android.app.fragment.HomeFragment;
import com.becam.android.app.models.CurrencyDetailsModel;
import com.becam.android.app.models.NewsModel;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

/**
 * Created by Ayadi on 2/6/2018.
 */

public class NewsAdapter extends RecyclerView.Adapter<NewsAdapter.MenuItemViewHolder> {

    Context context;
    ArrayList<NewsModel> resourcelList;





    public NewsAdapter(Context context, ArrayList<NewsModel> ResourcelList) {
        this.context = context;
        resourcelList = ResourcelList;
    }

    @Override
    public MenuItemViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.news_grid_item, parent, false);
        MenuItemViewHolder viewholder = new MenuItemViewHolder(view);
        return viewholder;
    }

    @Override
    public void onBindViewHolder(MenuItemViewHolder holder, final int position) {

        holder.title_text.setText(resourcelList.get(position).getTitle());
        holder.date_text.setText(resourcelList.get(position).getDate());
        Picasso.with(context).load(resourcelList.get(position).getImage()).into(holder.news_image);



        holder.news_image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                HomeFragment.lastPos=position;
                HomeFragment.goToNewsDetails(resourcelList.get(position).getId());
            }
        });






    }

    @Override
    public int getItemCount() {
        return resourcelList.size();
    }

    public static class MenuItemViewHolder extends RecyclerView.ViewHolder
    {

        TextView title_text ,date_text ;
        ImageView news_image;



        public MenuItemViewHolder(View itemView)
        {
            super(itemView);

            title_text = itemView.findViewById(R.id.title_text);
            date_text = itemView.findViewById(R.id.date_text);
            news_image = itemView.findViewById(R.id.news_image);


        }


    }




}
