package com.becam.android.app.fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.becam.android.app.R;
import com.becam.android.app.activity.MainScreenActivity;
import com.becam.android.app.adapters.BitcoinDetailsAdapter;
import com.becam.android.app.adapters.OilDetailsAdapter;
import com.becam.android.app.models.BitCoinDetailsModel;
import com.becam.android.app.models.CurrencyDetailsModel;
import com.becam.android.app.models.GoldDetailsModel;
import com.becam.android.app.models.OilDetailsModel;
import com.becam.android.app.others.appConstants;
import com.becam.android.app.others.urlClass;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

/**
 * Created by HP on 5/3/2018.
 */

public class BitcoinDetailsFragment extends Fragment  {
    public static MainScreenActivity myContext;

    public  static String itemID="";
    public  static String itemName="";
    public  static String lastUpdate="";

    TextView lastUpdate_TXT;

    View view;


    RecyclerView bitcoinDetailsLV;

    BitCoinDetailsModel bitCoinDetailsModel;
    ArrayList<BitCoinDetailsModel> arrayList;
    BitcoinDetailsAdapter adapter;

    @Override
    public void onAttach(Context context) {
        myContext=(MainScreenActivity) getActivity();
        super.onAttach(context);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.bitcoin_details_fragment, container, false);

        bitcoinDetailsLV=view.findViewById(R.id.bitcoinDetailsLV);
        lastUpdate_TXT=view.findViewById(R.id.lastUpdate_TXT);
        lastUpdate_TXT.setText(lastUpdate);

        MainScreenActivity.screenTitleTXT.setText("بيتكوين");

        appConstants.startSpinwheel(getActivity(), false, true);
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {


                      String  urlData= urlClass.getBitCoinDetailsURL;
                        getBitcoinDetailsReq(urlData, null);




                } catch (Exception xx) {
                }
            }
        }).start();



        return view;
    }


    private void getBitcoinDetailsReq(final String urlPost, final JSONObject jsonObject) {

        final RequestQueue queue;
        StringRequest stringRequest = null;

        try {

            queue = Volley.newRequestQueue(getActivity());

            stringRequest = new StringRequest(Request.Method.GET, urlPost,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response)
                        {
                            try
                            {
                                if(response!=null)
                                {
                                    appConstants.stopSpinWheel();
                                    arrayList=new ArrayList<>();

                                    JSONObject mainObj = new JSONObject(response.toString());
                                    JSONArray itemsArr =mainObj.getJSONArray("items");
                                    lastUpdate_TXT.setText(mainObj.getString("last_update_date"));
                                    for(int i=0;i<itemsArr.length();i++)
                                    {
                                        JSONObject obj=itemsArr.getJSONObject(i);

                                        String id=obj.getString("id");
                                        String open=obj.getString("open_price");
                                        String high=obj.getString("high_price");
                                        String low=obj.getString("low_price");
                                        String close=obj.getString("close_price");
                                        String volumBTC=obj.getString("volume_BTC");
                                        String volumCurrency=obj.getString("volume_Currency");
                                        String weghtedPrice=obj.getString("weighted_Price");
                                        String date=obj.getString("date_Price");

                                        bitCoinDetailsModel=new BitCoinDetailsModel(id,open,high,low,close,volumBTC,volumCurrency,weghtedPrice,date);
                                        arrayList.add(bitCoinDetailsModel);
                                    }

                                    if(arrayList.size()>0)
                                    {
                                        adapter = new BitcoinDetailsAdapter(getActivity(), arrayList);
                                        bitcoinDetailsLV.setAdapter(adapter);
                                        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
                                        bitcoinDetailsLV.setLayoutManager(layoutManager);
                                    }
                                }


                            }
                            catch (Exception e)
                            {
                                Toast.makeText(getActivity(), "لا يوجد نتائج", Toast.LENGTH_SHORT).show();
                                e.printStackTrace();
                                appConstants.stopSpinWheel();
                            }


                        }
                    }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(final VolleyError error) {

                   // Toast.makeText(getActivity(), "server error", Toast.LENGTH_SHORT).show();
                    appConstants.stopSpinWheel();


                }


            });

            stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                    appConstants.appTimeOut,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

            queue.add(stringRequest);


        } catch (Exception e) {
            appConstants.stopSpinWheel();
            e.toString();
        }



    }



}
