package com.becam.android.app.models;

/**
 * Created by HP on 02/10/2017.
 */

public class AdsModel
{
    private String id;
    private String image;
    private String url;
    private String video;

    public AdsModel(String id, String image, String url, String video) {
        this.id = id;
        this.image = image;
        this.url = url;
        this.video = video;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getVideo() {
        return video;
    }

    public void setVideo(String video) {
        this.video = video;
    }
}
