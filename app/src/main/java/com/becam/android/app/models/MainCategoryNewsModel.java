package com.becam.android.app.models;

public class MainCategoryNewsModel {

    private String id;
    private String image;
    private String name;
    private String link;

    public MainCategoryNewsModel(String id, String image, String name, String link) {
        this.id = id;
        this.image = image;
        this.name = name;
        this.link = link;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }
}
