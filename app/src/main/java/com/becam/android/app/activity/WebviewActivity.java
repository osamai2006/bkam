package com.becam.android.app.activity;


import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.becam.android.app.R;
import com.becam.android.app.others.SharedPrefsUtils;
import com.becam.android.app.others.appConstants;


import org.json.JSONObject;


public class WebviewActivity extends AppCompatActivity {
    private static String TAG = "PaymentActivity";

    WebView webview;
    SharedPrefsUtils myPref;
    int im = 0;
    public  static String url;
    SharedPrefsUtils sharedPref;
    String userToken="";

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.webview_fragment);

        webview = findViewById(R.id.webview);

        appConstants.startSpinwheel(WebviewActivity.this,false,true);


        webview.getSettings().setBuiltInZoomControls(false);
        webview.getSettings().setLoadWithOverviewMode(true);
        webview.getSettings().setUseWideViewPort(false);
        webview.setHorizontalScrollBarEnabled(false);
        // wv.setScrollBarStyle(View.SCROLLBARS_INSIDE_OVERLAY);
        webview.getSettings().setJavaScriptEnabled(true);


        webview.setWebViewClient(new WebViewClient()
                                         {

                                             public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {
                                                 Toast.makeText(getApplicationContext(), description, Toast.LENGTH_SHORT).show();
                                             }

                                             @Override
                                             public void onPageStarted(WebView view, String url, Bitmap favicon) {

                                                 appConstants.startSpinwheel(WebviewActivity.this,false,true);
                                             }

                                             @Override
                                             public void onPageFinished(WebView view, String url) {
                                                 appConstants.stopSpinWheel();
                                                 im += 1;
                                             }}

        );
        webview.loadUrl(url);

    }


}