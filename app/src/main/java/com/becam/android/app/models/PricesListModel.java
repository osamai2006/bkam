package com.becam.android.app.models;

import org.json.JSONArray;

/**
 * Created by HP on 3/25/2018.
 */

public class PricesListModel {

    private String id;
    private String name;
    private String descr;
    private JSONArray sharesArr;
    private String flag;

    public PricesListModel(String id, String name, String descr, JSONArray sharesArr, String flag) {
        this.id = id;
        this.name = name;
        this.descr = descr;
        this.sharesArr = sharesArr;
        this.flag = flag;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescr() {
        return descr;
    }

    public void setDescr(String descr) {
        this.descr = descr;
    }

    public JSONArray getSharesArr() {
        return sharesArr;
    }

    public void setSharesArr(JSONArray sharesArr) {
        this.sharesArr = sharesArr;
    }

    public String getFlag() {
        return flag;
    }

    public void setFlag(String flag) {
        this.flag = flag;
    }
}
