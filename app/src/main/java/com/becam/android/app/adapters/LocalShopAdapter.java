package com.becam.android.app.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.becam.android.app.R;
import com.becam.android.app.fragment.HomeFragment;
import com.becam.android.app.fragment.ShopListMainFragment;
import com.becam.android.app.models.LocalShopModel;
import com.becam.android.app.models.PricesListModel;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

/**
 * Created by Ayadi on 2/6/2018.
 */

public class LocalShopAdapter extends RecyclerView.Adapter<LocalShopAdapter.MenuItemViewHolder> {

    Context context;
    ArrayList<LocalShopModel> resourcelList;


    public LocalShopAdapter(Context context, ArrayList<LocalShopModel> ResourcelList) {
        this.context = context;
        resourcelList = ResourcelList;
    }

    @Override
    public MenuItemViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.local_shop_list_item_row, parent, false);
        MenuItemViewHolder viewholder = new MenuItemViewHolder(view);
        return viewholder;
    }

    @Override
    public void onBindViewHolder(MenuItemViewHolder holder, final int position) {

        holder.shopNameTXT.setText(resourcelList.get(position).getName() );
        holder.countryTXT.setText(resourcelList.get(position).getCountry());
        Picasso.with(context).load(resourcelList.get(position).getImage()).into(holder.shopIMG);


        holder.mainLO.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {

                ShopListMainFragment.goToShopDetails(resourcelList.get(position).getId(),"local",resourcelList.get(position).getCountry());
            }
        });



    }

    @Override
    public int getItemCount() {
        return resourcelList.size();
    }

    public static class MenuItemViewHolder extends RecyclerView.ViewHolder
    {

        LinearLayout mainLO;
        TextView shopNameTXT,countryTXT ;
        ImageView shopIMG;

        public MenuItemViewHolder(View itemView)
        {
            super(itemView);
            mainLO = itemView.findViewById(R.id.mainLO);
            shopNameTXT = itemView.findViewById(R.id.shopNameTXT);
            shopIMG = itemView.findViewById(R.id.shopIMG);
            countryTXT= itemView.findViewById(R.id.countryTXT);


        }


    }




}
