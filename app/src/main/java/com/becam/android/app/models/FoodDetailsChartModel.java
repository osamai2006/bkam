package com.becam.android.app.models;

public class FoodDetailsChartModel {

    private String name;
    private String price;
    private String status;
    private String date;

    public FoodDetailsChartModel(String name, String price, String status, String date) {
        this.name = name;
        this.price = price;
        this.status = status;
        this.date = date;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }
}
