package com.becam.android.app.models;

public class GoldDetailsModel {

    private String name;
    private String sale;
    private String status;
    private String date;

    public GoldDetailsModel(String name, String sale, String status, String date) {
        this.setName(name);
        this.setSale(sale);
        this.setStatus(status);
        this.setDate(date);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSale() {
        return sale;
    }

    public void setSale(String sale) {
        this.sale = sale;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }
}
