package com.becam.android.app.activity;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.becam.android.app.R;
import com.becam.android.app.others.SharedPrefsUtils;
import com.becam.android.app.others.appConstants;
import com.becam.android.app.others.urlClass;

import org.json.JSONObject;

/**
 * Created by HP on 5/3/2017.
 */

public class ChangePasswordActivity extends Activity {

    EditText oldPassTXT,confirmPassTXT,newPassTXT;
    Button submitBTN;
    Activity activityCurrent;
    SharedPrefsUtils sharedPref;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.change_pass_layout);
        activityCurrent=this;
        oldPassTXT= (EditText) findViewById(R.id.oldPAss_txt);
        newPassTXT= (EditText) findViewById(R.id.newPass_txt);
        confirmPassTXT= (EditText) findViewById(R.id.confirmNewPass_txt);
        submitBTN= (Button) findViewById(R.id.submitChangePassBTN);

        submitBTN.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (checkRule())
                {

                    appConstants.startSpinwheel(ChangePasswordActivity.this, false, true);

                    new Thread(new Runnable() {
                        @Override
                        public void run() {

                            try
                            {
                                String url ="";// urlClass.resetPassURL;
                                String id = sharedPref.getStringPreference(getApplicationContext(), appConstants.userID_KEY);
                                JSONObject Objct = new JSONObject();
                                Objct.putOpt("ID", id);
                                Objct.putOpt("PASSWORD", confirmPassTXT.getText().toString().trim());
                                resetPassRequest(url, Objct);

                            } catch (Exception xx) {
                                xx.toString();
                                appConstants.stopSpinWheel();
                            }
                        }
                    }).start();
                }
            }

        });

    }

    private  boolean checkRule() {
        if (!oldPassTXT.getText().toString().trim().equals(sharedPref.getStringPreference(getApplicationContext(),appConstants.userPassword_KEY)))
        {
            Toast.makeText(activityCurrent, "Old Password not correct", Toast.LENGTH_SHORT).show();
            return false;
        }
        if (!newPassTXT.getText().toString().trim().equals(confirmPassTXT.getText().toString().trim()))
        {
            Toast.makeText(activityCurrent, "New Password not match", Toast.LENGTH_SHORT).show();
            return false;
        }

        return  true;
    }

    private void resetPassRequest(String urlPost, final JSONObject jsonObjectRequest) {

        JsonObjectRequest jsonObjReq;

        jsonObjReq = new JsonObjectRequest(Request.Method.POST, urlPost, jsonObjectRequest, new Response.Listener<JSONObject>()
        {

            @Override
            public void onResponse(JSONObject response) {

                try
                {
                    if(response !=null)
                    {
                        appConstants.stopSpinWheel();
                        JSONObject jsonObjResp = new JSONObject(response.toString());
                        final String status=jsonObjResp.getString("status");

                        if(status.equalsIgnoreCase("true"))
                        {
                            Toast.makeText(getApplicationContext(), jsonObjResp.get("message").toString(), Toast.LENGTH_LONG).show();
                            sharedPref.setStringPreference(getApplicationContext(), appConstants.userPassword_KEY,confirmPassTXT.getText().toString().trim());

                            finish();

                        }
                        else
                        {
                            appConstants.stopSpinWheel();
                           // Toast.makeText(getApplicationContext(), jsonObjResp.get("message").toString(), Toast.LENGTH_LONG).show();
                        }
                    }
                    else
                    {
                        appConstants.stopSpinWheel();
                        //Toast.makeText(getApplicationContext(), "server error", Toast.LENGTH_LONG).show();
                    }


                }
                catch (Exception e)
                {
                    Toast.makeText(getApplicationContext(),e.getMessage().toString(), Toast.LENGTH_SHORT).show();
                    appConstants.stopSpinWheel();
                }

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error)
            {
                Toast.makeText(getApplicationContext(),R.string.connection_err, Toast.LENGTH_SHORT).show();
                appConstants.stopSpinWheel();

            }
        });

        RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());
        jsonObjReq.setRetryPolicy(new DefaultRetryPolicy(
                appConstants.appTimeOut,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(jsonObjReq);
    }

}


