package com.becam.android.app.fragment;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.NetworkImageView;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.becam.android.app.R;
import com.becam.android.app.activity.MainScreenActivity;
import com.becam.android.app.adapters.NewsAdapter;
import com.becam.android.app.models.NewsModel;
import com.becam.android.app.others.appConstants;
import com.becam.android.app.others.urlClass;
import com.pierfrancescosoffritti.androidyoutubeplayer.player.YouTubePlayer;
import com.pierfrancescosoffritti.androidyoutubeplayer.player.YouTubePlayerView;
import com.pierfrancescosoffritti.androidyoutubeplayer.player.listeners.AbstractYouTubePlayerListener;
import com.pierfrancescosoffritti.androidyoutubeplayer.player.listeners.YouTubePlayerInitListener;
import com.pierfrancescosoffritti.androidyoutubeplayer.player.playerUtils.FullScreenHelper;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;


public class NewsDetailsFragment extends Fragment {
    View v;
    TextView descTXT, dateTXT, titleTXT,youtubeLinkTXT;
    ImageView imageView;
    YouTubePlayerView youtubePlayerView;
    //FullScreenHelper fullScreenHelper;


    public static String postID;
    String embed="";



    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        v = inflater.inflate(R.layout.news_details_fragment, container, false);
        descTXT =  v.findViewById(R.id.descTXT);
        dateTXT =  v.findViewById(R.id.dateTXT);
        titleTXT =  v.findViewById(R.id.titleTXT);
        imageView = v.findViewById(R.id.news_image);

        youtubePlayerView = v.findViewById(R.id.youtube_player_view);
        getLifecycle().addObserver(youtubePlayerView);
//        fullScreenHelper=new FullScreenHelper();
//        fullScreenHelper.enterFullScreen(youtubePlayerView);

        MainScreenActivity.screenTitleTXT.setText(getResources().getString(R.string.home_tab));


//        youtubeLinkTXT= v.findViewById(R.id.youtubeLinkTXT);
//        youtubeLinkTXT.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//
//                openWebURL(embed);
//            }
//        });



        appConstants.startSpinwheel(getActivity(), false, true);

        new Thread(new Runnable() {
            @Override
            public void run() {
                try
                {
                    getNewsDetailsRequest(urlClass.getNewsDetailsURL+postID, null);

                } catch (Exception xx) {
                }
            }
        }).start();


        return v;
    }

    private void getNewsDetailsRequest(String urlPost, final JSONObject jsonObject) {

        final RequestQueue queue;
        StringRequest stringRequest = null;

        try {

            queue = Volley.newRequestQueue(getActivity());

            stringRequest = new StringRequest(Request.Method.GET, urlPost,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response)
                        {
                            try
                            {
                                if(response!=null)
                                {
                                    response=new String(response.getBytes("ISO-8859-1"), "UTF-8");
                                    appConstants.stopSpinWheel();


                                    JSONObject mainObj = new JSONObject(response);
                                    JSONArray arr=mainObj.getJSONArray("details");


                                        try {
                                            JSONObject dataObj=arr.getJSONObject(0);

                                            String id = dataObj.getString("post_id");


                                            titleTXT.setText(dataObj.getString("post_title"));
                                            descTXT.setText(dataObj.getString("post_content"));
                                            dateTXT.setText(  dataObj.getString("post_date"));
                                            Picasso.with(getActivity()).load(dataObj.getString("post_image")).into(imageView);

                                            try
                                            {
                                                 embed= dataObj.getString("embed");

                                            }catch (Exception xx){}

                                            if(embed.equalsIgnoreCase(""))
                                            {
                                               youtubePlayerView.setVisibility(View.GONE);
                                               // youtubeLinkTXT.setVisibility(View.GONE);
                                            }
                                            else
                                            {
                                                youtubePlayerView.setVisibility(View.VISIBLE);
                                                //youtubeLinkTXT.setVisibility(View.VISIBLE);
//                                                String ref="<a href="+embed+">شاهد الفيديو من هنا</a>";
//                                                youtubeLinkTXT.setText(Html.fromHtml( ref));

                                                youtubePlayerView.initialize(new YouTubePlayerInitListener() {
                                                    @Override
                                                    public void onInitSuccess(@NonNull final YouTubePlayer initializedYouTubePlayer) {
                                                        initializedYouTubePlayer.addListener(new AbstractYouTubePlayerListener() {
                                                            @Override
                                                            public void onReady() {
                                                                String videoId = appConstants.splitYoutubeVidio(embed);
                                                                initializedYouTubePlayer.loadVideo(videoId, 0);
                                                                initializedYouTubePlayer.pause();
                                                            }
                                                        });
                                                    }
                                                },true);


                                            }

                                        } catch (Exception xx) {
                                            xx.toString();
                                        }



                                }


                            }
                            catch (Exception e)
                            {
                                e.printStackTrace();
                                appConstants.stopSpinWheel();
                            }


                        }
                    }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(final VolleyError error) {

                   // Toast.makeText(getActivity(), "server error", Toast.LENGTH_SHORT).show();
                    appConstants.stopSpinWheel();


                }


            });

            stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                    appConstants.appTimeOut,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

            queue.add(stringRequest);


        } catch (Exception e) {
            appConstants.stopSpinWheel();
            e.toString();
        }



    }

    private void openWebURL( String url) {
        try {
            if (!url.startsWith("http://") && !url.startsWith("https://")) {
                url = "http://" + url;
                Intent browserIntent = new Intent(Intent.ACTION_VIEW,
                        Uri.parse(url));
                startActivity(browserIntent);
            }
            else
            {
                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
                startActivity(browserIntent);
            }
        }
        catch (Exception xx){}


    }

}