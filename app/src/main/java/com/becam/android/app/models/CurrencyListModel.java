package com.becam.android.app.models;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by HP on 5/21/2017.
 */

public class CurrencyListModel
{
    private String symbol;
    private String name;
    private String image;

    public CurrencyListModel(String symbol, String name, String image) {
        this.symbol = symbol;
        this.name = name;
        this.image = image;
    }

    public String getSymbol() {
        return symbol;
    }

    public void setSymbol(String symbol) {
        this.symbol = symbol;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }
}
