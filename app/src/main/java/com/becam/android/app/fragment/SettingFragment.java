package com.becam.android.app.fragment;

import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkResponse;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HttpHeaderParser;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.becam.android.app.R;
import com.becam.android.app.activity.LoginSubscriptionActivity;
import com.becam.android.app.activity.MainScreenActivity;
import com.becam.android.app.adapters.PricesListAdapter;
import com.becam.android.app.adapters.SettingAdapter;
import com.becam.android.app.models.PricesListModel;
import com.becam.android.app.models.SettingListModel;
import com.becam.android.app.others.SharedPrefsUtils;
import com.becam.android.app.others.appConstants;
import com.becam.android.app.others.urlClass;


import org.json.JSONArray;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Locale;
import java.util.Map;

/**
 * Created by HP on 5/3/2017.
 */

public class SettingFragment extends Fragment
{
    View view;

    TextView loginBTN,arBTN,engBTN;
    FragmentTransaction tx;
    SharedPrefsUtils sharedPref;
    boolean isLogggedIn=false;


    SettingListModel model;
    ArrayList<SettingListModel> arrayList;
    SettingAdapter adapter;
    RecyclerView settingLV;


    static String mobileNum="";

    public static MainScreenActivity myContext;
    @Override
    public void onAttach(Context context) {
        myContext = (MainScreenActivity) getActivity();
        super.onAttach(context);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view=inflater.inflate(R.layout.setting_fragment_layout, container, false);
        MainScreenActivity.screenTitleTXT.setText(getResources().getString(R.string.setting_tab));
        initialScreen();
        isLogggedIn=sharedPref.getBooleanPreference(getActivity(), appConstants.isLoggedIn,false);
         mobileNum = sharedPref.getStringPreference(getActivity(), appConstants.userMobile_KEY);

        if(isLogggedIn)
        {
            loginBTN.setText("تسجيل الخروج");

        }
        else
        {
            loginBTN.setText("تسجيل الدخول");

        }

//        arBTN.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                setInitialLanguage("ar");
//
//            }
//        });
//
//        engBTN.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                setInitialLanguage("en");
//
//            }
//        });

        loginBTN.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                if(isLogggedIn)
                {
                    sharedPref.setStringPreference(getActivity(), appConstants.userID_KEY, "");
                    sharedPref.setBooleanPreference(getActivity(), appConstants.isLoggedIn, false);


                    Intent i = getActivity().getPackageManager()
                            .getLaunchIntentForPackage(getActivity().getBaseContext().getPackageName());
                    i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_WHEN_TASK_RESET);
                    startActivity(i);


                }
                else
                {
                    startActivity(new Intent(getActivity(), LoginSubscriptionActivity.class));
                }

            }
        });

//        appConstants.startSpinwheel(getActivity(), false, true);
//
//        new Thread(new Runnable() {
//            @Override
//            public void run() {
//                try
//                {
//                    getSettingListRequest(urlClass.getCategoryWithShareURL, null);
//
//                } catch (Exception xx) {
//                }
//            }
//        }).start();



        appConstants.startSpinwheel(getActivity(), false, true);

        new Thread(new Runnable() {
            @Override
            public void run() {
                try
                {
                    getUserSettingRequest(urlClass.getUserSettingURL+mobileNum, null);

                } catch (Exception xx) {
                }
            }
        }).start();


        return view;

    }



    private void initialScreen()
    {
//        arBTN=  view.findViewById(R.id.arBTN);
//        engBTN=  view.findViewById(R.id.engBTN);
        loginBTN=  view.findViewById(R.id.login_stng_BTN);
        settingLV=  view.findViewById(R.id.settingLV);


    }


    public static void postPushStatusReq(String status,String catID) {

        appConstants.startSpinwheel(myContext,false,true);
        final RequestQueue queue;
        StringRequest stringRequest = null;

        try {

            String urlPost=urlClass.notificationONOffURL+mobileNum+"&status="+status+"&typeNotif="+catID;
            queue = Volley.newRequestQueue(myContext);

            stringRequest = new StringRequest(Request.Method.GET, urlPost,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            try
                            {

                                if (response != null)
                                {
                                    JSONArray arr=new JSONArray(response);
                                    JSONObject result=arr.getJSONObject(0);

                                    Toast.makeText(myContext, result.get("status").toString(), Toast.LENGTH_SHORT).show();


                                    appConstants.stopSpinWheel();

                                }



                            }
                            catch (Exception xx)
                            {
                                xx.toString();
                                appConstants.stopSpinWheel();
                            }


                        }
                    }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(final VolleyError error) {
                    String xx = error.toString();
                    Toast.makeText(myContext, R.string.connection_err, Toast.LENGTH_SHORT).show();
                    appConstants.stopSpinWheel();


                }


            })

            {

                @Override
                protected Response<String> parseNetworkResponse(NetworkResponse response) {
                    try {

                        String jsonString = new String(response.data,
                                HttpHeaderParser.parseCharset(response.headers));

                        return Response.success(jsonString, HttpHeaderParser.parseCacheHeaders(response));


                    } catch (UnsupportedEncodingException e) {
                        return Response.error(new ParseError(e));
                    }
                }


            };

            stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                    appConstants.appTimeOut,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

            queue.add(stringRequest);


        } catch (Exception e) {
            appConstants.stopSpinWheel();
            e.toString();
        }



    }

    private void getSettingListRequest(String urlPost, final JSONObject jsonObject) {

        final RequestQueue queue;
        StringRequest stringRequest = null;

        try {

            queue = Volley.newRequestQueue(getActivity());

            stringRequest = new StringRequest(Request.Method.GET, urlPost,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response)
                        {
                            try
                            {
                                if(response!=null)
                                {
                                    appConstants.stopSpinWheel();

                                    arrayList = new ArrayList<>();

                                    JSONArray arr = new JSONArray(response.toString());

                                    for (int i = 0; i < arr.length(); i++) {

                                        try {
                                            JSONObject mainObj=arr.getJSONObject(i);

                                            String id = mainObj.getString("id");
                                            String name = mainObj.getString("cat_name");
                                            String desc = "";
                                            JSONArray sharesArr =mainObj.getJSONArray("items");

                                            String flag="";
                                            if(id.equalsIgnoreCase("1"))
                                            {
                                                flag="currency" ;
                                            }
                                            else   if(id.equalsIgnoreCase("2"))
                                            {
                                                flag="gold";
                                            }
                                            else   if(id.equalsIgnoreCase("3"))
                                            {
                                                flag="oil";
                                            }
                                            else   if(id.equalsIgnoreCase("4"))
                                            {
                                                flag="bitcoin";
                                            }
                                            else   if(id.equalsIgnoreCase("5"))
                                            {
                                                flag="material";
                                            }
                                            else  if(id.equalsIgnoreCase("6"))
                                            {
                                                flag="food";
                                            }
                                            else  if(id.equalsIgnoreCase("7"))
                                            {
                                                flag="meat";
                                            }
                                            else  if(id.equalsIgnoreCase("8"))
                                            {
                                                flag="veg";
                                            }
                                            else  if(id.equalsIgnoreCase("9"))
                                            {
                                                flag="fruit";
                                            }
                                            else  if(id.equalsIgnoreCase("10"))
                                            {
                                                flag="wmeat";
                                            }

                                            model = new SettingListModel(id, name,"off");
                                            arrayList.add(model);

                                        } catch (Exception xx) {
                                            xx.toString();
                                        }
                                    }


                                    adapter = new SettingAdapter(getActivity(), arrayList);
                                    settingLV.setAdapter(adapter);
                                    LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
                                    settingLV.setLayoutManager(layoutManager);

                                }


                            }
                            catch (Exception e)
                            {
                                e.printStackTrace();
                                appConstants.stopSpinWheel();
                            }


                        }
                    }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(final VolleyError error) {

                  //  Toast.makeText(getActivity(), "server error", Toast.LENGTH_SHORT).show();
                    appConstants.stopSpinWheel();


                }


            });

            stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                    appConstants.appTimeOut,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

            queue.add(stringRequest);


        } catch (Exception e) {
            appConstants.stopSpinWheel();
            e.toString();
        }



    }

    private void getUserSettingRequest(String urlPost, final JSONObject jsonObject) {

        final RequestQueue queue;
        StringRequest stringRequest = null;

        try {

            queue = Volley.newRequestQueue(getActivity());

            stringRequest = new StringRequest(Request.Method.GET, urlPost,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response)
                        {
                            try
                            {
                                if(response!=null)
                                {
                                    appConstants.stopSpinWheel();
                                    response=new String(response.getBytes("ISO-8859-1"), "UTF-8");

                                    arrayList = new ArrayList<>();

                                    JSONArray arr = new JSONArray(response.toString());

                                    for (int i = 0; i < arr.length(); i++) {

                                        try {

                                            JSONObject mainObj=arr.getJSONObject(i);

                                            String id = mainObj.getString("id");
                                            String name = mainObj.getString("name");
                                            String status = mainObj.getString("status");


                                            model = new SettingListModel(id, name,status);
                                            arrayList.add(model);

                                        } catch (Exception xx) {
                                            xx.toString();
                                        }
                                    }


                                    adapter = new SettingAdapter(getActivity(), arrayList);
                                    settingLV.setAdapter(adapter);
                                    LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
                                    settingLV.setLayoutManager(layoutManager);

                                }


                            }
                            catch (Exception e)
                            {
                                e.printStackTrace();
                                appConstants.stopSpinWheel();
                            }


                        }
                    }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(final VolleyError error) {

                  //  Toast.makeText(getActivity(), "server error", Toast.LENGTH_SHORT).show();
                    appConstants.stopSpinWheel();


                }


            });

            stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                    appConstants.appTimeOut,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

            queue.add(stringRequest);


        } catch (Exception e) {
            appConstants.stopSpinWheel();
            e.toString();
        }



    }

    private void setInitialLanguage(String language) {
        Locale locale = new Locale(language);
        Resources res = getResources();
        DisplayMetrics dm = res.getDisplayMetrics();
        Configuration conf = res.getConfiguration();
        conf.locale = locale;
        res.updateConfiguration(conf, dm);
        onConfigurationChanged(conf);


        sharedPref.setStringPreference(getActivity(), appConstants.loacalCurrent,language);
        Intent i = getActivity().getPackageManager()
                .getLaunchIntentForPackage(getActivity().getBaseContext().getPackageName());
        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_WHEN_TASK_RESET);
        startActivity(i);
    }






}
