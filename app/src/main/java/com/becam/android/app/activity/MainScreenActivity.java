package com.becam.android.app.activity;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.telephony.TelephonyManager;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.becam.android.app.R;
import com.becam.android.app.fragment.AboutUsFragment;
import com.becam.android.app.fragment.BitcoinDetailsFragment;
import com.becam.android.app.fragment.CategoryDetailsFragment;
import com.becam.android.app.fragment.ContactUsFragment;
import com.becam.android.app.fragment.ExchangeFragment;
import com.becam.android.app.fragment.MainCategoryNewsFragment;
import com.becam.android.app.fragment.PricesListFragment;
import com.becam.android.app.fragment.SettingFragment;
import com.becam.android.app.fragment.ShopListMainFragment;
import com.becam.android.app.fragment.TermsConditionFragment;
import com.becam.android.app.others.SharedPrefsUtils;
import com.becam.android.app.others.appConstants;
import com.becam.android.app.others.urlClass;
import com.daimajia.slider.library.SliderLayout;
import com.daimajia.slider.library.SliderTypes.BaseSliderView;
import com.daimajia.slider.library.SliderTypes.DefaultSliderView;
import com.daimajia.slider.library.Tricks.ViewPagerEx;
import com.google.android.gms.ads.doubleclick.PublisherAdRequest;
import com.google.android.gms.ads.doubleclick.PublisherAdView;

import org.json.JSONArray;
import org.json.JSONObject;

public class MainScreenActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener, BaseSliderView.OnSliderClickListener, ViewPagerEx.OnPageChangeListener {
    public static TextView text_count_cart;
    NavigationView navigationView;
    ImageView imgmenu, toolbar_title,navImageView,backBTN;
    TextView logiBTN,profileNameTXT;
    public  static TextView screenTitleTXT;

    String curentTab = "";
    private DrawerLayout drawer;
    private Fragment fragment;
    FragmentTransaction tx;
    SharedPrefsUtils sharedPref;
    private TabLayout tabLayout;
    boolean isLoged;
    String userId="";
    SliderLayout sliderLayout;

    private PublisherAdView googleAdView;





    public static MainScreenActivity myContext;

    @Override
    protected void onResume() {
        super.onResume();

        try {
            isLoged = sharedPref.getBooleanPreference(getApplicationContext(), appConstants.isLoggedIn, false);
            userId = sharedPref.getStringPreference(getApplicationContext(), appConstants.userID_KEY);

            if (isLoged)
            {
                String userEmail = sharedPref.getStringPreference(getApplicationContext(), appConstants.userEmail_KEY);
                logiBTN.setText("تسجيل الخروج");
                profileNameTXT.setText(userEmail);

            } else
                {
                    logiBTN.setVisibility(View.GONE);
                    logiBTN.setText("تسجيل الدخول");
                profileNameTXT.setText("");
            }
        }
        catch (Exception xx){}
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main_screen_layout);

        myContext = MainScreenActivity.this;

        googleAdView = (PublisherAdView) findViewById(R.id.googleAdView);
        PublisherAdRequest adRequest = new PublisherAdRequest.Builder().build();
        googleAdView.loadAd(adRequest);


        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        drawer = (DrawerLayout) findViewById(R.id.drawerLayout);

        screenTitleTXT = findViewById(R.id.screenTitleTXT);
        screenTitleTXT.setText("الرئيسية");
        backBTN = findViewById(R.id.backBTN);
        backBTN.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                onBackPressed();
            }
        });


        //setSupportActionBar(toolbar);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, drawer, toolbar,
                R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        sliderLayout = findViewById(R.id.sliderdimjila);
        sliderLayout.setPresetTransformer(SliderLayout.Transformer.Default);
        //sliderLayout.setPresetIndicator(SliderLayout.PresetIndicators.Center_Bottom);
        //sliderLayout.setCustomAnimation(new DescriptionAnimation());
        sliderLayout.setDuration(3000);
        sliderLayout.addOnPageChangeListener(this);


        tabLayout = (TabLayout) findViewById(R.id.tab_layout);
        // tabLayout.setBackground(getResources().getColor(R.color.Black_fateh));
        tabLayout.addTab(tabLayout.newTab().setCustomView(R.layout.more_custom_tab).setTag("0"));
        tabLayout.addTab(tabLayout.newTab().setCustomView(R.layout.exchange_custom_tab).setTag("1"));
        tabLayout.addTab(tabLayout.newTab().setCustomView(R.layout.pricelist_custom_tab).setTag("2"));
        tabLayout.addTab(tabLayout.newTab().setCustomView(R.layout.home_custom_tab).setTag("3"));


        tabLayout.setSelectedTabIndicatorColor(getResources().getColor(R.color.transpernt));
        tabLayout.setTabTextColors(getResources().getColor(R.color.Black), getResources().getColor(R.color.zahry));
        //tabLayout.setBackgroundColor(getResources().getColor(R.color.zahry));
        tabLayout.setTabGravity(TabLayout.GRAVITY_FILL);


//        GradientDrawable drawable = new GradientDrawable();
//        drawable.setColor(Color.GRAY);
//        drawable.setSize(1, 0);


        navigationView = (NavigationView) findViewById(R.id.nav_view);
//        navigationView.inflateMenu(R.menu.menu_base);
//        assert navigationView != null;

        LayoutInflater inflater = (LayoutInflater) this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        View vi = inflater.inflate(R.layout.new_add_custom_tab, null);

        navigationView.inflateMenu(R.menu.menu_base);
        assert navigationView != null;
        navigationView.setNavigationItemSelectedListener(this);
        navigationView.inflateHeaderView(R.layout.nav_header);

        View header = navigationView.getHeaderView(0);

        logiBTN = (TextView) header.findViewById(R.id.logiBTN);
        navImageView = (ImageView) header.findViewById(R.id.profile_image);
        profileNameTXT = (TextView) header.findViewById(R.id.profile_name);

        logiBTN.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                boolean isLoged = sharedPref.getBooleanPreference(getApplicationContext(), appConstants.isLoggedIn, false);

                if (isLoged)
                {
                    sharedPref.setStringPreference(getApplicationContext(), appConstants.userID_KEY, "");
                    sharedPref.setStringPreference(getApplicationContext(), appConstants.userMobile_KEY, "");
                    sharedPref.setStringPreference(getApplicationContext(), appConstants.userEmail_KEY, "");
                    sharedPref.setStringPreference(getApplicationContext(), appConstants.userToken_KEY, "");
                    sharedPref.setBooleanPreference(getApplicationContext(), appConstants.isLoggedIn, false);

                    Intent i = getBaseContext().getPackageManager()
                            .getLaunchIntentForPackage(getBaseContext().getPackageName());
                    i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_WHEN_TASK_RESET);
                    startActivity(i);

                } else
                    {
                    Intent intent = new Intent(MainScreenActivity.this, LoginSubscriptionActivity.class);
                    startActivity(intent);

                }
            }
        });

//        navImageView.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//
//                boolean isLoged = sharedPref.getBooleanPreference(getApplicationContext(), appConstants.isLoggedIn, false);
//
//                if (isLoged)
//                {
////                    Intent intent2 = new Intent(MainScreenActivity.this, MyAccountActivity.class);
////                    startActivity(intent2);
//
//                }
//                else
//                {
//                    Intent intent = new Intent(MainScreenActivity.this, LoginActivity.class);
//                    startActivity(intent);
//
//                }
//            }
//        });


        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                if (tab.getTag().equals("0")) {

                    drawer.openDrawer(Gravity.LEFT);


                } else if (tab.getTag().equals("1")) {
                    FragmentTransaction tx = getSupportFragmentManager().beginTransaction();
                    tx.replace(R.id.main_container, new ExchangeFragment());
                    tx.addToBackStack(null);
                    tx.commit();
                } else if (tab.getTag().equals("2")) {
                    FragmentTransaction tx = getSupportFragmentManager().beginTransaction();
                    tx.replace(R.id.main_container, new PricesListFragment());
                    tx.addToBackStack(null);
                    tx.commit();
                } else if (tab.getTag().equals("3")) {

                    FragmentTransaction tx = getSupportFragmentManager().beginTransaction();
                    tx.replace(R.id.main_container, new MainCategoryNewsFragment());
                    tx.addToBackStack(null);
                    tx.commit();

                }

            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

                if (tab.getTag().equals("0")) {

                    drawer.openDrawer(Gravity.LEFT);


                } else if (tab.getTag().equals("1")) {
                    FragmentTransaction tx = getSupportFragmentManager().beginTransaction();
                    tx.replace(R.id.main_container, new ExchangeFragment());
                    tx.addToBackStack(null);
                    tx.commit();
                } else if (tab.getTag().equals("2")) {
                    FragmentTransaction tx = getSupportFragmentManager().beginTransaction();
                    tx.replace(R.id.main_container, new PricesListFragment());
                    tx.addToBackStack(null);
                    tx.commit();
                } else if (tab.getTag().equals("3")) {
                    FragmentTransaction tx = getSupportFragmentManager().beginTransaction();
                    tx.replace(R.id.main_container, new MainCategoryNewsFragment());
                    tx.addToBackStack(null);
                    tx.commit();

                }

            }
        });

//        appConstants.startSpinwheel(getApplicationContext(), false, true);
//        new Thread(new Runnable() {
//            @Override
//            public void run() {
//                try {
//                    getInnerBottomAdsReq(urlClass.getInnerBottomAdsURL, null);
//                } catch (Exception xx) {
//                }
//            }
//        }).start();

        try {
            TelephonyManager tm = (TelephonyManager) this.getSystemService(Context.TELEPHONY_SERVICE);
            String countryCodeValue = tm.getSimCountryIso();
            appConstants.zipCode = countryCodeValue;
        }
        catch (Exception xx){}


        appConstants.startSpinwheel(getApplicationContext(), false, true);
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    getSocilaLinksReq(urlClass.getSocialLinksURL, null);
                } catch (Exception xx) {
                }
            }
        }).start();


        new Thread(new Runnable() {
            @Override
            public void run() {
                try
                {
                    String phoneNumber=sharedPref.getStringPreference(getApplicationContext(),appConstants.userMobile_KEY);
                    String url=urlClass.updateLocationUSURL+phoneNumber+"&countryCode="+appConstants.zipCode;
                    updateLocationRequest(url, null);

                } catch (Exception xx) {
                }
            }
        }).start();


        Intent intent = getIntent();
        String catID = "";
        if (intent.getExtras() != null)
        {

               catID=intent.getStringExtra("catID");

                if(catID.equalsIgnoreCase("1"))
                {
                    appConstants.categoryTypeFlag="currency" ;
                }
                else   if(catID.equalsIgnoreCase("2"))
                {
                    appConstants.categoryTypeFlag="gold";
                }
                else   if(catID.equalsIgnoreCase("3"))
                {
                    appConstants.categoryTypeFlag="oil";
                }
                else   if(catID.equalsIgnoreCase("4"))
                {
                    appConstants.categoryTypeFlag="bitcoin";

                    BitcoinDetailsFragment.itemID="";
                    BitcoinDetailsFragment.itemName="Bitcoin";
                    BitcoinDetailsFragment.lastUpdate="";

                    tx = getSupportFragmentManager().beginTransaction();
                    tx.replace(R.id.main_container, new BitcoinDetailsFragment());
                    //tx.addToBackStack(null);
                    tx.commit();

                    return;
                }
                else  if(catID.equalsIgnoreCase("5"))
                {
                    appConstants.categoryTypeFlag="material";
                }
                else  if(catID.equalsIgnoreCase("6"))
                {
                    appConstants.categoryTypeFlag="food";
                }
                else  if(catID.equalsIgnoreCase("7"))
                {
                    appConstants.categoryTypeFlag="meat";
                }
                else  if(catID.equalsIgnoreCase("8"))
                {
                    appConstants.categoryTypeFlag="veg";
                }
                else  if(catID.equalsIgnoreCase("9"))
                {
                    appConstants.categoryTypeFlag="fruit";
                }
                else  if(catID.equalsIgnoreCase("10"))
                {
                    appConstants.categoryTypeFlag="wmeat";
                }


            tx =getSupportFragmentManager().beginTransaction();
            tx.replace(R.id.main_container, new CategoryDetailsFragment());
            //tx.addToBackStack(null);
            tx.commit();


        }
        else
        {

            FragmentTransaction tx = getSupportFragmentManager().beginTransaction();
            tx.replace(R.id.main_container, new MainCategoryNewsFragment());
            // tx.addToBackStack(null);
            tx.commit();
        }


    }


    @Override
    public boolean onNavigationItemSelected(MenuItem item) {

        Intent intent;

        switch (item.getItemId()) {


            case R.id.home_dr:
                fragment = new MainCategoryNewsFragment();
                tx = getSupportFragmentManager().beginTransaction();
                tx.replace(R.id.main_container, fragment);
                tx.addToBackStack(null);
                tx.commit();
                break;

            case R.id.pricelist_dr:
                fragment = new PricesListFragment();
                tx = getSupportFragmentManager().beginTransaction();
                tx.replace(R.id.main_container, fragment);
                tx.addToBackStack(null);
                tx.commit();
                break;

            case R.id.shop_dr:
                fragment = new ShopListMainFragment();
                tx = getSupportFragmentManager().beginTransaction();
                tx.replace(R.id.main_container, fragment);
                tx.addToBackStack(null);
                tx.commit();
                break;

            case R.id.setting_dr:
                fragment = new SettingFragment();
                tx = getSupportFragmentManager().beginTransaction();
                tx.replace(R.id.main_container, fragment);
                tx.addToBackStack(null);
                tx.commit();
                break;

            case R.id.about_dr:
                fragment = new AboutUsFragment();
                tx = getSupportFragmentManager().beginTransaction();
                tx.replace(R.id.main_container, fragment);
                tx.addToBackStack(null);
                tx.commit();
                break;

            case R.id.contact_dr:
                fragment = new ContactUsFragment();
                tx = getSupportFragmentManager().beginTransaction();
                tx.replace(R.id.main_container, fragment);
                tx.addToBackStack(null);
                tx.commit();
                break;

            case R.id.terms_dr:
                fragment = new TermsConditionFragment();
                tx = getSupportFragmentManager().beginTransaction();
                tx.replace(R.id.main_container, fragment);
                tx.addToBackStack(null);
                tx.commit();
                break;

            case R.id.facebook_dr:

                openWebURL(getApplicationContext(),appConstants.facebookLINK);
                break;

            case R.id.twitter_dr:

                openWebURL(getApplicationContext(),appConstants.twitterLINK);
                break;

            case R.id.youtube_dr:

                openWebURL(getApplicationContext(),appConstants.youtubeLINK);
                break;

            case R.id.share_dr:

                Intent sendIntent = new Intent();
                sendIntent.setAction(Intent.ACTION_SEND);
                sendIntent.putExtra(Intent.EXTRA_TEXT, "https://play.google.com/store/apps/details?id=com.becam.android.app");
                sendIntent.setType("text/plain");
                startActivity(sendIntent);


                break;





      }

        drawer.closeDrawer(Gravity.LEFT);
        return true;
    }


    public static void openWebURL(Context context, String url) {
        try {
            if(!url.equalsIgnoreCase("")) {
                if (!url.startsWith("http://") && !url.startsWith("https://")) {
                    url = "http://" + url;
                    Intent browserIntent = new Intent(Intent.ACTION_VIEW,
                            Uri.parse(url));
                    context.startActivity(browserIntent);
                } else {
                    Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
                    context.startActivity(browserIntent);
                }
            }
        }
        catch (Exception xx){}


    }

    @Override
    public void onBackPressed() {

        try {



            DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawerLayout);
            if (drawer.isDrawerOpen(Gravity.LEFT)) {
                drawer.closeDrawer(Gravity.LEFT);
            }

            else {

                int fragments = getSupportFragmentManager().getBackStackEntryCount();
                if (fragments == 0) {
                    new AlertDialog.Builder(this)
                            .setIcon(R.mipmap.icon)
                            .setMessage(R.string.exit_msg)
                            .setCancelable(false)
                            .setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    MainScreenActivity.this.finish();
                                }
                            })
                            .setNegativeButton(R.string.no, null)
                            .show();
                } else {

                    super.onBackPressed();
                }

            }
        }
        catch (Exception xx){}

    }


    private void getInnerBottomAdsReq(String urlPost, final JSONObject jsonObject) {

        final RequestQueue queue;
        StringRequest stringRequest = null;

        try {

            queue = Volley.newRequestQueue(MainScreenActivity.this);

            stringRequest = new StringRequest(Request.Method.GET, urlPost,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response)
                        {
                            try
                            {
                                if(response!=null)
                                {
                                    appConstants.stopSpinWheel();
                                    JSONArray arr = new JSONArray(response.toString());

                                    for(int i=0;i<arr.length();i++)
                                    {
                                        JSONObject obj=arr.getJSONObject(i);

                                        String image1 =obj.getString("image");

                                        DefaultSliderView textSliderView = new DefaultSliderView(getApplicationContext());
                                        textSliderView
                                                .description("")
                                                .image(image1)
                                                .setScaleType(BaseSliderView.ScaleType.Fit)
                                                .setOnSliderClickListener(MainScreenActivity.this);
                                        textSliderView.bundle(new Bundle());
                                        textSliderView.getBundle()
                                                .putString("extra", "");

                                        sliderLayout.addSlider(textSliderView);
                                    }

                                }


                            }
                            catch (Exception e)
                            {
                                e.printStackTrace();
                                appConstants.stopSpinWheel();
                            }


                        }
                    }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(final VolleyError error) {

                   // Toast.makeText(MainScreenActivity.this, "server error", Toast.LENGTH_SHORT).show();
                    appConstants.stopSpinWheel();


                }


            })


            {

//                @Override
//                protected Map<String, String> getParams() {
//
//                    Map<String, String> params = new HashMap<String, String>();
//                    try {
//
//                        Iterator<?> keys = jsonObject.keys();
//
//                        while (keys.hasNext()) {
//                            String key = (String) keys.next();
//                            String value = jsonObject.getString(key);
//                            params.put(key, value);
//
//                        }
//
//
//                    } catch (Exception xx) {
//                        xx.toString();
//                        appConstants.stopSpinWheel();
//                    }
//                    return params;
//                }

//
//                @Override
//                protected Response<String> parseNetworkResponse(NetworkResponse response) {
//                    try {
//
//                        String jsonString = new String(response.data,
//                                HttpHeaderParser.parseCharset(response.headers));
//
//                        return Response.success(jsonString, HttpHeaderParser.parseCacheHeaders(response));
//
//
//                    } catch (UnsupportedEncodingException e) {
//                        return Response.error(new ParseError(e));
//                    }
//                }


            };

            stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                    appConstants.appTimeOut,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

            queue.add(stringRequest);


        } catch (Exception e) {
            appConstants.stopSpinWheel();
            e.toString();
        }



    }


    private void getSocilaLinksReq(String urlPost, final JSONObject jsonObject) {

        final RequestQueue queue;
        StringRequest stringRequest = null;

        try {

            queue = Volley.newRequestQueue(MainScreenActivity.this);

            stringRequest = new StringRequest(Request.Method.GET, urlPost,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response)
                        {
                            try
                            {
                                if(response!=null)
                                {
                                    appConstants.stopSpinWheel();
                                    JSONArray arr = new JSONArray(response.toString());

                                    for(int i=0;i<arr.length();i++)
                                    {
                                        JSONObject obj=arr.getJSONObject(i);

                                        String id =obj.getString("id");
                                        String link =obj.getString("url");

                                        if(id.equalsIgnoreCase("1"))
                                        {
                                           appConstants.facebookLINK=link;
                                        }
                                        else   if(id.equalsIgnoreCase("2"))
                                        {
                                            appConstants.twitterLINK=link;
                                        }
                                        else   if(id.equalsIgnoreCase("3"))
                                        {
                                            appConstants.youtubeLINK=link;
                                        }

                                        }

                                }


                            }
                            catch (Exception e)
                            {
                                e.printStackTrace();
                                appConstants.stopSpinWheel();
                            }


                        }
                    }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(final VolleyError error) {

                    //Toast.makeText(MainScreenActivity.this, "server error", Toast.LENGTH_SHORT).show();
                    appConstants.stopSpinWheel();


                }


            })


            {

//                @Override
//                protected Map<String, String> getParams() {
//
//                    Map<String, String> params = new HashMap<String, String>();
//                    try {
//
//                        Iterator<?> keys = jsonObject.keys();
//
//                        while (keys.hasNext()) {
//                            String key = (String) keys.next();
//                            String value = jsonObject.getString(key);
//                            params.put(key, value);
//
//                        }
//
//
//                    } catch (Exception xx) {
//                        xx.toString();
//                        appConstants.stopSpinWheel();
//                    }
//                    return params;
//                }

//
//                @Override
//                protected Response<String> parseNetworkResponse(NetworkResponse response) {
//                    try {
//
//                        String jsonString = new String(response.data,
//                                HttpHeaderParser.parseCharset(response.headers));
//
//                        return Response.success(jsonString, HttpHeaderParser.parseCacheHeaders(response));
//
//
//                    } catch (UnsupportedEncodingException e) {
//                        return Response.error(new ParseError(e));
//                    }
//                }


            };

            stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                    appConstants.appTimeOut,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

            queue.add(stringRequest);


        } catch (Exception e) {
            appConstants.stopSpinWheel();
            e.toString();
        }



    }

    private void updateLocationRequest(String urlPost, final JSONObject jsonObject) {

        final RequestQueue queue;
        StringRequest stringRequest = null;

        try {

            queue = Volley.newRequestQueue(getApplicationContext());

            stringRequest = new StringRequest(Request.Method.GET, urlPost,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response)
                        {
                            try
                            {
                                if(response!=null)
                                {
                                    appConstants.stopSpinWheel();
                                    JSONArray arr = new JSONArray(response.toString());

                                    JSONObject mainObject=arr.getJSONObject(0);

                                    String result = mainObject.getString("status");
                                    if(result.equalsIgnoreCase("success"))
                                    {
                                        //Toast.makeText(getActivity(), "server error", Toast.LENGTH_SHORT).show();
                                    }
                                    else
                                    {
                                        // Toast.makeText(getActivity(), result, Toast.LENGTH_SHORT).show();
                                    }


                                }


                            }
                            catch (Exception e)
                            {
                                e.printStackTrace();
                                appConstants.stopSpinWheel();
                            }


                        }
                    }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(final VolleyError error) {

                    //Toast.makeText(getActivity(), "server error", Toast.LENGTH_SHORT).show();
                    appConstants.stopSpinWheel();


                }


            })


            {




            };

            stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                    appConstants.appTimeOut,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

            queue.add(stringRequest);


        } catch (Exception e) {
            appConstants.stopSpinWheel();
            e.toString();
        }



    }




    @Override
    public void onSliderClick(BaseSliderView slider) {

    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

    }

    @Override
    public void onPageSelected(int position) {

    }

    @Override
    public void onPageScrollStateChanged(int state) {

    }
}