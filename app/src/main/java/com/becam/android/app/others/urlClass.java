package com.becam.android.app.others;

/**
 * Created by HP on 5/8/2017.
 */

public class urlClass
{

   // public static String  baseURL="http://74.124.193.110/~demo/iqtisadia/api/";
    public static String  baseURL="http://bekam.net/api/";


    public static String getMainAdsURL=baseURL+"main_ads.php";
    public static String getInnerTopAdsURL=baseURL+"main_ads.php?inner_ads";
    public static String getInnerBottomAdsURL=baseURL+"main_ads.php?inner_ads_bottom";
    public static String getBreakNewsURL=baseURL+"news.php";
    public static String getTermsPagesURL=baseURL+"information_page.php";
    public static String getAboutPagesURL=baseURL+"information_page.php";
    public static String getNewsDetailsURL="http://libyanbusiness.tv/news-list/postbyid.php?post_id=";
    public static String postUserInfoURL=baseURL+"user_info.php?newUser";
    public static String getCategoryWithShareURL=baseURL+"list_category.php";
    public static String getCurencConvertListURL=baseURL+"convert.php?list";
    public static String getConvertResultURL=baseURL+"convert.php?";
    public static String getCurencyListURL=baseURL+"internalItems.php?listCurrency";
    public static String getOilListURL=baseURL+"internalItems.php?listOil";
    public static String getGoldListURL=baseURL+"internalItems.php?listGold";
    public static String getMaterialistURL=baseURL+"internalItems.php?buildingmaterials";
    public static String getMaterialDetailsURL=baseURL+"buildingmadetails.php?item_id=";
    public static String getMaterialChartURL=baseURL+"buildingmaReport.php?item_id=";
    public static String getCurencyDetailsURL=baseURL+"currencydetails.php?currency_id=";
    public static String getGoldDetailsURL=baseURL+"golddetails.php?gold_id=";
    public static String getOilDetailsURL=baseURL+"oildetails.php?oil_name=";
    public static String getBitCoinDetailsURL=baseURL+"bitcoindetails.php?bitcoin";
    public static String updateTokenURL=baseURL+"user_info.php?updateToken&phoneNumber=";
    public static String notificationONOffURL=baseURL+"update_notifications.php?phoneNumber=";
    public static String getCurencyChartURL=baseURL+"currencyReport.php?currency_id=";
    public static String getOilChartURL=baseURL+"oilReport.php?oil_name=";
    public static String getGoldChartURL=baseURL+"goldReport.php?gold_id=";
    public static String getSocialLinksURL=baseURL+"social_link.php";
    public static String getUserSettingURL=baseURL+"user_info.php?phoneNumber=";

    public static String getMainNewsCategoryURL=baseURL+"resources.php";


    public static String getFoodlistURL=baseURL+"internalItems.php?grocery";
    public static String getFoodChartURL=baseURL+"groceryReport.php?item_id=";
    public static String getFoodDetailsURL=baseURL+"grocerydetails.php?item_id=";

    public static String getMeatlistURL=baseURL+"internalItems.php?redmeats";
    public static String getMeatChartURL=baseURL+"redmeatReport.php?item_id=";
    public static String getMeatDetailsURL=baseURL+"redmeatdetails.php?item_id=";

    public static String getWhiteMeatlistURL=baseURL+"internalItems.php?whitemeats";
    public static String getWhiteMeatChartURL=baseURL+"whitemeatReport.php?item_id=";
    public static String getWhiteMeatDetailsURL=baseURL+"whitemeatdetails.php?item_id=";

    public static String getVeglistURL=baseURL+"internalItems.php?vegetables";
    public static String getVegChartURL=baseURL+"vegetablesReport.php?item_id=";
    public static String getVegDetailsURL=baseURL+"vegetablesdetails.php?item_id=";

    public static String getFruitlistURL=baseURL+"internalItems.php?fruits";
    public static String getFruitChartURL=baseURL+"fruitsReport.php?item_id=";
    public static String getFruitDetailsURL=baseURL+"fruitsdetails.php?item_id=";

    public static String getLocalShopURL=baseURL+"agents.php?local";
    public static String getInternationlShopURL=baseURL+"agents.php?international";
    public static String getShopDetailsURL=baseURL+"agent_details.php?";

    public static String contactUSURL=baseURL+"contact_us.php?email=";
    public static String updateLocationUSURL=baseURL+"update_location.php?phoneNumber=";



}