package com.becam.android.app.models;

/**
 * Created by HP on 3/25/2018.
 */

public class InternationalShopModel {

    private String id;
    private String name;
    private String image;
    private String country;

    public InternationalShopModel(String id, String name, String image, String country) {
        this.id = id;
        this.name = name;
        this.image = image;
        this.country = country;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }
}
