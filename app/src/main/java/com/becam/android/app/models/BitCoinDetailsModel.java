package com.becam.android.app.models;

public class BitCoinDetailsModel {

    private String id;
    private String open;
    private String high;
    private String low;
    private String close;
    private String volum_btc;
    private String volum_currency;
    private String weight_price;
    private String date;

    public BitCoinDetailsModel(String id, String open, String high, String low, String close, String volum_btc, String volum_currency, String weight_price, String date) {
        this.id = id;
        this.open = open;
        this.high = high;
        this.low = low;
        this.close = close;
        this.volum_btc = volum_btc;
        this.volum_currency = volum_currency;
        this.weight_price = weight_price;
        this.date = date;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getOpen() {
        return open;
    }

    public void setOpen(String open) {
        this.open = open;
    }

    public String getHigh() {
        return high;
    }

    public void setHigh(String high) {
        this.high = high;
    }

    public String getLow() {
        return low;
    }

    public void setLow(String low) {
        this.low = low;
    }

    public String getClose() {
        return close;
    }

    public void setClose(String close) {
        this.close = close;
    }

    public String getVolum_btc() {
        return volum_btc;
    }

    public void setVolum_btc(String volum_btc) {
        this.volum_btc = volum_btc;
    }

    public String getVolum_currency() {
        return volum_currency;
    }

    public void setVolum_currency(String volum_currency) {
        this.volum_currency = volum_currency;
    }

    public String getWeight_price() {
        return weight_price;
    }

    public void setWeight_price(String weight_price) {
        this.weight_price = weight_price;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }
}
