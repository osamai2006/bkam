package com.becam.android.app.models;

/**
 * Created by HP on 3/25/2018.
 */

public class LocalShopModel {

    private String id;
    private String name;
    private String image;
    private String country;

    public LocalShopModel(String id, String name, String image, String country) {
        this.setId(id);
        this.setName(name);
        this.setImage(image);
        this.setCountry(country);
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }
}
