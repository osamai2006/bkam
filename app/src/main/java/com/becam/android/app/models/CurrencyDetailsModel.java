package com.becam.android.app.models;

public class CurrencyDetailsModel {

    private String id;
    private String name;
    private String sale;
    private String buy;
    private String status;
    private String date;

    public CurrencyDetailsModel(String id, String name, String sale, String buy, String status, String date) {
        this.setId(id);
        this.setName(name);
        this.setSale(sale);
        this.setBuy(buy);
        this.setStatus(status);
        this.setDate(date);
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSale() {
        return sale;
    }

    public void setSale(String sale) {
        this.sale = sale;
    }

    public String getBuy() {
        return buy;
    }

    public void setBuy(String buy) {
        this.buy = buy;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }
}