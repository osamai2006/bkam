package com.becam.android.app.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.becam.android.app.R;
import com.becam.android.app.models.CurrencyDetailsChartModel;
import com.becam.android.app.models.CurrencyDetailsModel;
import com.becam.android.app.models.SharesModel;

import java.util.ArrayList;

/**
 * Created by Ayadi on 2/6/2018.
 */

public class CurrencyDetailsAdapter extends RecyclerView.Adapter<CurrencyDetailsAdapter.MenuItemViewHolder> {

    Context context;
    ArrayList<CurrencyDetailsModel> resourcelList;





    public CurrencyDetailsAdapter(Context context, ArrayList<CurrencyDetailsModel> ResourcelList) {
        this.context = context;
        resourcelList = ResourcelList;
    }

    @Override
    public MenuItemViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.currency_details_item_row, parent, false);
        MenuItemViewHolder viewholder = new MenuItemViewHolder(view);
        return viewholder;
    }

    @Override
    public void onBindViewHolder(MenuItemViewHolder holder, final int position) {

        holder.dateTXT.setText(resourcelList.get(position).getDate());
        holder.saleTXT.setText(resourcelList.get(position).getSale());
        holder.buyTXT.setText(resourcelList.get(position).getBuy());



        //Picasso.with(context).load(resourcelList.get(position).getStatus()).into(holder.statusImg);

        if (resourcelList.get(position).getStatus().equalsIgnoreCase("fixed"))
        {
            // holder.shareStatusIMG.setImageResource(R.drawable.up_arrow);

            holder.statusImg.setImageResource(R.drawable.fixed);

        }else  if (resourcelList.get(position).getStatus().equalsIgnoreCase("up"))
        {

            holder.statusImg.setImageResource(R.drawable.up);


        }else  if (resourcelList.get(position).getStatus().equalsIgnoreCase("down"))
        {
            holder.statusImg.setImageResource(R.drawable.down);

        }
        else
        {
            holder.statusImg.setImageResource(R.drawable.fixed);

        }






    }

    @Override
    public int getItemCount() {
        return resourcelList.size();
    }

    public static class MenuItemViewHolder extends RecyclerView.ViewHolder
    {

        TextView dateTXT ,saleTXT,buyTXT ;
        ImageView statusImg;



        public MenuItemViewHolder(View itemView)
        {
            super(itemView);
            dateTXT = itemView.findViewById(R.id.dateTXT);
            saleTXT = itemView.findViewById(R.id.saleTXT);
            buyTXT = itemView.findViewById(R.id.buyTXT);
            statusImg = itemView.findViewById(R.id.statusImg);

        }


    }




}
