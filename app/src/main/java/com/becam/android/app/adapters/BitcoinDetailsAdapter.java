package com.becam.android.app.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.becam.android.app.R;
import com.becam.android.app.fragment.HomeFragment;
import com.becam.android.app.models.BitCoinDetailsModel;
import com.becam.android.app.models.CategoryDetailsModel;
import com.becam.android.app.models.SharesModel;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;

import java.util.ArrayList;

/**
 * Created by Ayadi on 2/6/2018.
 */

public class BitcoinDetailsAdapter extends RecyclerView.Adapter<BitcoinDetailsAdapter.MenuItemViewHolder> {

    Context context;
    ArrayList<BitCoinDetailsModel> resourcelList;


    public BitcoinDetailsAdapter(Context context, ArrayList<BitCoinDetailsModel> ResourcelList) {
        this.context = context;
        resourcelList = ResourcelList;
    }

    @Override
    public MenuItemViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.bitcoin_details_item_row, parent, false);
        MenuItemViewHolder viewholder = new MenuItemViewHolder(view);
        return viewholder;
    }

    @Override
    public void onBindViewHolder(MenuItemViewHolder holder, final int position) {

        try {


            holder.highTXT.setText(resourcelList.get(position).getHigh());
            holder.openTXT.setText(resourcelList.get(position).getOpen());
            holder.closeTXT.setText(resourcelList.get(position).getClose());
            holder.lowTXT.setText(resourcelList.get(position).getLow());
            holder.btcTXT.setText(resourcelList.get(position).getVolum_btc());
            holder.currencyTXT.setText(resourcelList.get(position).getVolum_currency());
            holder.weightTXT.setText(resourcelList.get(position).getWeight_price());
            holder.lastUpdateTXT.setText(resourcelList.get(position).getDate());

        //    Glide.with(context).load(resourcelList.get(position).getFlagImg()).apply(RequestOptions.circleCropTransform()).into(holder.flagImg);




            holder.mainLO.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {




                }
            });

        }
        catch (Exception xx)
        {

        }


    }

    @Override
    public int getItemCount() {
        return resourcelList.size();
    }

    public static class MenuItemViewHolder extends RecyclerView.ViewHolder
    {

        TextView openTXT ,closeTXT, highTXT,lowTXT,btcTXT,currencyTXT,weightTXT,lastUpdateTXT ;
        LinearLayout mainLO;


        public MenuItemViewHolder(View itemView)
        {
            super(itemView);
            openTXT = itemView.findViewById(R.id.openTXT);
            closeTXT = itemView.findViewById(R.id.closeTXT);
            highTXT = itemView.findViewById(R.id.highTXT);
            lowTXT= itemView.findViewById(R.id.lowTXT);
            btcTXT= itemView.findViewById(R.id.btcTXT);
            currencyTXT = itemView.findViewById(R.id.currencyTXT);
            weightTXT= itemView.findViewById(R.id.weightTXT);
            mainLO = itemView.findViewById(R.id.mainLO);
            lastUpdateTXT= itemView.findViewById(R.id.lastUpdateTXT);


        }


    }




}
