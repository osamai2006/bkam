package com.becam.android.app.fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkResponse;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HttpHeaderParser;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.becam.android.app.R;
import com.becam.android.app.activity.MainScreenActivity;
import com.becam.android.app.others.appConstants;
import com.becam.android.app.others.urlClass;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

/**
 * Created by HP on 5/9/2017.
 */

public class ContactUsFragment extends Fragment  {

    View view;
    EditText subject_contact_txt,emailTXT,mesgTXT;
    Button submitBTN;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.contact_us_layout, container, false);

        MainScreenActivity.screenTitleTXT.setText(getResources().getString(R.string.contact_dr));
        subject_contact_txt=view.findViewById(R.id.subject_contact_txt);
        emailTXT=view.findViewById(R.id.email_contact_txt);
        mesgTXT=view.findViewById(R.id.mesg_contacttxt);

        submitBTN =view.findViewById(R.id.submit_contactBTN);
        submitBTN.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                if(checkRule()) {
                    appConstants.startSpinwheel(getActivity(), false, true);

                    new Thread(new Runnable() {
                        @Override
                        public void run() {

                            try {
                                String url = urlClass.contactUSURL+emailTXT.getText().toString().trim()+
                                        "&subject="+subject_contact_txt.getText().toString().trim().replace(" ","%20")+
                                         "&message="+ mesgTXT.getText().toString().trim().replace(" ","%20");


                                postContactusRequest(url, null);

                            } catch (Exception xx) {
                                xx.toString();
                                appConstants.stopSpinWheel();
                            }
                        }
                    }).start();
                }
            }
        });



        return view;
    }
    private  boolean checkRule()
    {
        if (subject_contact_txt.getText().toString().trim().equals(""))
        {
            Toast.makeText(getActivity(), getResources().getString(R.string.fill_empty), Toast.LENGTH_SHORT).show();
            return false;
        }
        if (emailTXT.getText().toString().trim().equals("")) {
            Toast.makeText(getActivity(), getResources().getString(R.string.fill_empty), Toast.LENGTH_SHORT).show();
            return false;
        }
        if (!emailTXT.getText().toString().trim().matches(appConstants.email_pattern)) {
            Toast.makeText(getActivity(), "Email not correct", Toast.LENGTH_SHORT).show();
            return false;
        }

        if (mesgTXT.getText().toString().trim().equals("")) {
            Toast.makeText(getActivity(), getResources().getString(R.string.fill_empty), Toast.LENGTH_SHORT).show();
            return false;
        }


        return true;


    }

    private void postContactusRequest(String urlPost, final JSONObject jsonObject) {

        appConstants.startSpinwheel(getActivity(),false,true);
        final RequestQueue queue;
        StringRequest stringRequest = null;

        try {


            queue = Volley.newRequestQueue(getActivity());

            stringRequest = new StringRequest(Request.Method.GET, urlPost,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            try
                            {

                                if(response !=null)
                                {
                                    appConstants.stopSpinWheel();

                                    JSONArray jsonResp = new JSONArray(response.toString());
                                    final JSONObject mainObj=jsonResp.getJSONObject(0);
                                    String status=mainObj.getString("status");

                                    Toast.makeText(getActivity(), status, Toast.LENGTH_LONG).show();
                                    getActivity().getSupportFragmentManager().popBackStack();

                                }
                                else
                                {
                                    appConstants.stopSpinWheel();
                                   // Toast.makeText(getActivity(), "server error", Toast.LENGTH_LONG).show();
                                }



                            }
                            catch (Exception xx)
                            {
                                xx.toString();
                                appConstants.stopSpinWheel();
                            }


                        }
                    }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(final VolleyError error) {
                    String xx = error.toString();
                    Toast.makeText(getActivity(), R.string.connection_err, Toast.LENGTH_SHORT).show();
                    appConstants.stopSpinWheel();


                }


            })


            {



                @Override
                protected Map<String, String> getParams() {

                    Map<String, String> params = new HashMap<String, String>();
                    try {

                        Iterator<?> keys = jsonObject.keys();

                        while (keys.hasNext()) {
                            String key = (String) keys.next();
                            String value = jsonObject.getString(key);
                            params.put(key, value);

                        }


                    } catch (Exception xx) {
                        xx.toString();
                        appConstants.stopSpinWheel();
                    }
                    return params;
                }


                @Override
                protected Response<String> parseNetworkResponse(NetworkResponse response) {
                    try {

                        String jsonString = new String(response.data,
                                HttpHeaderParser.parseCharset(response.headers));

                        return Response.success(jsonString, HttpHeaderParser.parseCacheHeaders(response));


                    } catch (UnsupportedEncodingException e) {
                        return Response.error(new ParseError(e));
                    }
                }


            };

            stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                    appConstants.appTimeOut,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

            queue.add(stringRequest);


        } catch (Exception e) {
            appConstants.stopSpinWheel();
            e.toString();
        }



    }




}
