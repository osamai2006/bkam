package com.becam.android.app.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.becam.android.app.R;
import com.becam.android.app.models.OtherPricesDetailsModel;
import com.becam.android.app.models.SharesModel;

import java.util.ArrayList;

/**
 * Created by Ayadi on 2/6/2018.
 */

public class OtherPricesDetailsAdapter extends RecyclerView.Adapter<OtherPricesDetailsAdapter.MenuItemViewHolder> {

    Context context;
    ArrayList<OtherPricesDetailsModel> resourcelList;

    SharesModel sharesModel;
    SharesAdapter sharesAdapter;



    public OtherPricesDetailsAdapter(Context context, ArrayList<OtherPricesDetailsModel> ResourcelList) {
        this.context = context;
        resourcelList = ResourcelList;
    }

    @Override
    public MenuItemViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.other_price_details_item_row, parent, false);
        MenuItemViewHolder viewholder = new MenuItemViewHolder(view);
        return viewholder;
    }

    @Override
    public void onBindViewHolder(MenuItemViewHolder holder, final int position) {

        holder.dateMaterialTXT.setText(resourcelList.get(position).getDate());
        holder.priceMaterialTXT.setText(resourcelList.get(position).getPrice());



        //Picasso.with(context).load(resourcelList.get(position).getStatus()).into(holder.statusImg);

        if (resourcelList.get(position).getStatus().equalsIgnoreCase("fixed"))
        {
            // holder.shareStatusIMG.setImageResource(R.drawable.up_arrow);

            holder.statusMaterialImg.setImageResource(R.drawable.fixed);

        }else  if (resourcelList.get(position).getStatus().equalsIgnoreCase("up"))
        {

            holder.statusMaterialImg.setImageResource(R.drawable.up);


        }else  if (resourcelList.get(position).getStatus().equalsIgnoreCase("down"))
        {
            holder.statusMaterialImg.setImageResource(R.drawable.down);

        }
        else
        {
            holder.statusMaterialImg.setImageResource(R.drawable.fixed);

        }






    }

    @Override
    public int getItemCount() {
        return resourcelList.size();
    }

    public static class MenuItemViewHolder extends RecyclerView.ViewHolder
    {

        TextView dateMaterialTXT ,priceMaterialTXT ;
        ImageView statusMaterialImg;



        public MenuItemViewHolder(View itemView)
        {
            super(itemView);
            dateMaterialTXT = itemView.findViewById(R.id.dateMaterialTXT);
            priceMaterialTXT = itemView.findViewById(R.id.priceMaterialTXT);
            statusMaterialImg = itemView.findViewById(R.id.statusMaterialImg);

        }


    }




}
