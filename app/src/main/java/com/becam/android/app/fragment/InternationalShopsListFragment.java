package com.becam.android.app.fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.becam.android.app.R;
import com.becam.android.app.activity.MainScreenActivity;
import com.becam.android.app.adapters.InternationalShopAdapter;
import com.becam.android.app.adapters.LocalShopAdapter;
import com.becam.android.app.models.InternationalShopModel;
import com.becam.android.app.models.LocalShopModel;
import com.becam.android.app.others.appConstants;
import com.becam.android.app.others.urlClass;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by HP on 5/3/2018.
 */

public class InternationalShopsListFragment extends Fragment  {
    public static MainScreenActivity myContext;


    View view;

    RecyclerView shopsLV;


    InternationalShopModel model;
    ArrayList<InternationalShopModel> arrayList;
    InternationalShopAdapter adapter;


    @Override
    public void onAttach(Context context) {
        myContext=(MainScreenActivity) getActivity();
        super.onAttach(context);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.international_shop_list_fragment, container, false);
        shopsLV= view.findViewById(R.id.shopsLV);

        final SwipeRefreshLayout swipeToRefreshLayout = (SwipeRefreshLayout) view.findViewById(R.id.swipeTorefresh);


        swipeToRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                swipeToRefreshLayout.setRefreshing(true);


                swipeToRefreshLayout.setRefreshing(false);

            }
        });

        appConstants.startSpinwheel(getActivity(), false, true);

        new Thread(new Runnable() {
            @Override
            public void run() {
                try
                {
                    getInternationalShopRequest(urlClass.getInternationlShopURL, null);

                } catch (Exception xx) {
                }
            }
        }).start();


        return view;
    }


    private void getInternationalShopRequest(String urlPost, final JSONObject jsonObject) {

        final RequestQueue queue;
        StringRequest stringRequest = null;

        try {

            queue = Volley.newRequestQueue(getActivity());

            stringRequest = new StringRequest(Request.Method.GET, urlPost,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response)
                        {
                            try
                            {
                                if(response!=null)
                                {
                                    appConstants.stopSpinWheel();

                                    arrayList = new ArrayList<>();

                                    JSONArray arr = new JSONArray(response.toString());

                                    for (int i = 0; i < arr.length(); i++) {

                                        try {
                                            JSONObject mainObj=arr.getJSONObject(i);

                                            String id = mainObj.getString("id");
                                            String name = mainObj.getString("name");
                                            String image = mainObj.getString("image");
                                            String country = mainObj.getString("country_name");


                                            model = new InternationalShopModel(id, name, image,country);
                                            arrayList.add(model);

                                        } catch (Exception xx) {
                                            xx.toString();
                                        }
                                    }


                                    adapter = new InternationalShopAdapter(getActivity(), arrayList);
                                    shopsLV.setAdapter(adapter);
                                    LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
                                    shopsLV.setLayoutManager(layoutManager);

                                }


                            }
                            catch (Exception e)
                            {
                                e.printStackTrace();
                                appConstants.stopSpinWheel();
                            }


                        }
                    }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(final VolleyError error) {

                   // Toast.makeText(getActivity(), "server error", Toast.LENGTH_SHORT).show();
                    appConstants.stopSpinWheel();


                }


            });

            stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                    appConstants.appTimeOut,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

            queue.add(stringRequest);


        } catch (Exception e) {
            appConstants.stopSpinWheel();
            e.toString();
        }



    }


}
