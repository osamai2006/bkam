package com.becam.android.app.models;

/**
 * Created by HP on 3/25/2018.
 */

public class SharesModel {
    private String id;
    private String name;
    private String price;
    private String sale;
    private String buy;
    private String date;
    private String openPrice;
    private String closePrice;
    private String highPrice;
    private String lowPrice;
    private String volume_btc;
    private String volume_currency;
    private String weighted_Price;
    private String date_price;
    private String status;
    private String flag;

    public SharesModel(String id, String name, String price, String sale, String buy, String date, String openPrice, String closePrice, String highPrice, String lowPrice, String volume_btc, String volume_currency, String weighted_Price, String date_price, String status, String flag) {
        this.id = id;
        this.name = name;
        this.price = price;
        this.sale = sale;
        this.buy = buy;
        this.date = date;
        this.openPrice = openPrice;
        this.closePrice = closePrice;
        this.highPrice = highPrice;
        this.lowPrice = lowPrice;
        this.volume_btc = volume_btc;
        this.volume_currency = volume_currency;
        this.weighted_Price = weighted_Price;
        this.date_price = date_price;
        this.status = status;
        this.flag = flag;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getSale() {
        return sale;
    }

    public void setSale(String sale) {
        this.sale = sale;
    }

    public String getBuy() {
        return buy;
    }

    public void setBuy(String buy) {
        this.buy = buy;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getOpenPrice() {
        return openPrice;
    }

    public void setOpenPrice(String openPrice) {
        this.openPrice = openPrice;
    }

    public String getClosePrice() {
        return closePrice;
    }

    public void setClosePrice(String closePrice) {
        this.closePrice = closePrice;
    }

    public String getHighPrice() {
        return highPrice;
    }

    public void setHighPrice(String highPrice) {
        this.highPrice = highPrice;
    }

    public String getLowPrice() {
        return lowPrice;
    }

    public void setLowPrice(String lowPrice) {
        this.lowPrice = lowPrice;
    }

    public String getVolume_btc() {
        return volume_btc;
    }

    public void setVolume_btc(String volume_btc) {
        this.volume_btc = volume_btc;
    }

    public String getVolume_currency() {
        return volume_currency;
    }

    public void setVolume_currency(String volume_currency) {
        this.volume_currency = volume_currency;
    }

    public String getWeighted_Price() {
        return weighted_Price;
    }

    public void setWeighted_Price(String weighted_Price) {
        this.weighted_Price = weighted_Price;
    }

    public String getDate_price() {
        return date_price;
    }

    public void setDate_price(String date_price) {
        this.date_price = date_price;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getFlag() {
        return flag;
    }

    public void setFlag(String flag) {
        this.flag = flag;
    }
}
