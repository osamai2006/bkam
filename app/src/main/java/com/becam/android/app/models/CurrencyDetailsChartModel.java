package com.becam.android.app.models;

import org.json.JSONArray;

public class CurrencyDetailsChartModel {

    private String id;
    private String name;
    private String sale;
    private String buy;
    private String status;
    private String date;

    public CurrencyDetailsChartModel(String id, String name, String sale, String buy, String status, String date) {
        this.id = id;
        this.name = name;
        this.sale = sale;
        this.buy = buy;
        this.status = status;
        this.date = date;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSale() {
        return sale;
    }

    public void setSale(String sale) {
        this.sale = sale;
    }

    public String getBuy() {
        return buy;
    }

    public void setBuy(String buy) {
        this.buy = buy;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }
}
