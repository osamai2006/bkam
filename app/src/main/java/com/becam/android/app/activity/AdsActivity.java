package com.becam.android.app.activity;

import android.Manifest;
import android.app.Application;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.RequiresApi;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatSpinner;
import android.view.View;
import android.view.View.OnClickListener;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.VideoView;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkResponse;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HttpHeaderParser;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import com.becam.android.app.R;
import com.becam.android.app.models.AdsModel;
import com.becam.android.app.others.CWebVideoView;
import com.becam.android.app.others.SharedPrefsUtils;
import com.becam.android.app.others.appConstants;
import com.becam.android.app.others.urlClass;
import com.bumptech.glide.manager.LifecycleListener;
import com.pierfrancescosoffritti.androidyoutubeplayer.player.YouTubePlayer;
import com.pierfrancescosoffritti.androidyoutubeplayer.player.YouTubePlayerView;
import com.pierfrancescosoffritti.androidyoutubeplayer.player.listeners.AbstractYouTubePlayerListener;
import com.pierfrancescosoffritti.androidyoutubeplayer.player.listeners.YouTubePlayerFullScreenListener;
import com.pierfrancescosoffritti.androidyoutubeplayer.player.listeners.YouTubePlayerInitListener;
import com.pierfrancescosoffritti.androidyoutubeplayer.player.playerUtils.FullScreenHelper;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;


public class AdsActivity extends AppCompatActivity {


    AdsModel adsModel_;
    ArrayList<AdsModel> adsModelList_;
    ImageView tutorIMG;
    TextView skipBTN;
    int counter = 0;
    SharedPrefsUtils sharedPref;
    boolean isReadTutor = false;
    YouTubePlayerView youtubePlayerView;
    FullScreenHelper fullScreenHelper;


    @RequiresApi(api = Build.VERSION_CODES.O)
    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        if (requestCode == 12) {
            try {

                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    try
                    {
                        appConstants.serialNum = Build.SERIAL;

                    }
                    catch (Exception xx){}
                }
                else
                {
                    Toast.makeText(getApplicationContext(),"Permission not granted",Toast.LENGTH_LONG).show();
                    finish();
                }

            }
            catch (Exception xx){}
        }


    }


    @RequiresApi(api = Build.VERSION_CODES.O)
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.ads_activity);

        tutorIMG = (ImageView) findViewById(R.id.totorIMG);
        youtubePlayerView = findViewById(R.id.youtube_player_view);
        getLifecycle().addObserver(youtubePlayerView);
        fullScreenHelper=new FullScreenHelper();
        fullScreenHelper.enterFullScreen(youtubePlayerView);



        if(ActivityCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.READ_PHONE_STATE)
                != PackageManager.PERMISSION_GRANTED)
        {
            ActivityCompat.requestPermissions(AdsActivity.this, new String[]{android.Manifest.permission.READ_PHONE_STATE}, 12);

        }
        else
        {
            try
            {
                appConstants.serialNum = Build.SERIAL;

            }
            catch (Exception xx){}
        }


        skipBTN = (TextView) findViewById(R.id.skipBTN);
        skipBTN.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v)
            {

               // sharedPref.setBooleanPreference(AdsActivity.this, appConstants.isAdsRead_KEY,true);

                Intent i = new Intent(getApplicationContext(), LoginSubscriptionActivity.class);
                startActivity(i);
                finish();

            }
        });
        tutorIMG.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {

                if (adsModelList_.size()>0) {
                    if (!adsModelList_.get(counter).getUrl().equalsIgnoreCase("")) {
                        // openWebURL( adsModelList_.get(counter).getUrl());

                        try {
                            WebviewActivity.url = adsModelList_.get(counter).getUrl();
                            startActivity(new Intent(AdsActivity.this, WebviewActivity.class));
                        } catch (Exception xx) {
                        }
                    }
                }

//                counter+=1;
//                if(counter< adsModelList_.size())
//                {
//                    Picasso.with(getApplicationContext()).load(adsModelList_.get(counter).getImage())
//                            .into(tutorIMG);
//                }
//                else
//                {
//                    //sharedPref.setBooleanPreference(AdsActivity.this, appConstants.isAdsRead_KEY,true);
//
//
//                    Intent i = new Intent(getApplicationContext(), LoginSubscriptionActivity.class);
//                    startActivity(i);
//                    finish();
//                }


                }

        });


        //isReadTutor=sharedPref.getBooleanPreference(getApplicationContext(), appConstants.isAdsRead_KEY,false);

        if(isReadTutor==false)
        {
            appConstants.startSpinwheel(AdsActivity.this,false,true);

            //appConstants.showProgressDialog(this.getv);
            new Thread(new Runnable() {
                @Override
                public void run() {

                    try {

                        String  urlData = urlClass.getMainAdsURL;

                        getAdsReq(urlData, null);
                    }
                    catch (Exception xx)
                    {}

                }
            }).start();
        }
        else
        {
            Intent i = new Intent(getApplicationContext(), LoginSubscriptionActivity.class);
            startActivity(i);
            finish();
        }





    }
    private void getAdsReq(String urlPost, final JSONObject jsonObject) {

        final RequestQueue queue;
        StringRequest stringRequest = null;

        try {


            queue = Volley.newRequestQueue(getApplicationContext());

            stringRequest = new StringRequest(Request.Method.GET, urlPost,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            if(response.equals("") ||response==null || response.length()==0 )
                            {
                                appConstants.stopSpinWheel();
                                Intent i = new Intent(getApplicationContext(), LoginSubscriptionActivity.class);
                                startActivity(i);
                                finish();
                            }
                            else
                            {
                                if (response != null)
                                {
                                    try
                                    {
                                        appConstants.stopSpinWheel();

                                        adsModelList_ = new ArrayList<>();


                                         JSONArray jsonArrResp = new JSONArray(response.toString());
                                        for (int j = 0; j < jsonArrResp.length(); j++) {
                                            JSONObject mainObject = jsonArrResp.getJSONObject(j);

                                            String id = mainObject.getString("id");
                                            String image = mainObject.getString("image");
                                            String url ="";
                                            String video="";
                                            try
                                            {
                                                url = mainObject.getString("ads_url");
                                                video = mainObject.getString("video");
                                            }
                                            catch (Exception xx){}


                                                adsModel_ = new AdsModel(id, image, url,video);
                                                adsModelList_.add(adsModel_);


                                        }
//                                        adsModelList_.get(0).setVideo("0");
//                                        adsModelList_.get(0).setImage("https://i.dailymail.co.uk/i/newpix/2018/03/05/11/49DCBC4600000578-5463181-image-m-3_1520248081343.jpg");

                                        if(adsModelList_.get(0).getVideo().equalsIgnoreCase("0") ||
                                                adsModelList_.get(0).getVideo().equalsIgnoreCase(""))
                                        {
                                             youtubePlayerView.setVisibility(View.GONE);
                                             tutorIMG.setVisibility(View.VISIBLE);
                                             Picasso.with(getApplicationContext()).load(adsModelList_.get(counter).getImage()).into(tutorIMG);


                                        }
                                        else
                                        {
                                            tutorIMG.setVisibility(View.GONE);
                                            youtubePlayerView.setVisibility(View.VISIBLE);
                                            final String url=adsModelList_.get(counter).getUrl();


                                            youtubePlayerView.initialize(new YouTubePlayerInitListener() {
                                                @Override
                                                public void onInitSuccess(@NonNull final YouTubePlayer initializedYouTubePlayer) {
                                                    initializedYouTubePlayer.addListener(new AbstractYouTubePlayerListener() {
                                                        @Override
                                                        public void onReady() {
                                                            //String videoId = "TcMmGe9w_BU";
                                                            String videoId =appConstants.splitYoutubeVidio(url)  ;
                                                            initializedYouTubePlayer.loadVideo(videoId, 0);
                                                        }
                                                    });
                                                }
                                            }, true);


                                        }
                                    }
                                    catch (Exception xx){}

                                }

                            }


                        }
                    }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(final VolleyError error) {
                    String xx = error.toString();
                    appConstants.stopSpinWheel();

                    Intent i = new Intent(getApplicationContext(), LoginSubscriptionActivity.class);
                    startActivity(i);
                    finish();



                }


            })


            {



                @Override
                protected Map<String, String> getParams() {

                    Map<String, String> params = new HashMap<String, String>();
                    try {

                        Iterator<?> keys = jsonObject.keys();

                        while (keys.hasNext()) {
                            String key = (String) keys.next();
                            String value = jsonObject.getString(key);
                            params.put(key, value);

                        }


                    } catch (Exception xx) {
                        xx.toString();
                        appConstants.stopSpinWheel();
                    }
                    return params;
                }


                @Override
                protected Response<String> parseNetworkResponse(NetworkResponse response) {
                    try {

                        String jsonString = new String(response.data,
                                HttpHeaderParser.parseCharset(response.headers));

                        return Response.success(jsonString, HttpHeaderParser.parseCacheHeaders(response));


                    } catch (UnsupportedEncodingException e) {
                        return Response.error(new ParseError(e));
                    }
                }


            };

            stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                    appConstants.appTimeOut,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

            queue.add(stringRequest);


        } catch (Exception e) {
            appConstants.stopSpinWheel();
            e.toString();
        }



    }



//    private void addFullScreenListenerToPlayer(final YouTubePlayer youTubePlayer) {
//        youtubePlayerView.addFullScreenListener(new YouTubePlayerFullScreenListener() {
//            @Override
//            public void onYouTubePlayerEnterFullScreen() {
//                setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
//                fullScreenHelper.enterFullScreen();
//
//
//            }
//
//            @Override
//            public void onYouTubePlayerExitFullScreen() {
//                setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
//                fullScreenHelper.exitFullScreen();
//
//            }
//        });
//    }

    private void openWebURL( String url) {
        try {
            if (!url.startsWith("http://") && !url.startsWith("https://")) {
                url = "http://" + url;
                Intent browserIntent = new Intent(Intent.ACTION_VIEW,
                        Uri.parse(url));
               startActivity(browserIntent);
            }
            else
            {
                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
                 startActivity(browserIntent);
            }
        }
        catch (Exception xx){}


    }


}