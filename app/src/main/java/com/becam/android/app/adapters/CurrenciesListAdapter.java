package com.becam.android.app.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.becam.android.app.R;
import com.becam.android.app.models.CurrencyListModel;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

/**
 * Created by HP on 5/4/2017.
 */

public class CurrenciesListAdapter extends BaseAdapter {

    Context context;
    ArrayList<CurrencyListModel> resourcelList;


    public CurrenciesListAdapter(Context context, ArrayList<CurrencyListModel> arrayList_) {
        this.context = context;
        this.resourcelList = arrayList_;
    }

    @Override
    public int getCount() {
        return resourcelList.size();
    }

    @Override
    public Object getItem(int position) {
        return position;
    }


    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent)
    {
        View view = convertView;
        try {
            LayoutInflater inflater = (LayoutInflater) context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

            if (convertView == null)
            {
                view = inflater.inflate(R.layout.currencies_item_row, null);

            }


            TextView currencyNameTXT=view.findViewById(R.id.currencyNameTXT);
            TextView symbolTXT =view.findViewById(R.id.symbolTXT);
            ImageView flagImg = view.findViewById(R.id.flagImg);
            LinearLayout mainLO= view.findViewById(R.id.mainLO);

            symbolTXT.setText(resourcelList.get(position).getSymbol());
            currencyNameTXT.setText(resourcelList.get(position).getName());

            Glide.with(context).load(resourcelList.get(position).getImage()).apply(RequestOptions.circleCropTransform()).into(flagImg);




        }
        catch (Exception xx)
        {}
        return view;
    }

}
