package com.becam.android.app.fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.becam.android.app.R;
import com.becam.android.app.activity.MainScreenActivity;
import com.becam.android.app.adapters.PricesListAdapter;
import com.becam.android.app.models.PricesListModel;
import com.becam.android.app.others.appConstants;
import com.becam.android.app.others.urlClass;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by HP on 5/3/2018.
 */

public class ShopListMainFragment extends Fragment  {



    View view;
    private TabLayout tabLayout;
    private ViewPager viewPager;




    public static MainScreenActivity myContext;
    @Override
    public void onAttach(Context context) {
        myContext=(MainScreenActivity) getActivity();
        super.onAttach(context);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.shop_main_list_fragment, container, false);

        viewPager = view.findViewById(R.id.viewpager);
        setupViewPager(viewPager);
        tabLayout = view.findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(viewPager);
        MainScreenActivity.screenTitleTXT.setText(getResources().getString(R.string.shoplist_title));




        return view;
    }


    private void setupViewPager(ViewPager viewPager) {

//        tabLayout.getTabAt(0).setIcon(getResources().getDrawable(R.drawable.tv2));
//        tabLayout.getTabAt(1).setIcon(getResources().getDrawable(R.drawable.layers));

           ViewPagerAdapter adapter = new ViewPagerAdapter(getChildFragmentManager());

            adapter.addFragment(new LocalShopsListFragment(), getString(R.string.shoplist_local));
            adapter.addFragment(new InternationalShopsListFragment(), getString(R.string.shoplist_internationl));


        viewPager.setAdapter(adapter);

    }

    class ViewPagerAdapter extends FragmentPagerAdapter {

        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFragment(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    }

    public static void goToShopDetails(String id,String type,String country)
    {
        ShopDetailsFragment.shopID=id;
        ShopDetailsFragment.shopType=type;
        ShopDetailsFragment.country=country;


           FragmentTransaction tx = myContext.getSupportFragmentManager().beginTransaction();
            tx.replace(R.id.main_container, new ShopDetailsFragment());
            tx.addToBackStack(null);
            tx.commit();

    }

}
