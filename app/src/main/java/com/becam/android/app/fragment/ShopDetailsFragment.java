package com.becam.android.app.fragment;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.becam.android.app.R;
import com.becam.android.app.activity.MainScreenActivity;
import com.becam.android.app.adapters.LocalShopAdapter;
import com.becam.android.app.models.LocalShopModel;
import com.becam.android.app.others.appConstants;
import com.becam.android.app.others.urlClass;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by HP on 5/3/2018.
 */

public class ShopDetailsFragment extends Fragment  {
    public static MainScreenActivity myContext;


    View view;



    public static String shopID;
    public static String shopType,country;

    TextView nameTXT,mobileeTXT,mobile2TXT,adressNameTXT;
    ImageView shopImg;

    @Override
    public void onAttach(Context context) {
        myContext=(MainScreenActivity) getActivity();
        super.onAttach(context);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.shop_details_layout, container, false);

        nameTXT=view.findViewById(R.id.nameTXT);
        mobileeTXT=view.findViewById(R.id.mobileeTXT);
        mobile2TXT=view.findViewById(R.id.mobile2TXT);
        adressNameTXT=view.findViewById(R.id.adressNameTXT);
        shopImg=view.findViewById(R.id.shopImg);
        MainScreenActivity.screenTitleTXT.setText(getResources().getString(R.string.shoplist_tab));

        mobileeTXT.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(Intent.ACTION_DIAL);
                intent.setData(Uri.parse("tel:"+mobileeTXT.getText().toString().trim()));
                startActivity(intent);
            }
        });

        mobile2TXT.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(Intent.ACTION_DIAL);
                intent.setData(Uri.parse("tel:"+mobile2TXT.getText().toString().trim()));
                startActivity(intent);
            }
        });

        appConstants.startSpinwheel(getActivity(), false, true);

        new Thread(new Runnable() {
            @Override
            public void run() {
                try
                {
                    getShopDetailsRequest(urlClass.getShopDetailsURL+shopType+"&id="+shopID, null);

                } catch (Exception xx) {
                }
            }
        }).start();


        return view;
    }


    private void getShopDetailsRequest(String urlPost, final JSONObject jsonObject) {

        final RequestQueue queue;
        StringRequest stringRequest = null;

        try {

            queue = Volley.newRequestQueue(getActivity());

            stringRequest = new StringRequest(Request.Method.GET, urlPost,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response)
                        {
                            try
                            {
                                if(response!=null)
                                {
                                    appConstants.stopSpinWheel();


                                    JSONArray arr = new JSONArray(response.toString());

                                    for (int i = 0; i < arr.length(); i++) {

                                        try {
                                            JSONObject mainObj=arr.getJSONObject(i);

                                            String id = mainObj.getString("id");
                                            String name = mainObj.getString("name");
                                            String image = mainObj.getString("image");
                                            String mobile = mainObj.getString("mobile");
                                            String mobile2= mainObj.getString("mobile_second");
                                            String address = mainObj.getString("address");


                                            nameTXT.setText(name+ " - "+country);
                                            mobileeTXT.setText(mobile);
                                            mobile2TXT.setText(mobile2);
                                            adressNameTXT.setText(address);
                                            Picasso.with(getActivity()).load(image).into(shopImg);

                                        } catch (Exception xx) {
                                            xx.toString();
                                        }
                                    }



                                }


                            }
                            catch (Exception e)
                            {
                                e.printStackTrace();
                                appConstants.stopSpinWheel();
                            }


                        }
                    }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(final VolleyError error) {

                 //   Toast.makeText(getActivity(), "server error", Toast.LENGTH_SHORT).show();
                    appConstants.stopSpinWheel();


                }


            });

            stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                    appConstants.appTimeOut,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

            queue.add(stringRequest);


        } catch (Exception e) {
            appConstants.stopSpinWheel();
            e.toString();
        }



    }


}
