package com.becam.android.app.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.becam.android.app.R;
import com.becam.android.app.models.GoldDetailsChartModel;
import com.becam.android.app.models.GoldDetailsModel;
import com.becam.android.app.models.SharesModel;

import java.util.ArrayList;

/**
 * Created by Ayadi on 2/6/2018.
 */

public class GoldDetailsAdapter extends RecyclerView.Adapter<GoldDetailsAdapter.MenuItemViewHolder> {

    Context context;
    ArrayList<GoldDetailsModel> resourcelList;

    SharesModel sharesModel;
    SharesAdapter sharesAdapter;



    public GoldDetailsAdapter(Context context, ArrayList<GoldDetailsModel> ResourcelList) {
        this.context = context;
        resourcelList = ResourcelList;
    }

    @Override
    public MenuItemViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.gold_details_item_row, parent, false);
        MenuItemViewHolder viewholder = new MenuItemViewHolder(view);
        return viewholder;
    }

    @Override
    public void onBindViewHolder(MenuItemViewHolder holder, final int position) {

        holder.dateGoldTXT.setText(resourcelList.get(position).getDate());
        holder.saleGoldTXT.setText(resourcelList.get(position).getSale());



        //Picasso.with(context).load(resourcelList.get(position).getStatus()).into(holder.statusImg);

        if (resourcelList.get(position).getStatus().equalsIgnoreCase("fixed"))
        {
            // holder.shareStatusIMG.setImageResource(R.drawable.up_arrow);

            holder.statusGoldImg.setImageResource(R.drawable.fixed);

        }else  if (resourcelList.get(position).getStatus().equalsIgnoreCase("up"))
        {

            holder.statusGoldImg.setImageResource(R.drawable.up);


        }else  if (resourcelList.get(position).getStatus().equalsIgnoreCase("down"))
        {
            holder.statusGoldImg.setImageResource(R.drawable.down);

        }
        else
        {
            holder.statusGoldImg.setImageResource(R.drawable.fixed);

        }






    }

    @Override
    public int getItemCount() {
        return resourcelList.size();
    }

    public static class MenuItemViewHolder extends RecyclerView.ViewHolder
    {

        TextView dateGoldTXT ,saleGoldTXT ;
        ImageView statusGoldImg;



        public MenuItemViewHolder(View itemView)
        {
            super(itemView);
            dateGoldTXT = itemView.findViewById(R.id.dateGoldTXT);
            saleGoldTXT = itemView.findViewById(R.id.priceGoldTXT);
            statusGoldImg = itemView.findViewById(R.id.statusGoldImg);

        }


    }




}
