package com.becam.android.app.fragment;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.becam.android.app.R;
import com.becam.android.app.activity.MainScreenActivity;
import com.becam.android.app.activity.WebviewActivity;
import com.becam.android.app.adapters.CatAndSharesAdapter;
import com.becam.android.app.adapters.NewsAdapter;
import com.becam.android.app.models.CategoryModel;
import com.becam.android.app.models.NewsModel;
import com.becam.android.app.others.SharedPrefsUtils;
import com.becam.android.app.others.appConstants;
import com.becam.android.app.others.urlClass;

import com.daimajia.slider.library.SliderLayout;
import com.lsjwzh.widget.recyclerviewpager.RecyclerViewPager;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by HP on 5/3/2018.
 */

public class HomeFragment extends Fragment
{

    RecyclerViewPager catAndSharesLV;
    RecyclerView mainNewsLV;
    LinearLayout sharesMainLO;
    RelativeLayout topAdsLO;
    ImageView topAdsImg,closeAdsImg;
    TextView showHideBTN;
    HashMap<String, String> Hash_file_maps;
  //  public static WebView news_webview;
    View view;

    public  static int lastPos=0;
    public  static String newsLink="";
    SharedPrefsUtils sharedPref;



    ArrayList<CategoryModel> categoryModelArrayList;
    CategoryModel categoryModel;
    CatAndSharesAdapter catAdapter;


    ArrayList<NewsModel> newsArrayList;
    NewsModel newsModel;
    NewsAdapter newsAdapter;


    static FragmentTransaction tx;
    private static Fragment fragment;
    private SwipeRefreshLayout swipeToRefreshLayout;
    SliderLayout sliderLayout;

    TextView newsTXT,closeNewsBTN,timerTXT;
    LinearLayout newsLO;
    CountDownTimer newsTimer;
    int timerDuration=60000;
    Animation anim;
    String topAdsURL="";
    String adsUrl="";


    public static MainScreenActivity myContext;
    @Override
    public void onAttach(Context context) {
        myContext = (MainScreenActivity) getActivity();
        super.onAttach(context);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


        if(appConstants.breakNewsShown==false) {
            new Thread(new Runnable() {
                @Override
                public void run() {
                    try {
                        getBreakNewsRequest(urlClass.getBreakNewsURL, null);

                    } catch (Exception xx) {
                    }
                }
            }).start();
        }

        if(appConstants.topAdsShown==false)
        {
            new Thread(new Runnable() {
                @Override
                public void run() {
                    try
                    {
                        getInnerTopAdsReq(urlClass.getInnerTopAdsURL, null);

                    } catch (Exception xx) {
                    }
                }
            }).start();
        }



    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.home_fragment_layout, container, false);


       MainScreenActivity.screenTitleTXT.setText("الرئيسية");
        catAndSharesLV = (RecyclerViewPager) view.findViewById(R.id.catgRecycleView);

        sharesMainLO = view.findViewById(R.id.sharesMainLO);
        mainNewsLV= view.findViewById(R.id.mainNewsLV);

        topAdsLO= view.findViewById(R.id.topAdsLO);
        topAdsImg= view.findViewById(R.id.topAdsImg);
        closeAdsImg= view.findViewById(R.id.closeAdsImg);

        topAdsLO.setVisibility(View.GONE);
        topAdsImg.setVisibility(View.GONE);
        closeAdsImg.setVisibility(View.GONE);

        swipeToRefreshLayout = (SwipeRefreshLayout) view.findViewById(R.id.swipeTorefresh);
        showHideBTN = view.findViewById(R.id.showPriceDrawerBTN);


        anim= AnimationUtils.loadAnimation(getActivity(), R.anim.splashanimation);

        timerTXT= view.findViewById(R.id.timerTXT);
        newsLO= view.findViewById(R.id.newsLO);
        newsTXT= view.findViewById(R.id.newsTXT);
        closeNewsBTN= view.findViewById(R.id.closeNewsBTN);
        newsLO.setVisibility(View.GONE);

        closeNewsBTN.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                newsLO.setVisibility(View.GONE);
                appConstants.breakNewsShown=true;
            }
        });




        closeAdsImg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                topAdsLO.setVisibility(View.GONE);
                closeAdsImg.setVisibility(View.GONE);
                appConstants.topAdsShown=true;
            }
        });

        topAdsLO.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(!topAdsURL.equalsIgnoreCase(""))
                {
                    MainScreenActivity.openWebURL(getActivity(), topAdsURL);
                }
            }
        });

        topAdsImg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(!adsUrl.equalsIgnoreCase("")) {
                    WebviewActivity.url = adsUrl;
                    startActivity(new Intent(getActivity(), WebviewActivity.class));
                }
            }
        });


        swipeToRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                swipeToRefreshLayout.setRefreshing(true);

                appConstants.startSpinwheel(getActivity(), false, true);

                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        try
                        {
                            getCatAndSharesRequest(urlClass.getCategoryWithShareURL, null);

                        } catch (Exception xx) {
                        }
                    }
                }).start();




                appConstants.startSpinwheel(getActivity(), false, true);

                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        try
                        {
                            getNewsRequest(newsLink, null);

                        } catch (Exception xx) {
                        }
                    }
                }).start();

                swipeToRefreshLayout.setRefreshing(false);

            }
        });

        showHideBTN.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (catAndSharesLV.getVisibility() == View.VISIBLE) {
                    catAndSharesLV.setVisibility(View.GONE);

                } else {
                    catAndSharesLV.setVisibility(View.VISIBLE);
                }

            }
        });


        appConstants.startSpinwheel(getActivity(), false, true);

        new Thread(new Runnable() {
            @Override
            public void run() {
                try
                {
                    getCatAndSharesRequest(urlClass.getCategoryWithShareURL, null);

                } catch (Exception xx) {
                }
            }
        }).start();




        appConstants.startSpinwheel(getActivity(), false, true);

        new Thread(new Runnable() {
            @Override
            public void run() {
                try
                {
                    getNewsRequest(newsLink, null);

                } catch (Exception xx) {
                }
            }
        }).start();





        return view;
    }





    private void getCatAndSharesRequest(String urlPost, final JSONObject jsonObject) {

        final RequestQueue queue;
        StringRequest stringRequest = null;

        try {

            queue = Volley.newRequestQueue(getActivity());

            stringRequest = new StringRequest(Request.Method.GET, urlPost,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response)
                        {
                            try
                            {
                                if(response!=null)
                                {
                                    appConstants.stopSpinWheel();

                                    categoryModelArrayList = new ArrayList<>();

                                    JSONArray arr = new JSONArray(response.toString());

                                    for (int i = 0; i < arr.length(); i++) {

                                        try {
                                            JSONObject mainObj=arr.getJSONObject(i);

                                            String id = mainObj.getString("id");
                                            String name = mainObj.getString("cat_name");
                                            String desc = "";
                                            JSONArray sharesArr =mainObj.getJSONArray("items");

                                            String flag="";
                                            if(id.equalsIgnoreCase("1"))
                                            {
                                                flag="currency" ;
                                            }
                                            else   if(id.equalsIgnoreCase("2"))
                                            {
                                              flag="gold";
                                            }
                                            else   if(id.equalsIgnoreCase("3"))
                                            {
                                               flag="oil";
                                            }
                                            else   if(id.equalsIgnoreCase("4"))
                                            {
                                                flag="bitcoin";
                                            }
                                            else  if(id.equalsIgnoreCase("5"))
                                            {
                                                flag="material";
                                            }
                                            else  if(id.equalsIgnoreCase("6"))
                                            {
                                                flag="food";
                                            }
                                            else  if(id.equalsIgnoreCase("7"))
                                            {
                                                flag="meat";
                                            }
                                            else  if(id.equalsIgnoreCase("8"))
                                            {
                                                flag="veg";
                                            }
                                            else  if(id.equalsIgnoreCase("9"))
                                            {
                                                flag="fruit";
                                            }
                                            else  if(id.equalsIgnoreCase("10"))
                                            {
                                                flag="wmeat";
                                            }

                                            //sharesArr.put(sharesArr.length(),flag);

                                            categoryModel = new CategoryModel(id, name, desc, sharesArr,flag);
                                            categoryModelArrayList.add(categoryModel);

                                        } catch (Exception xx) {
                                            xx.toString();
                                        }
                                    }


                                    catAdapter = new CatAndSharesAdapter(getActivity(), categoryModelArrayList);
                                    catAndSharesLV.setAdapter(catAdapter);
                                    LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false);
                                    catAndSharesLV.setLayoutManager(layoutManager);

                                }


                            }
                            catch (Exception e)
                            {
                                e.printStackTrace();
                                appConstants.stopSpinWheel();
                            }


                        }
                    }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(final VolleyError error) {

                  //  Toast.makeText(getActivity(), "server error", Toast.LENGTH_SHORT).show();
                    appConstants.stopSpinWheel();


                }


            });

            stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                    appConstants.appTimeOut,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

            queue.add(stringRequest);


        } catch (Exception e) {
            appConstants.stopSpinWheel();
            e.toString();
        }



    }

    private void getNewsRequest(String urlPost, final JSONObject jsonObject) {

        final RequestQueue queue;
        StringRequest stringRequest = null;

        try {

            queue = Volley.newRequestQueue(getActivity());

            stringRequest = new StringRequest(Request.Method.GET, urlPost,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response)
                        {
                            try
                            {
                                if(response!=null)
                                {
                                    response=new String(response.getBytes("ISO-8859-1"), "UTF-8");
                                    appConstants.stopSpinWheel();

                                    newsArrayList = new ArrayList<>();

                                    JSONObject mainObj = new JSONObject(response);
                                    JSONArray arr=mainObj.getJSONArray("data");

                                    for (int i = 0; i < arr.length(); i++) {

                                        try {
                                            JSONObject dataObj=arr.getJSONObject(i);

                                            String id = dataObj.getString("post_id");
                                            String title = dataObj.getString("post_title");
                                            String image = dataObj.getString("post_image");
                                            String shorDesc = dataObj.getString("post_content");
                                            String date = dataObj.getString("post_date");



                                            newsModel = new NewsModel(id, title,shorDesc,image,date);
                                            newsArrayList.add(newsModel);

                                        } catch (Exception xx) {
                                            xx.toString();
                                        }
                                    }


                                    newsAdapter = new NewsAdapter(getActivity(), newsArrayList);
                                    mainNewsLV.setAdapter(newsAdapter);
                                    LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
                                    mainNewsLV.setLayoutManager(layoutManager);
                                    mainNewsLV.scrollToPosition(lastPos);

                                }


                            }
                            catch (Exception e)
                            {
                                e.printStackTrace();
                                appConstants.stopSpinWheel();
                            }


                        }
                    }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(final VolleyError error) {

                    Toast.makeText(getActivity(), "server error", Toast.LENGTH_SHORT).show();
                    appConstants.stopSpinWheel();


                }


            });

            stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                    appConstants.appTimeOut,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

            queue.add(stringRequest);


        } catch (Exception e) {
            appConstants.stopSpinWheel();
            e.toString();
        }



    }

    private void getBreakNewsRequest(String urlPost, final JSONObject jsonObject) {

        final RequestQueue queue;
        StringRequest stringRequest = null;

        try
        {
            queue = Volley.newRequestQueue(getActivity());

            stringRequest = new StringRequest(Request.Method.GET, urlPost,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response)
                        {
                            try
                            {
                                if(response!=null)
                                {
                                    appConstants.stopSpinWheel();

                                    JSONArray arr = new JSONArray(response.toString());

                                    try {
                                        JSONObject mainObj=arr.getJSONObject(0);

                                        String id = mainObj.getString("id");
                                        String news = mainObj.getString("newstext");
                                        String timeSecond = mainObj.getString("time_seconds");

                                        newsLO.startAnimation(anim);
                                        newsTXT.setText("عاجل | "+news);
                                        newsLO.setVisibility(View.VISIBLE);
                                        newsTXT.setVisibility(View.VISIBLE);

                                        try
                                        {
                                            timerDuration = Integer.parseInt(timeSecond)*1000;
                                        }
                                        catch (Exception xx){}


                                        newsTimer=new CountDownTimer(timerDuration,1) {
                                            @Override
                                            public void onTick(long millisUntilFinished)
                                            {
                                                timerTXT.setText(String.valueOf(millisUntilFinished/1000));

                                            }

                                            @Override
                                            public void onFinish() {

                                                //newsLO.startAnimation(anim);
                                                appConstants.breakNewsShown=true;
                                                newsTXT.setText("");
                                                newsLO.setVisibility(View.GONE);

                                            }
                                        }.start();


                                    } catch (Exception xx) {
                                        xx.toString();
                                    }


                                }


                            }
                            catch (Exception e)
                            {
                                e.printStackTrace();
                                appConstants.stopSpinWheel();
                            }


                        }
                    }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(final VolleyError error) {

                   // Toast.makeText(getActivity(), "server error", Toast.LENGTH_SHORT).show();
                    appConstants.stopSpinWheel();


                }


            });

            stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                    appConstants.appTimeOut,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

            queue.add(stringRequest);


        }
        catch (Exception e) {
            appConstants.stopSpinWheel();
            e.toString();
        }



    }

    private void getInnerTopAdsReq(String urlPost, final JSONObject jsonObject) {

        final RequestQueue queue;
        StringRequest stringRequest = null;

        try {

            queue = Volley.newRequestQueue(getActivity());

            stringRequest = new StringRequest(Request.Method.GET, urlPost,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response)
                        {
                            try
                            {
                                if(response!=null)
                                {
                                    appConstants.stopSpinWheel();
                                    JSONArray arr = new JSONArray(response.toString());

                                    JSONObject mainObject=arr.getJSONObject(0);

                                    String id = mainObject.getString("id");
                                    String image = mainObject.getString("image");
                                   try{ adsUrl = mainObject.getString("ads_url");}
                                   catch (Exception xx){}


                                    topAdsLO.setVisibility(View.VISIBLE);
                                    topAdsImg.setVisibility(View.VISIBLE);
                                    closeAdsImg.setVisibility(View.VISIBLE);


                                    Picasso.with(getActivity()).load(image).into(topAdsImg);
                                    appConstants.topAdsShown=true;



                                }


                            }
                            catch (Exception e)
                            {
                                e.printStackTrace();
                                appConstants.stopSpinWheel();
                            }


                        }
                    }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(final VolleyError error) {

                  //  Toast.makeText(getActivity(), "server error", Toast.LENGTH_SHORT).show();
                    appConstants.stopSpinWheel();


                }


            })


            {

            };

            stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                    appConstants.appTimeOut,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

            queue.add(stringRequest);


        } catch (Exception e) {
            appConstants.stopSpinWheel();
            e.toString();
        }



    }



    public static void goToCategoryDetails(String Category,String lastDate,String title)
    {

        MainScreenActivity myContext=MainScreenActivity.myContext;

        appConstants.categoryTypeFlag=Category;

        if(!appConstants.categoryTypeFlag.equalsIgnoreCase("bitcoin"))
        {
            //CategoryDetailsFragment.catName=title;

            tx = myContext.getSupportFragmentManager().beginTransaction();
            tx.replace(R.id.main_container, new CategoryDetailsFragment());
            tx.addToBackStack(null);
            tx.commit();
        }
        else
        {
            appConstants.categoryTypeFlag="bitcoin";
            BitcoinDetailsFragment.itemID="";
            BitcoinDetailsFragment.itemName="Bitcoin";
            BitcoinDetailsFragment.lastUpdate=lastDate;

            tx = myContext.getSupportFragmentManager().beginTransaction();
            tx.replace(R.id.main_container, new BitcoinDetailsFragment());
            tx.addToBackStack(null);
            tx.commit();
        }
    }

    public static void goToShareDetails(String id,String name,String category,String lastUpdateDate,String country,String measurment)
    {
        MainScreenActivity myContext=MainScreenActivity.myContext;

        if(category.equalsIgnoreCase("currency"))
        {
            appConstants.categoryTypeFlag="currency";
            CurrencyDetailsFragment.itemID=id;
            CurrencyDetailsFragment.itemName=name;
            CurrencyDetailsFragment.lastUpdateDate=lastUpdateDate;

            tx = myContext.getSupportFragmentManager().beginTransaction();
            tx.replace(R.id.main_container, new CurrencyDetailsFragment());
            tx.addToBackStack(null);
            tx.commit();
        }
        else  if(category.equalsIgnoreCase("oil"))
        {
            appConstants.categoryTypeFlag="oil";
            OilDetailsFragment.itemName=name;
            OilDetailsFragment.lastUpdateDate=lastUpdateDate;

            tx = myContext.getSupportFragmentManager().beginTransaction();
            tx.replace(R.id.main_container, new OilDetailsFragment());
            tx.addToBackStack(null);
            tx.commit();
        }
        else  if(category.equalsIgnoreCase("gold"))
        {
            appConstants.categoryTypeFlag="gold";
            GoldDetailsFragment.itemID=id;
            GoldDetailsFragment.itemName=name;
            GoldDetailsFragment.lastUpdateDate=lastUpdateDate;

            tx = myContext.getSupportFragmentManager().beginTransaction();
            tx.replace(R.id.main_container, new GoldDetailsFragment());
            tx.addToBackStack(null);
            tx.commit();
        }
        else  if(category.equalsIgnoreCase("bitcoin"))
        {
            appConstants.categoryTypeFlag="bitcoin";
            BitcoinDetailsFragment.itemID=id;
            BitcoinDetailsFragment.itemName=name;
            BitcoinDetailsFragment.lastUpdate=lastUpdateDate;

            tx = myContext.getSupportFragmentManager().beginTransaction();
            tx.replace(R.id.main_container, new BitcoinDetailsFragment());
            tx.addToBackStack(null);
            tx.commit();

        }
        else  if(category.equalsIgnoreCase("material"))
        {
            appConstants.categoryTypeFlag="material";
            OtherPricesDetailsFragment.itemID=id;
            OtherPricesDetailsFragment.itemName=name;
            OtherPricesDetailsFragment.lastUpdateDate=lastUpdateDate;

            tx = myContext.getSupportFragmentManager().beginTransaction();
            tx.replace(R.id.main_container, new OtherPricesDetailsFragment());
            tx.addToBackStack(null);
            tx.commit();
        }

        else  if(category.equalsIgnoreCase("food"))
        {
//            appConstants.categoryTypeFlag="food";
//            FoodDetailsFragment.itemID=id;
//            FoodDetailsFragment.itemName=name;
//            FoodDetailsFragment.lastUpdateDate=lastUpdateDate;
//
//            tx = myContext.getSupportFragmentManager().beginTransaction();
//            tx.replace(R.id.main_container, new FoodDetailsFragment());
//            tx.addToBackStack(null);
//            tx.commit();

            appConstants.categoryTypeFlag="food";
            OtherPricesDetailsFragment.itemID=id;
            OtherPricesDetailsFragment.itemName=name;
            OtherPricesDetailsFragment.lastUpdateDate=lastUpdateDate;
            OtherPricesDetailsFragment.country=country;
            OtherPricesDetailsFragment.measurment=measurment;

            tx = myContext.getSupportFragmentManager().beginTransaction();
            tx.replace(R.id.main_container, new OtherPricesDetailsFragment());
            tx.addToBackStack(null);
            tx.commit();
        }
        else  if(category.equalsIgnoreCase("meat"))
        {
            appConstants.categoryTypeFlag="meat";
            OtherPricesDetailsFragment.itemID=id;
            OtherPricesDetailsFragment.itemName=name;
            OtherPricesDetailsFragment.lastUpdateDate=lastUpdateDate;

            tx = myContext.getSupportFragmentManager().beginTransaction();
            tx.replace(R.id.main_container, new OtherPricesDetailsFragment());
            tx.addToBackStack(null);
            tx.commit();
        }
        else  if(category.equalsIgnoreCase("fruit"))
        {
            appConstants.categoryTypeFlag="fruit";
            OtherPricesDetailsFragment.itemID=id;
            OtherPricesDetailsFragment.itemName=name;
            OtherPricesDetailsFragment.lastUpdateDate=lastUpdateDate;

            tx = myContext.getSupportFragmentManager().beginTransaction();
            tx.replace(R.id.main_container, new OtherPricesDetailsFragment());
            tx.addToBackStack(null);
            tx.commit();
        }
        else  if(category.equalsIgnoreCase("veg"))
        {
            appConstants.categoryTypeFlag="veg";
            OtherPricesDetailsFragment.itemID=id;
            OtherPricesDetailsFragment.itemName=name;
            OtherPricesDetailsFragment.lastUpdateDate=lastUpdateDate;

            tx = myContext.getSupportFragmentManager().beginTransaction();
            tx.replace(R.id.main_container, new OtherPricesDetailsFragment());
            tx.addToBackStack(null);
            tx.commit();
        }
        else  if(category.equalsIgnoreCase("wmeat"))
        {
            appConstants.categoryTypeFlag="wmeat";
            OtherPricesDetailsFragment.itemID=id;
            OtherPricesDetailsFragment.itemName=name;
            OtherPricesDetailsFragment.lastUpdateDate=lastUpdateDate;

            tx = myContext.getSupportFragmentManager().beginTransaction();
            tx.replace(R.id.main_container, new OtherPricesDetailsFragment());
            tx.addToBackStack(null);
            tx.commit();
        }
    }

    public static void goToNewsDetails(String id)
    {


        MainScreenActivity myContext=MainScreenActivity.myContext;
        NewsDetailsFragment.postID=id;

            tx = myContext.getSupportFragmentManager().beginTransaction();
            tx.replace(R.id.main_container, new NewsDetailsFragment());
            tx.addToBackStack(null);
            tx.commit();


    }
}
