package com.becam.android.app.fragment;

import android.content.Context;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.becam.android.app.R;
import com.becam.android.app.activity.MainScreenActivity;
import com.becam.android.app.adapters.CatAndSharesAdapter;
import com.becam.android.app.adapters.CatDetailsAdapter;
import com.becam.android.app.models.CategoryDetailsModel;
import com.becam.android.app.models.CategoryModel;
import com.becam.android.app.others.appConstants;
import com.becam.android.app.others.urlClass;
import com.daimajia.slider.library.SliderLayout;
import com.daimajia.slider.library.SliderTypes.BaseSliderView;
import com.daimajia.slider.library.SliderTypes.TextSliderView;
import com.lsjwzh.widget.recyclerviewpager.RecyclerViewPager;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

/**
 * Created by HP on 5/3/2018.
 */

public class CategoryDetailsFragment extends Fragment  {
    public static MainScreenActivity myContext;
    RecyclerView catLV;
    TextView lastUpdatTXT;

    View view;

    ArrayList<CategoryDetailsModel> arrayList;
    CategoryDetailsModel model;
    CatDetailsAdapter adapter;



    static FragmentTransaction tx;
    private static Fragment fragment;
    private SwipeRefreshLayout swipeToRefreshLayout;
    public static String catName="التفاصيل";


    TextView priceTXT,sellTXT,buyTXT,symbolTXT ;
    LinearLayout headerLO;

    @Override
    public void onAttach(Context context) {
        myContext=(MainScreenActivity) getActivity();
        super.onAttach(context);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.category_details_fragment, container, false);
        catLV=  view.findViewById(R.id.catDetailsLV);
        lastUpdatTXT=  view.findViewById(R.id.lastUpdatTXT);
        lastUpdatTXT.setText("loading...");
        headerLO=  view.findViewById(R.id.headerLO);
        headerLO.setVisibility(View.INVISIBLE);

        sellTXT = view.findViewById(R.id.sellTXT);
        buyTXT= view.findViewById(R.id.buyTXT);
        priceTXT= view.findViewById(R.id.priceTXT);
        symbolTXT= view.findViewById(R.id.symbolTXT);

        MainScreenActivity.screenTitleTXT.setText(catName);
        swipeToRefreshLayout = (SwipeRefreshLayout) view.findViewById(R.id.swipeTorefresh);

        swipeToRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                swipeToRefreshLayout.setRefreshing(true);

                appConstants.startSpinwheel(getActivity(), false, true);

                appConstants.startSpinwheel(getActivity(), false, true);
                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        try {

                            String urlData="";
                            if(appConstants.categoryTypeFlag.equalsIgnoreCase("oil"))
                            {
                                urlData= urlClass.getOilListURL;
                            }
                            else   if(appConstants.categoryTypeFlag.equalsIgnoreCase("gold"))
                            {
                                urlData= urlClass.getGoldListURL;
                            }
                            else   if(appConstants.categoryTypeFlag.equalsIgnoreCase("currency"))
                            {
                                urlData= urlClass.getCurencyListURL;
                            }
                            else   if(appConstants.categoryTypeFlag.equalsIgnoreCase("bitcoin"))
                            {
                                // urlData= urlClass.getCurencyListURL;
                            }
                            else   if(appConstants.categoryTypeFlag.equalsIgnoreCase("material"))
                            {
                                urlData= urlClass.getMaterialistURL;
                            }
                            else   if(appConstants.categoryTypeFlag.equalsIgnoreCase("food"))
                            {
                                urlData= urlClass.getFoodlistURL;
                            }
                            else   if(appConstants.categoryTypeFlag.equalsIgnoreCase("meat"))
                            {
                                urlData= urlClass.getMeatlistURL;
                            }
                            else   if(appConstants.categoryTypeFlag.equalsIgnoreCase("fruit"))
                            {
                                urlData= urlClass.getFruitlistURL;
                            }
                            else   if(appConstants.categoryTypeFlag.equalsIgnoreCase("veg"))
                            {
                                urlData= urlClass.getVeglistURL;
                            }
                            else   if(appConstants.categoryTypeFlag.equalsIgnoreCase("wmeat"))
                            {
                                urlData= urlClass.getWhiteMeatlistURL;
                            }
                            getCategoryDetailsReq(urlData, null);

                        } catch (Exception xx) {
                        }
                    }
                }).start();

                swipeToRefreshLayout.setRefreshing(false);

            }
        });



        appConstants.startSpinwheel(getActivity(), false, true);
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {

                    String urlData="";
                    if(appConstants.categoryTypeFlag.equalsIgnoreCase("oil"))
                    {
                        urlData= urlClass.getOilListURL;
                    }
                    else   if(appConstants.categoryTypeFlag.equalsIgnoreCase("gold"))
                    {
                        urlData= urlClass.getGoldListURL;
                    }
                    else   if(appConstants.categoryTypeFlag.equalsIgnoreCase("currency"))
                    {
                        urlData= urlClass.getCurencyListURL;
                    }
                    else   if(appConstants.categoryTypeFlag.equalsIgnoreCase("bitcoin"))
                    {
                        // urlData= urlClass.getCurencyListURL;
                    }
                    else   if(appConstants.categoryTypeFlag.equalsIgnoreCase("material"))
                    {
                        urlData= urlClass.getMaterialistURL;
                    }
                    else   if(appConstants.categoryTypeFlag.equalsIgnoreCase("food"))
                    {
                        urlData= urlClass.getFoodlistURL;
                    }
                    else   if(appConstants.categoryTypeFlag.equalsIgnoreCase("meat"))
                    {
                        urlData= urlClass.getMeatlistURL;
                    }
                    else   if(appConstants.categoryTypeFlag.equalsIgnoreCase("fruit"))
                    {
                        urlData= urlClass.getFruitlistURL;
                    }
                    else   if(appConstants.categoryTypeFlag.equalsIgnoreCase("veg"))
                    {
                        urlData= urlClass.getVeglistURL;
                    }
                    else   if(appConstants.categoryTypeFlag.equalsIgnoreCase("wmeat"))
                    {
                        urlData= urlClass.getWhiteMeatlistURL;
                    }
                    getCategoryDetailsReq(urlData, null);

                } catch (Exception xx) {
                }
            }
        }).start();



        return view;
    }


    private void getCategoryDetailsReq(final String urlPost, final JSONObject jsonObject) {

        final RequestQueue queue;
        StringRequest stringRequest = null;

        try {

            queue = Volley.newRequestQueue(getActivity());

            stringRequest = new StringRequest(Request.Method.GET, urlPost,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response)
                        {
                            try
                            {
                                if(response!=null)
                                {
                                    appConstants.stopSpinWheel();
                                    arrayList=new ArrayList<>();

                                    JSONObject mainObj = new JSONObject(response.toString());
                                    JSONArray mainArr =mainObj.getJSONArray("items");
                                    lastUpdatTXT.setText(mainObj.getString("last_update_date"));

                                    for(int i=0;i<mainArr.length();i++)
                                    {
                                        JSONObject obj=mainArr.getJSONObject(i);

                                         String id="";
                                         String item_id="";
                                         String name="";
                                         String symbol="";
                                         String price="";
                                         String sell="";
                                         String buy="";
                                         String status="";
                                         String date="";
                                         String flagImg="";
                                        String measurment="";
                                        String country="";
                                        String weight="";

                                        try{   id=obj.getString("id");}  catch (Exception xx){}
                                        try{   item_id=obj.getString("item_id");}  catch (Exception xx){}
                                        try{   name=obj.getString("name");}  catch (Exception xx){}
                                        try{   symbol=obj.getString("symbol");}  catch (Exception xx){}
                                        try{   price=obj.getString("price");}  catch (Exception xx){}
                                        try{   sell=obj.getString("sale");}  catch (Exception xx){}
                                        try{   buy=obj.getString("buy");}  catch (Exception xx){}
                                        try{   status=obj.getString("status");}  catch (Exception xx){}
                                        try{   date=obj.getString("date");}  catch (Exception xx){}
                                        try{   flagImg=obj.getString("flag");}  catch (Exception xx){}
                                        try{   measurment=obj.getString("measurement");}  catch (Exception xx){}
                                        try{   country=obj.getString("country");}  catch (Exception xx){}
                                        try{   weight=obj.getString("weight");}  catch (Exception xx){}


                                        model = new CategoryDetailsModel(id,item_id, name, symbol, price,sell,buy,status,date,appConstants.categoryTypeFlag,flagImg,measurment,country,weight);
                                        arrayList.add(model);
                                    }

                                    if(arrayList.size()>0)
                                    {
                                        if (arrayList.get(0).getPrice().equalsIgnoreCase(""))
                                        {
                                            priceTXT.setVisibility(View.GONE);
                                        }
                                        if (arrayList.get(0).getSell().equalsIgnoreCase(""))
                                        {
                                            sellTXT.setVisibility(View.GONE);
                                        }
                                        if (arrayList.get(0).getBuy().equalsIgnoreCase(""))
                                        {
                                            buyTXT.setVisibility(View.INVISIBLE);
                                        }
                                        if (arrayList.get(0).getSymbol().equalsIgnoreCase(""))
                                        {
                                            symbolTXT.setVisibility(View.INVISIBLE);
                                        }
                                        headerLO.setVisibility(View.VISIBLE);

                                        adapter = new CatDetailsAdapter(getActivity(), arrayList);
                                        catLV.setAdapter(adapter);
                                        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
                                        catLV.setLayoutManager(layoutManager);
                                    }

                                }


                            }
                            catch (Exception e)
                            {
                                Toast.makeText(getActivity(), "لا يوجد نتائج", Toast.LENGTH_SHORT).show();
                                appConstants.stopSpinWheel();
                            }


                        }
                    }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(final VolleyError error) {

                    Toast.makeText(getActivity(), "لا يوجد نتائج", Toast.LENGTH_SHORT).show();
                    appConstants.stopSpinWheel();


                }


            });

            stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                    appConstants.appTimeOut,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

            queue.add(stringRequest);


        } catch (Exception e) {
            appConstants.stopSpinWheel();
            e.toString();
        }



    }






}
