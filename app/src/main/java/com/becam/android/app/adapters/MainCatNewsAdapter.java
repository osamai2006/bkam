package com.becam.android.app.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.becam.android.app.R;
import com.becam.android.app.models.MainCategoryNewsModel;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;

import java.util.ArrayList;

/**
 * Created by HP on 5/4/2017.
 */

public class MainCatNewsAdapter extends BaseAdapter {

    private Context context;
    private ArrayList<MainCategoryNewsModel> arrayList;


    public MainCatNewsAdapter(Context context, ArrayList<MainCategoryNewsModel> arrayList_) {
        this.context = context;
        this.arrayList = arrayList_;
    }

    @Override
    public int getCount() {
        return arrayList.size();
    }

    @Override
    public Object getItem(int position) {
        return position;
    }


    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent)
    {
        View view = convertView;
        try {
            LayoutInflater inflater = (LayoutInflater) context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

            if (convertView == null)
            {
                view = inflater.inflate(R.layout.main_category_grid_item_layout, null);

            }
            ImageView logoIMG = (ImageView) view.findViewById(R.id.logoIMG);
            TextView nameTXT = (TextView) view.findViewById(R.id.nameTXT);

            nameTXT.setText(arrayList.get(position).getName());
            Picasso.with(context).load(arrayList.get(position).getImage()).into(logoIMG);


        }
        catch (Exception xx)
        {}
        return view;
    }

}
