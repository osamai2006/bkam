package com.becam.android.app.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.becam.android.app.R;
import com.becam.android.app.models.FoodDetailsModel;
import com.becam.android.app.models.SharesModel;

import java.util.ArrayList;

/**
 * Created by Ayadi on 2/6/2018.
 */

public class FoodDetailsAdapter extends RecyclerView.Adapter<FoodDetailsAdapter.MenuItemViewHolder> {

    Context context;
    ArrayList<FoodDetailsModel> resourcelList;

    SharesModel sharesModel;
    SharesAdapter sharesAdapter;



    public FoodDetailsAdapter(Context context, ArrayList<FoodDetailsModel> ResourcelList) {
        this.context = context;
        resourcelList = ResourcelList;
    }

    @Override
    public MenuItemViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.food_details_item_row, parent, false);
        MenuItemViewHolder viewholder = new MenuItemViewHolder(view);
        return viewholder;
    }

    @Override
    public void onBindViewHolder(MenuItemViewHolder holder, final int position) {

        holder.dateFoodTXT.setText(resourcelList.get(position).getDate());
        holder.priceFoodTXT.setText(resourcelList.get(position).getPrice());



        //Picasso.with(context).load(resourcelList.get(position).getStatus()).into(holder.statusImg);

        if (resourcelList.get(position).getStatus().equalsIgnoreCase("fixed"))
        {
            // holder.shareStatusIMG.setImageResource(R.drawable.up_arrow);

            holder.statusFoodImg.setImageResource(R.drawable.fixed);

        }else  if (resourcelList.get(position).getStatus().equalsIgnoreCase("up"))
        {

            holder.statusFoodImg.setImageResource(R.drawable.up);


        }else  if (resourcelList.get(position).getStatus().equalsIgnoreCase("down"))
        {
            holder.statusFoodImg.setImageResource(R.drawable.down);

        }
        else
        {
            holder.statusFoodImg.setImageResource(R.drawable.fixed);

        }






    }

    @Override
    public int getItemCount() {
        return resourcelList.size();
    }

    public static class MenuItemViewHolder extends RecyclerView.ViewHolder
    {

        TextView dateFoodTXT ,priceFoodTXT ;
        ImageView statusFoodImg;



        public MenuItemViewHolder(View itemView)
        {
            super(itemView);
            dateFoodTXT = itemView.findViewById(R.id.dateFoodTXT);
            priceFoodTXT = itemView.findViewById(R.id.priceFoodTXT);
            statusFoodImg = itemView.findViewById(R.id.statusFoodImg);

        }


    }




}
