package com.becam.android.app.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.becam.android.app.R;
import com.becam.android.app.activity.ForgetPasswordActivity;
import com.becam.android.app.others.SharedPrefsUtils;
import com.becam.android.app.others.appConstants;
import com.becam.android.app.others.urlClass;

import org.json.JSONObject;

/**
 * Created by HP on 5/3/2017.
 */

public class loginDetailsFragment extends Fragment {
    View view;
    TextView forgetPasswordTxt;
    EditText emailTXT,passTXT,zipCodeTXT;
    Button loginBTN;
    Animation animShke;
    SharedPrefsUtils sharedPref;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.login_details_fragment_layout, container, false);
        emailTXT= (EditText) view.findViewById(R.id.login_email_txt);
        passTXT= (EditText) view.findViewById(R.id.password_etxt);
        zipCodeTXT= (EditText) view.findViewById(R.id.zipCodeTXT);
        zipCodeTXT.setFocusable(false);
        animShke= AnimationUtils.loadAnimation(getActivity(),R.anim.shake);
        loginBTN= (Button) view.findViewById(R.id.lognBTN);
        loginBTN.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                try
                {
                    if(checkRule())
                    {
                        appConstants.startSpinwheel(getActivity(), false, true);

                        new Thread(new Runnable() {
                            @Override
                            public void run()
                            {

                                try
                                {
                                    String url="";// urlClass.loginURL;
                                    JSONObject Objct = new JSONObject();
                                    Objct.putOpt("EMAIL", emailTXT.getText().toString().trim());
                                    Objct.putOpt("PASSWORD", passTXT.getText().toString().trim());

                                    loginRequest(url,Objct);
                                }
                                catch (Exception xx)
                                {
                                    xx.toString();
                                    appConstants.stopSpinWheel();
                                }
                            }
                        }).start();
                    }
                }
                catch (Exception xx)
                {
                    xx.toString();
                }
            }
        });

        forgetPasswordTxt= (TextView) view.findViewById(R.id.forget_password_txt);

        forgetPasswordTxt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                Intent intent = new Intent(getActivity(), ForgetPasswordActivity.class);
                startActivity(intent);
            }
        });


        return view;
    }

    private  boolean checkRule() {

        if (emailTXT.getText().toString().trim().equals("")) {
            Toast.makeText(getActivity(), "Please check the email", Toast.LENGTH_SHORT).show();
            emailTXT.startAnimation(animShke);
            return false;
        }
        if (!emailTXT.getText().toString().trim().matches(appConstants.email_pattern)) {
            Toast.makeText(getActivity(), "Please enter a valid email", Toast.LENGTH_SHORT).show();
            emailTXT.startAnimation(animShke);
            return false;
        }
        if (passTXT.getText().toString().trim().equals("")) {
            Toast.makeText(getActivity(), "Please check the password", Toast.LENGTH_SHORT).show();
            passTXT.startAnimation(animShke);
            return false;
        }
        return  true;
    }


    private void loginRequest(String urlPost, final JSONObject jsonObjectRequest) {

        JsonObjectRequest jsonObjReq;

         jsonObjReq = new JsonObjectRequest(Request.Method.POST, urlPost, jsonObjectRequest, new Response.Listener<JSONObject>()
                 {

            @Override
            public void onResponse(JSONObject response) {

                try
                {
                    if(response !=null)
                    {
                        appConstants.stopSpinWheel();
                        JSONObject jsonObjResp = new JSONObject(response.toString());
                        final String status=jsonObjResp.getString("status");

                        if(status.equalsIgnoreCase("true"))
                        {
                            String userID=jsonObjResp.get("message").toString();
                            sharedPref.setStringPreference(getActivity(), appConstants.userID_KEY,userID);
                            sharedPref.setStringPreference(getActivity(), appConstants.userEmail_KEY,emailTXT.getText().toString().trim());
                            sharedPref.setStringPreference(getActivity(), appConstants.userPassword_KEY,passTXT.getText().toString().trim());
                            sharedPref.setBooleanPreference(getActivity(), appConstants.isLoggedIn,true);
                            getActivity().finish();
                        }
                        else
                        {
                            appConstants.stopSpinWheel();
                            Toast.makeText(getActivity(), jsonObjResp.get("message").toString(), Toast.LENGTH_LONG).show();
                        }
                    }
                    else
                    {
                        appConstants.stopSpinWheel();
                    //    Toast.makeText(getActivity(), "server error", Toast.LENGTH_LONG).show();
                    }


                }
                catch (Exception e)
                {
                    Toast.makeText(getActivity(),e.getMessage().toString(), Toast.LENGTH_SHORT).show();
                    appConstants.stopSpinWheel();
                }

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error)
            {
                Toast.makeText(getActivity(),error.getMessage(), Toast.LENGTH_SHORT).show();
                appConstants.stopSpinWheel();

            }
        });

        RequestQueue requestQueue = Volley.newRequestQueue(getActivity());
        jsonObjReq.setRetryPolicy(new DefaultRetryPolicy(
                appConstants.appTimeOut,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(jsonObjReq);
    }

}
