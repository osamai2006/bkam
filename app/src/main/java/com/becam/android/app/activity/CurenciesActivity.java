package com.becam.android.app.activity;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.becam.android.app.R;
import com.becam.android.app.adapters.CurrenciesListAdapter;
import com.becam.android.app.fragment.ExchangeFragment;
import com.becam.android.app.models.CurrencyListModel;
import com.becam.android.app.others.appConstants;
import com.becam.android.app.others.urlClass;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

public class CurenciesActivity extends Activity {


    ListView currenciesLV;
    EditText serachCurencyTXT;

    CurrencyListModel model;
    ArrayList<CurrencyListModel> arrayList;
    CurrenciesListAdapter adapter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.currencies_activity);

        currenciesLV=findViewById(R.id.currenciesLV);
        //serachCurencyTXT=findViewById(R.id.serachCurencyTXT);

        currenciesLV.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                String symb=arrayList.get(position).getSymbol();
                String name=arrayList.get(position).getName();
                String image=arrayList.get(position).getImage();

                ExchangeFragment.setCurencyTXTVIEW(symb,name,image,appConstants.fromToFlag);

                finish();
            }
        });

        appConstants.startSpinwheel(CurenciesActivity.this, false, true);
        new Thread(new Runnable() {
            @Override
            public void run() {
                try
                {
                    getCurrencyReq(urlClass.getCurencConvertListURL, null);
                } catch (Exception xx) {
                }
            }
        }).start();
    }


    private void getCurrencyReq(String urlPost, final JSONObject jsonObject) {

        final RequestQueue queue;
        StringRequest stringRequest = null;

        try {

            queue = Volley.newRequestQueue(CurenciesActivity.this);

            stringRequest = new StringRequest(Request.Method.GET, urlPost,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response)
                        {
                            try
                            {
                                if(response!=null)
                                {
                                    appConstants.stopSpinWheel();
                                    arrayList=new ArrayList<>();

                                    JSONArray arr = new JSONArray(response.toString());

                                    for(int i=0;i<arr.length();i++)
                                    {
                                        JSONObject obj=arr.getJSONObject(i);

                                        String symbol =obj.getString("symbol");
                                        String name =obj.getString("name");
                                        String image =obj.getString("image");

                                        model=new CurrencyListModel(symbol,name,image);
                                        arrayList.add(model);


                                    }

                                    adapter = new CurrenciesListAdapter(CurenciesActivity.this, arrayList);
                                    currenciesLV.setAdapter(adapter);


                                }


                            }
                            catch (Exception e)
                            {
                                e.printStackTrace();
                                appConstants.stopSpinWheel();
                            }


                        }
                    }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(final VolleyError error) {

                   // Toast.makeText(CurenciesActivity.this, "server error", Toast.LENGTH_SHORT).show();
                    appConstants.stopSpinWheel();


                }


            });

            stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                    appConstants.appTimeOut,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

            queue.add(stringRequest);


        } catch (Exception e) {
            appConstants.stopSpinWheel();
            e.toString();
        }



    }


}
