package com.becam.android.app.fragment;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkResponse;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HttpHeaderParser;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.becam.android.app.R;
import com.becam.android.app.activity.MainScreenActivity;
import com.becam.android.app.adapters.MainCatNewsAdapter;
import com.becam.android.app.models.MainCategoryNewsModel;
import com.becam.android.app.others.appConstants;
import com.becam.android.app.others.urlClass;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

/**
 * Created by HP on 5/9/2017.
 */

public class MainCategoryNewsFragment extends Fragment {


    View view;
    private GridView catGV;
    MainCatNewsAdapter adapter;
    Context context;
    MainCategoryNewsModel model;
    private ArrayList<MainCategoryNewsModel> arrayList;

    private static MainScreenActivity myContext;
    @Override
    public void onAttach(Activity activity)
    {
        myContext = (MainScreenActivity) activity;
        super.onAttach(activity);
    }



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.main_category_layout, container, false);

        catGV = (GridView) view.findViewById(R.id.catGV);
        MainScreenActivity.screenTitleTXT.setText("الرئيسية");


        appConstants.startSpinwheel(getActivity(),false,true);
        new Thread(new Runnable() {
            @Override
            public void run()
            {
                try
                {
                    String urlData= urlClass.getMainNewsCategoryURL+"";

                    getNewsCategoriesReq(urlData,null);
                }
                catch (Exception xx)
                {}
            }
        }).start();


        catGV.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                HomeFragment.newsLink=arrayList.get(position).getLink();

                if(!arrayList.get(position).getLink().equalsIgnoreCase("")) {
                    FragmentTransaction tx = myContext.getSupportFragmentManager().beginTransaction();
                    tx.replace(R.id.main_container, new HomeFragment());
                    tx.addToBackStack(null);
                    tx.commit();
                }

            }
        });

        return view;
    }



    private void getNewsCategoriesReq(String urlPost, final JSONObject jsonObject) {

        final RequestQueue queue;
        StringRequest stringRequest = null;

        try {

            queue = Volley.newRequestQueue(getActivity());

            stringRequest = new StringRequest(Request.Method.GET, urlPost,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            try
                            {

                                if (response != null)
                                {
                                        appConstants.stopSpinWheel();

                                        arrayList = new ArrayList<>();

                                        JSONArray arr = new JSONArray(response.toString());

                                        for (int i = 0; i < arr.length(); i++)
                                        {
                                            JSONObject mainObject = arr.getJSONObject(i);

                                            String id = mainObject.getString("id");
                                            String name = mainObject.getString("title");
                                            String image = mainObject.getString("logo");
                                            String link= mainObject.getString("link");

                                            model = new MainCategoryNewsModel(id, image,name,link);
                                            arrayList.add(model);

                                        }
                                        adapter = new MainCatNewsAdapter(getActivity(), arrayList);
                                        catGV.setAdapter(adapter);




                                }



                            }
                            catch (Exception xx)
                            {
                                xx.toString();
                                appConstants.stopSpinWheel();
                            }


                        }
                    }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(final VolleyError error) {

                    Toast.makeText(getActivity(), "server error", Toast.LENGTH_SHORT).show();
                    appConstants.stopSpinWheel();


                }


            })


            {

                @Override
                protected Map<String, String> getParams() {

                    Map<String, String> params = new HashMap<String, String>();
                    try {

                        Iterator<?> keys = jsonObject.keys();

                        while (keys.hasNext()) {
                            String key = (String) keys.next();
                            String value = jsonObject.getString(key);
                            params.put(key, value);

                        }


                    } catch (Exception xx) {
                        xx.toString();
                        appConstants.stopSpinWheel();
                    }
                    return params;
                }


                @Override
                protected Response<String> parseNetworkResponse(NetworkResponse response) {
                    try {

                        String jsonString = new String(response.data,
                                HttpHeaderParser.parseCharset(response.headers));

                        return Response.success(jsonString, HttpHeaderParser.parseCacheHeaders(response));


                    } catch (UnsupportedEncodingException e) {
                        return Response.error(new ParseError(e));
                    }
                }


            };

            stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                    appConstants.appTimeOut,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

            queue.add(stringRequest);


        } catch (Exception e) {
            appConstants.stopSpinWheel();
            e.toString();
        }



    }







}
