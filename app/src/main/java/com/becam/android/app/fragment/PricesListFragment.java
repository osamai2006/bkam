package com.becam.android.app.fragment;

import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.becam.android.app.R;
import com.becam.android.app.activity.MainScreenActivity;
import com.becam.android.app.adapters.CatAndSharesAdapter;
import com.becam.android.app.adapters.GoldDetailsAdapter;
import com.becam.android.app.adapters.PricesListAdapter;
import com.becam.android.app.models.CategoryModel;
import com.becam.android.app.models.GoldDetailsChartModel;
import com.becam.android.app.models.GoldDetailsModel;
import com.becam.android.app.models.PricesListModel;
import com.becam.android.app.others.appConstants;
import com.becam.android.app.others.urlClass;
import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.components.AxisBase;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.formatter.IAxisValueFormatter;
import com.github.mikephil.charting.formatter.IFillFormatter;
import com.github.mikephil.charting.interfaces.dataprovider.LineDataProvider;
import com.github.mikephil.charting.interfaces.datasets.IDataSet;
import com.github.mikephil.charting.interfaces.datasets.ILineDataSet;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by HP on 5/3/2018.
 */

public class PricesListFragment extends Fragment  {
    public static MainScreenActivity myContext;


    View view;

    RecyclerView pricesListLV;


    PricesListModel model;
    ArrayList<PricesListModel> arrayList;
    PricesListAdapter adapter;




    @Override
    public void onAttach(Context context) {
        myContext=(MainScreenActivity) getActivity();
        super.onAttach(context);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.prices_list_fragment, container, false);
        pricesListLV= view.findViewById(R.id.pricesListLV);
        final SwipeRefreshLayout swipeToRefreshLayout = (SwipeRefreshLayout) view.findViewById(R.id.swipeTorefresh);
        MainScreenActivity.screenTitleTXT.setText(getResources().getString(R.string.pricelist_tab));


        swipeToRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                swipeToRefreshLayout.setRefreshing(true);

                appConstants.startSpinwheel(getActivity(), false, true);

                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        try
                        {
                            getPriceRequest(urlClass.getCategoryWithShareURL, null);

                        } catch (Exception xx) {
                        }
                    }
                }).start();

                swipeToRefreshLayout.setRefreshing(false);

            }
        });

        appConstants.startSpinwheel(getActivity(), false, true);

        new Thread(new Runnable() {
            @Override
            public void run() {
                try
                {
                    getPriceRequest(urlClass.getCategoryWithShareURL, null);

                } catch (Exception xx) {
                }
            }
        }).start();


        return view;
    }


    private void getPriceRequest(String urlPost, final JSONObject jsonObject) {

        final RequestQueue queue;
        StringRequest stringRequest = null;

        try {

            queue = Volley.newRequestQueue(getActivity());

            stringRequest = new StringRequest(Request.Method.GET, urlPost,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response)
                        {
                            try
                            {
                                if(response!=null)
                                {
                                    appConstants.stopSpinWheel();

                                    arrayList = new ArrayList<>();

                                    JSONArray arr = new JSONArray(response.toString());

                                    for (int i = 0; i < arr.length(); i++) {

                                        try {
                                            JSONObject mainObj=arr.getJSONObject(i);

                                            String id = mainObj.getString("id");
                                            String name = mainObj.getString("cat_name");
                                            String desc = "";
                                            JSONArray sharesArr =mainObj.getJSONArray("items");

                                            String flag="";
                                            if(id.equalsIgnoreCase("1"))
                                            {
                                                flag="currency" ;
                                            }
                                            else   if(id.equalsIgnoreCase("2"))
                                            {
                                                flag="gold";
                                            }
                                            else   if(id.equalsIgnoreCase("3"))
                                            {
                                                flag="oil";
                                            }
                                            else   if(id.equalsIgnoreCase("4"))
                                            {
                                                flag="bitcoin";
                                            }
                                            else   if(id.equalsIgnoreCase("5"))
                                            {
                                                flag="material";
                                            }
                                            else  if(id.equalsIgnoreCase("6"))
                                            {
                                                flag="food";
                                            }
                                            else  if(id.equalsIgnoreCase("7"))
                                            {
                                                flag="meat";
                                            }
                                            else  if(id.equalsIgnoreCase("8"))
                                            {
                                                flag="veg";
                                            }
                                            else  if(id.equalsIgnoreCase("9"))
                                            {
                                                flag="fruit";
                                            }
                                            else  if(id.equalsIgnoreCase("10"))
                                            {
                                                flag="wmeat";
                                            }

                                            model = new PricesListModel(id, name, desc, sharesArr,flag);
                                            arrayList.add(model);

                                        } catch (Exception xx) {
                                            xx.toString();
                                        }
                                    }


                                    adapter = new PricesListAdapter(getActivity(), arrayList);
                                    pricesListLV.setAdapter(adapter);
                                    LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
                                    pricesListLV.setLayoutManager(layoutManager);

                                }


                            }
                            catch (Exception e)
                            {
                                e.printStackTrace();
                                appConstants.stopSpinWheel();
                            }


                        }
                    }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(final VolleyError error) {

                  //  Toast.makeText(getActivity(), "server error", Toast.LENGTH_SHORT).show();
                    appConstants.stopSpinWheel();


                }


            });

            stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                    appConstants.appTimeOut,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

            queue.add(stringRequest);


        } catch (Exception e) {
            appConstants.stopSpinWheel();
            e.toString();
        }



    }


}
