package com.becam.android.app.activity;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.becam.android.app.R;
import com.becam.android.app.others.appConstants;
import com.becam.android.app.others.urlClass;

import org.json.JSONObject;

/**
 * Created by HP on 5/3/2017.
 */

public class ForgetPasswordActivity extends Activity {

    EditText emailTXT;
    Button submitBTN;
    Activity activityCurrent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.forget_password_layout);
        activityCurrent=this;
        emailTXT= (EditText) findViewById(R.id.email_forget_txt);
        submitBTN= (Button) findViewById(R.id.submitForgetBTN);
        submitBTN.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                   if(checkRule())
                    {
                        appConstants.startSpinwheel(ForgetPasswordActivity.this, false, true);

                            new Thread(new Runnable() {
                                @Override
                                public void run()
                                {

                                    try
                                    {
                                        String url="";// urlClass.forgetPassURL;
                                        JSONObject Objct = new JSONObject();
                                        Objct.putOpt("EMAIL", emailTXT.getText().toString().trim());

                                        forgetPassRequest(url,Objct);
                                    }
                                    catch (Exception xx)
                                    {
                                        xx.toString();
                                        appConstants.stopSpinWheel();
                                    }
                                }
                            }).start();





                    }

            }
        });

    }

    private  boolean checkRule() {
        if (emailTXT.getText().toString().trim().equals("")) {
            Toast.makeText(activityCurrent, "Please check the email", Toast.LENGTH_SHORT).show();
            return false;
        }

        if (!emailTXT.getText().toString().trim().matches(appConstants.email_pattern)) {
            Toast.makeText(activityCurrent, "invalid email", Toast.LENGTH_SHORT).show();
            return false;
        }

        return  true;
    }

    private void forgetPassRequest(String urlPost, final JSONObject jsonObjectRequest) {

        JsonObjectRequest jsonObjReq;

        jsonObjReq = new JsonObjectRequest(Request.Method.POST, urlPost, jsonObjectRequest, new Response.Listener<JSONObject>()
        {

            @Override
            public void onResponse(JSONObject response) {

                try
                {
                    if(response !=null)
                    {
                        appConstants.stopSpinWheel();

                            JSONObject jsonObjResp = new JSONObject(response.toString());
                            final String status=jsonObjResp.getString("status");

                            if(status.equalsIgnoreCase("true"))
                            {
                                Toast.makeText(ForgetPasswordActivity.this, jsonObjResp.get("message").toString(), Toast.LENGTH_LONG).show();
                                finish();
                            }
                            else
                            {
                                Toast.makeText(ForgetPasswordActivity.this, jsonObjResp.get("message").toString(), Toast.LENGTH_LONG).show();
                            }

                    }
                    else
                    {
                        appConstants.stopSpinWheel();
                       // Toast.makeText(getApplicationContext(), "server error", Toast.LENGTH_LONG).show();
                    }


                }
                catch (Exception e)
                {
                    Toast.makeText(getApplicationContext(),e.getMessage().toString(), Toast.LENGTH_SHORT).show();
                    appConstants.stopSpinWheel();
                }

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error)
            {
                Toast.makeText(getApplicationContext(),error.getMessage(), Toast.LENGTH_SHORT).show();
                appConstants.stopSpinWheel();

            }
        });

        RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());
        jsonObjReq.setRetryPolicy(new DefaultRetryPolicy(
                appConstants.appTimeOut,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(jsonObjReq);
    }


}


