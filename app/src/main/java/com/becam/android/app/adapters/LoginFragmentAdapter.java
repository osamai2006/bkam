package com.becam.android.app.adapters;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.becam.android.app.fragment.RegisterDetailsFragment;
import com.becam.android.app.fragment.loginDetailsFragment;

/**
 * Created by HP on 5/3/2017.
 */

public class LoginFragmentAdapter extends FragmentStatePagerAdapter {

    int TabCount;

    public LoginFragmentAdapter(FragmentManager fragmentManager, int CountTabs) {

        super(fragmentManager);

        this.TabCount = CountTabs;
    }

    @Override
    public Fragment getItem(int position) {

        switch (position) {
            case 0:
                loginDetailsFragment loginDetailsFragment = new loginDetailsFragment();
                return loginDetailsFragment;

            case 1:
                RegisterDetailsFragment registerDetailsFragment = new RegisterDetailsFragment();
                return registerDetailsFragment;

            default:
                return null;
        }
    }

    @Override
    public int getCount() {
        return TabCount;
    }
}